using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FOVScaler : MonoBehaviour
{
    [SerializeField] Transform FOVCollider;

    void Awake()
    {
        //if (Instance == null) { Instance = this; }
        Humans.OnCrewChange += CrewChange;
    }

    private void OnDestroy()
    {
        Humans.OnCrewChange -= CrewChange;
    }

    private void CrewChange(string name)
    {
        // Debug.Log("Scaling FOV to " + PlayerCharacter.Instance.GetSkillValue(2));
        // need to wait for all the events and stuff to add all the attributes before reacting.
        Invoke("ScaleFOV", 0.5f);
    }

    private void ScaleFOV()
    {

        FOVCollider.localScale = new Vector3(PlayerCharacter.Instance.GetSkillValue(3), 1f, PlayerCharacter.Instance.GetSkillValue(2));
    }
}
