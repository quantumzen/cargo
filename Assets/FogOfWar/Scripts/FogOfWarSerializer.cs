using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using GameDevTV.Saving;

public class FogOfWarSerializer : MonoBehaviour 
{
    [SerializeField]
    Texture2D m_Texture;
    [SerializeField]
    bool m_Capture = false;
    string savePath;
    byte[] bytes;
    bool m_Initialized = false;
    

    private void Start()
    {
        savePath = Application.persistentDataPath + "/VisionBlockingTexture_SerialiserSave.png";
    }

    void Update()
    {
        if (!m_Initialized)
        {
            m_Initialized = true;
            if (TryLoad(out bytes))
            {
                m_Texture = GetComponent<FogOfWarManager>().GetFogTexture();
                m_Texture.LoadImage(bytes);
                GetComponent<FogOfWarManager>().SetFogTexture(m_Texture);
            }
        }
    }
    private void OnValidate()
    {
        if (m_Capture)
        {
            Capture();
            m_Capture = false;
        }
    }

    private void Capture()
    {
        m_Texture = GetComponent<FogOfWarManager>().GetFogTexture();
        bytes = m_Texture.EncodeToPNG();

        Save(bytes);
    }

    void Save(byte[] b) {
        File.WriteAllBytes(savePath, b);
    }

    bool TryLoad(out byte[] b) {
        if (File.Exists(savePath))
        {
            b= File.ReadAllBytes(savePath); 
            return true;
        }
        b = null;
        return false;
    }

    public object CaptureState()
    {
        Capture();
        if (TryLoad(out bytes))
        {
            //DumpBytes();
            return bytes;
        }
        return null;
    }

    public void RestoreState(object state)
    {
        bytes = (byte[])state;
        m_Texture = GetComponent<FogOfWarManager>().GetFogTexture();
        m_Texture.LoadImage(bytes);
        //DumpBytes();
        GetComponent<FogOfWarManager>().SetFogTexture(m_Texture); 
    }

    private void DumpBytes()
    {

        string s = "";
        foreach (byte _b in bytes) s += ((int)_b).ToString() + " ";
        Debug.Log("VisionBlockingTexture has been given. " + s);
        System.IO.File.WriteAllText(Application.persistentDataPath + "/arrayDump.txt", s);
    }

}
