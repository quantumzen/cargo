using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddBuff : MonoBehaviour
{
[SerializeField] PlayerCharacter.BuffType buffType;
    [SerializeField] string description;
    [SerializeField] float timeLeft;
    [SerializeField] float strength;

    public void TriggerBuff()
    {
        PlayerCharacter.Instance.AddBuff(new PlayerCharacter.Buff(buffType, description, timeLeft, strength));
    }


}
