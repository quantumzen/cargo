using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// I will not use it for now. refactoring it now would be too messy.
public class Ammo
{

    public int invNum;
    public int APdamage;
    public string label;


    public Ammo(string _label, int _invNum)
    {
        invNum = _invNum;
        label = _label;

    }

}
