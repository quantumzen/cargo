using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuffBeh : MonoBehaviour
{
    public Image buffSprite;
    public Image buffTimer;
    
    //emerge and destroy and such?
    public void DelayDestruction()
    {
        Invoke("Destruction", 2f);
    }
    private void Destruction()
    {
        Destroy(gameObject);
    }
}
