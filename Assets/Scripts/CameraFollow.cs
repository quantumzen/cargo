using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    [SerializeField] private bool isMiniMap;
    [SerializeField] private bool isFollowThirdPerson;
    [SerializeField] private Transform FollowThirdPersonPoint;
    [SerializeField] private Transform FollowMinimapPoint;
    [SerializeField] private float Cameraheight;


    void LateUpdate()
    {
        if (isMiniMap)
        {
            transform.position = new Vector3(FollowMinimapPoint.position.x, Cameraheight, FollowMinimapPoint.position.z);
        }
        else if (isFollowThirdPerson)
        {
            transform.position = new Vector3(FollowThirdPersonPoint.position.x, FollowThirdPersonPoint.position.y, FollowThirdPersonPoint.position.z);
            transform.LookAt(FollowThirdPersonPoint.parent.position);
            // transform.rotation = FollowThirdPersonPoint.rotation;
        }

    }
}
