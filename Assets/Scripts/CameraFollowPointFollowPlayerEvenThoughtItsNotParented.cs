using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPointFollowPlayerEvenThoughtItsNotParented : MonoBehaviour
{

    [SerializeField] Transform toFollow;
    
    void Update()
    {
        if (GameManager.Instance.stage == GameManager.Stage.game)
        {
            transform.position = toFollow.position;  // Lerping din't help  Vector3.Lerp(transform.position, toFollow.position, 0.5f);   //
        }

        if (GameManager.Instance.stage == GameManager.Stage.gameover)
        {
            transform.position = toFollow.position;
            transform.RotateAround(Vector3.up, 0.2f*Time.deltaTime);
            //Quaternion q = transform.rotation * Quaternion.AngleAxis(_look.x, Vector3.up); // 
            //transform.rotation = Quaternion.Slerp(followTransform.transform.rotation, q, Time.deltaTime * lookSensitivity);
        }
    }
}
