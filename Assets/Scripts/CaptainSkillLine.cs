using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CaptainSkillLine : MonoBehaviour
{
    // Main Menu Manager owns me and I will speak only to it. Even if it interacts with other stuff line character stats script.
    [SerializeField] TMPro.TextMeshProUGUI skillLabel;
    [SerializeField] TMPro.TextMeshProUGUI value;
    int skillValue; // I have a feeling I'll need it.
    public int skillNumber; 
    public MainMenuManager pb;
    [SerializeField] Button buy;// increase skill
    [SerializeField] Button sell; // decreaase skill

    public void OnBuyPressed()
    {
        pb.OnBuyClicked(skillNumber);

    }
    public void OnSellPressed()
    {

        pb.OnSellClicked(skillNumber);
    }

    public void SetSkillName(string s)
    {
        skillLabel.text = s;
    }

    public void SetSkillValue(int s)
    {
        value.text = s.ToString();
        skillValue = s;
    }

    public void UpdateButtons() // enabling and disabling buttons, triggered by mainmenuman or this.
    {
        if (skillValue == 0)
        {
            sell.interactable =false;
        } else
        {
            sell.interactable = true;
        }

        buy.interactable = true;

        if (pb.availableSkillPoints <= 0 || (pb.availableSkillPoints <= 1 && skillValue >= 12) || (pb.availableSkillPoints <= 3 && skillValue >= 16) || (pb.availableSkillPoints <= 4 && skillValue >= 18) ) buy.interactable = false;

    }

}
