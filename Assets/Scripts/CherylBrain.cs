using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CherylBrain : MonoBehaviour
{

    [SerializeField] Animator ani;

    private void Update()
    {
        if (Vector3.Distance(PlayerMovement.Instance.transform.position, transform.position) < 10 && transform.GetComponent<Interactible>().hailable)
        {
            ani.SetFloat("Blend", 1);
        } else
        {
            ani.SetFloat("Blend", 0);
        }
    }

    public void ActionTrigger() // I need something to start the actions.
    {

    }
    
}
