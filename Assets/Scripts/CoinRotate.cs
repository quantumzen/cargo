using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinRotate : MonoBehaviour
{
    [SerializeField] float rotationSpeed = 1f;
 
    void Update()
    {
        // transform.RotateAroundLocal(Vector3.up, rotationSpeed);
        transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);

    }
}
