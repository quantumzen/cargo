using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Condition
{

    
    [SerializeField] Disjunction[] and;

    public bool Check(IEnumerable<IPredicateEvaluator> evaluators)
    {
        foreach (Disjunction dis in and)
        {
            if (!dis.Check(evaluators)) return false;
        }
        return true ;
    }

    [System.Serializable]
    public class Disjunction
    {
        [SerializeField] Predicate[] or;
        public bool Check(IEnumerable<IPredicateEvaluator> evaluators)
        {
            foreach (Predicate pred in or)
            {
                if (pred.Check(evaluators)) return true;
            }
            return false;
        }
    }



    [System.Serializable]
    public class Predicate
    {

        [SerializeField] bool not = false;
        [SerializeField] string predicate;
        [SerializeField] string[] args;

        public bool Check(IEnumerable<IPredicateEvaluator> evaluators)
        {
            foreach (IPredicateEvaluator evaluator in evaluators)
            {
                bool? result = evaluator.Evaluate(predicate, args);
                if (result == null) continue;
                if (result == not) return false;

            }
            return true;
        }
    }

}
