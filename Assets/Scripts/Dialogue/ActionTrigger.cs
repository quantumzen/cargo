using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActionTrigger : MonoBehaviour
{

    [System.Serializable]
    public class TriggerParameters
    {
        [SerializeField] public string action;
        [SerializeField] public UnityEvent OnTrigger;
    }

    [SerializeField] TriggerParameters[] triggerParameters;

    public void Trigger(string actionToTrigger)
    {
        foreach (TriggerParameters tp in triggerParameters)
        {
            if (actionToTrigger == tp.action)
            {
                Debug.Log("Invoking trigger " + tp.action);
                tp.OnTrigger.Invoke();
            }
        }
    }

}
