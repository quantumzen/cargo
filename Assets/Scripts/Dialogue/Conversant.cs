using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using GameDevTV.Saving;

namespace RPG.Dialogue
{
    public class Conversant : MonoBehaviour, ISaveable
    {

        [SerializeField] Dialogue curDialogue; // temporary.
        DialogueNode curNode;
        [SerializeField] private PanelBeh dialoguePanelBeh;
        [SerializeField] private TextMeshProUGUI speaker;
        [SerializeField] private Image SpeakerPicture;
        [SerializeField] private TextMeshProUGUI dialogueBody;
        [SerializeField] private GameObject buttonPrefab;
        [SerializeField] private Transform NextButton;
        [SerializeField] private Gradient risk;
        [SerializeField] private PlayerCharacter pch;
        private float diceRoll;
        private float riskRoll;
        private int nodeIsTestResultSkillNumber = 11;
        private Interactible interlocutor;
        // private List<string> children = new List<string>();
        private List<GameObject> buttons = new List<GameObject>();
        private List<string> burnt = new List<string>(); // it's not dialogue specific. It holds ALL burned nodes. for EVERYTHING.
        private List<string> disabledNodes = new List<string>(); // I'll need to be smart not to duplicate entries here.
        [HideInInspector] public static Conversant Instance { get; private set; }


        void Awake()
        {
            if (Instance == null) { Instance = this; }
            // SetDialogue(curDialogue);
            // if (curNode == null)  curNode = curDialogue.GetRootNode();
            DisableThroughAllDialogues();
            Invoke("CloseDialogue", 0.1f);
            // Invoke("PurgeLists", 1f);
        }

        private void DisableThroughAllDialogues()
        {
            foreach (Dialogue q in Resources.LoadAll<Dialogue>(""))
            {
                MarkDisabledNodes(q);
            }
        }



        private void DrawDialogue()
        {
            if (curDialogue == null) return; // cap[tturing ewxception for when the ... wharchamacallit dialogue close unexpectedly but still wants tgo draw itsweeflf
            buttons.Clear();
            dialogueBody.text = FormatNodeText(curNode.GetText());
            speaker.text = curNode.GetSpeakerName();
            SpeakerPicture.sprite = GetComponent<Humans>().GetFace(speaker.text);
            diceRoll = 0f; // means we're not rolling, nothing to test here. // no, no point.
            riskRoll = 0f;

            foreach (DialogueNode childNode in FilterOnCOndition(curDialogue.GetAllChildren(curNode)))
            {
                if (!childNode.IsPlayerSpeaking())
                {
                    NextButton.GetComponent<Button>().interactable = true;
                    NextButton.GetComponent<DialogueOptionBeh>().myNode = curNode; // haHA! how do you like them apples!
                    // no text means we enable "..." button, which is different. Permannetnntn.
                    // this configuration should have only one node though.
                    // FilterOnCOndition so multiple nodes allowed was something to do, but turns out, it works. I tested HsQuality predicate. If many meet the criterion, the last one is taken. The last node. It seems. But it's a sample of 2, so I may be wrong.
                }
                else
                {
                    NextButton.GetComponent<Button>().interactable = false;
                    GameObject btn = Instantiate(buttonPrefab, dialogueBody.transform.parent.transform);
                    string _t = "";
                    if (childNode.GetSkillTest().skillNumber != 0) // yay! we are skill testing!
                    {
                        // Debug.Log("childNode.GetSkillTest().chance " + childNode.GetSkillTest().chance);
                        // Debug.Log("skillnumber " + childNode.GetSkillTest().skillNumber);
                        // Debug.Log("skillval " + pch.GetSkillValue(1));
                        if (childNode.GetSkillTest().chance > pch.GetSkillValue(childNode.GetSkillTest().skillNumber) && childNode.GetSkillTest().absolute)
                        { // w�z albo przew�z. W�z
                            _t = "[ <color=#ff3333>" + pch.GetSkillName(childNode.GetSkillTest().skillNumber) + " " + pch.GetSkillValue(childNode.GetSkillTest().skillNumber) + "</color> ] ";

                            btn.GetComponent<Button>().interactable = false;
                            btn.GetComponentInChildren<TextMeshProUGUI>().alpha = 0.7f;
                        } else if (childNode.GetSkillTest().chance <= pch.GetSkillValue(childNode.GetSkillTest().skillNumber) && childNode.GetSkillTest().absolute)
                        { // przew�z.
                            _t = "[ <color=#33ff33>" + pch.GetSkillName(childNode.GetSkillTest().skillNumber) + "</color> ] "; // + " " + pch.GetSkillValue(childNode.GetSkillTest().skillNumber) + // we don't need to display the skill value, do we?
                        } else if (!childNode.GetSkillTest().absolute)
                        { // szansa jest zawsze
                            riskRoll = Mathf.Clamp01(pch.GetSkillValue(childNode.GetSkillTest().skillNumber) / childNode.GetSkillTest().chance);
                            _t = "[ <color=#"+ ColorUtility.ToHtmlStringRGB(risk.Evaluate(riskRoll)) + ">" + pch.GetSkillName(childNode.GetSkillTest().skillNumber) + " " + pch.GetSkillValue(childNode.GetSkillTest().skillNumber) + " (chance "+ Mathf.FloorToInt(riskRoll * 100)+"%) </color> ] ";
                            
                        }
                    } // end of skilltesting
                    btn.GetComponentInChildren<TextMeshProUGUI>().text = _t + FormatNodeText(childNode.GetText(),true);
                    buttons.Add(btn);
                    btn.GetComponent<DialogueOptionBeh>().myNode = childNode;
                }

            }
            if (curDialogue.CountAllChildren(curNode) == 0) //  (buttons.Count == 0 && NextButton.GetComponent<Button>().interactable == false)
            {
                // we have no button! The dialogue is at its end! (no it's not. It used to be, a month ago)
                // this means the interlocutor should not be hailable anymore; they have nothing to say
                // if I want their wisdom to linger, I should make a looped node somehow.
                // interlocutor.hailable = false;  // daring and ugly, but I'm good enough.
                // what if there is another dialogue set on this NPC?
                // they should set the interactible flag
                // No. I can't just clear that flag. There may be children hidden there. 
                // I can change the condition above, perhaps... 
                // NO. No, no, NO. Use "onExit" action trigger and clear it this way. 
                // now interactible has ClearHailable() method.
                interlocutor.ClearHailable();

                PlayerMovement.Instance.ClearInteractible(); // make the "press E" disappear.
            }

        }

        private string FormatNodeText(string s, bool isButton = false)
        {
            
            
            string normalFont = "<color=#" + ColorUtility.ToHtmlStringRGBA(GameManager.Instance.normalText) + ">";
            string narrativeFont = "<color=#" + ColorUtility.ToHtmlStringRGBA(GameManager.Instance.narrativeText) + ">";
            string attentionFont = "<color=#" + ColorUtility.ToHtmlStringRGBA(GameManager.Instance.attentionText) + "><b>"; // bold isn't working. may be to do with font atlas.
            if (isButton)
            {
                normalFont = "<color=#" + ColorUtility.ToHtmlStringRGBA(GameManager.Instance.buttonText) + ">";
            }


            s = normalFont + s;
            s = s.Replace("[", narrativeFont + "[");
            s = s.Replace("]", "]" + normalFont);
            s = s.Replace("<$hubDistance>", Mathf.Round(PlayerMovement.Instance.GetDistanceToHub()).ToString());
            s = s.Replace("<$hubBearing>", Mathf.Round(PlayerMovement.Instance.GetBearingToHub()).ToString());
            s = s.Replace("<$endConversation>", attentionFont + "[ end conversation ]</b>" + normalFont);

            return s;
        }


        private void MarkDisabledNodes(Dialogue d) // mark then in currently open dialogue.
        {
            foreach(DialogueNode n in d.GetAllNodes())
            {
                if (!n.IsEnabled()) DisableNode(n.name);
            }
        }

        private void DisableNode(string n)
        {
            // if doesn't contain : add
            if (!disabledNodes.Contains(n)) { disabledNodes.Add(n); // Debug.Log("Disabling node " + n);
                                                                    
            }
        }
        private void EnableNode(string n)
        {
            // if doesn't contain : add
            if (disabledNodes.Contains(n)) { disabledNodes.Remove(n); // Debug.Log("Enabling node " + n);
            }
        }


        private void ClearDialogue()
        {
            foreach (GameObject b in buttons)
            {
                Destroy(b);
            }
            dialogueBody.text = "";
            speaker.text = "";
        }

        public void NodeClicked(DialogueNode n)
        {
            ClearDialogue();
            bool noNextNode = true;
            diceRoll = Random.Range(0f, 1f);
            
            foreach (DialogueNode dn in FilterOnCOndition(curDialogue.GetAllChildren(n)))
            {
                noNextNode = false;
                if (dn.GetSkillTest().skillNumber == nodeIsTestResultSkillNumber)
                {
                    Debug.Log("The dice have spoken: " + diceRoll + " versus the chance of: " + riskRoll + ". \r\n Chance of the child node we're measuring: "+ dn.GetSkillTest().chance);
                    if (diceRoll > riskRoll && dn.GetSkillTest().chance < 0.1f) { SetCurNode(dn); break; }
                    if (diceRoll < riskRoll && dn.GetSkillTest().chance > 0.9f) { SetCurNode(dn); break; }
                } else   SetCurNode(dn);
                //curNode = dn; // we'll wind up witht hte last child, which is fine as this is the only one ...I seriously hope.
            }
            if (n.IsSingleUse()) burnt.Add(n.name);      // I don't need to filter. can't access a burnt node anyway.
            foreach (string _n in curDialogue.GetAllDisableeNames(n)) {
                Debug.Log("Disabling node " + _n);
                DisableNode(_n); }
            foreach (string _n in curDialogue.GetAllEnableeNames(n))
            {
                Debug.Log("Enabling node " + _n); EnableNode(_n); }
            if (noNextNode) TriggerExitAction(n);   // this is for cases where player speaks last and his button is to close the dialogue.
                                                    // player should always speak last!
            DrawDialogue();
        }

        public void OpenDialogue(Dialogue d, DialogueNode n, bool needsPause, Interactible interactible)
        {
            Debug.Log("Dialogue set to " + d.name);
            ClearDialogue(); // I need to clear it here in case it hasn't been properly cleaned. If X clicked, it just hides the window.
            curDialogue = d;
            interlocutor = interactible;
            SetCurNode(n);
            DrawDialogue();
            dialoguePanelBeh.Deploy();
            dialoguePanelBeh.GetComponent<TabHandler>().ButtonClicked((int)TabHandler.tab.dialogue);
            
        }

        public void CloseDialogue()
        {

            // this no longer makes sense. These should be no use cases where NPC node ends a dialogue.
            // if (curDialogue.CountAllChildren(curNode) == 0) TriggerExitAction(curNode); // as there is no other way to close the dialogue, we need to trigger the exit on entrance. Tough.
            // I've removed the (buttons.Count == 0)  as there is no point in counting buttons, as some may be hidden. Let's hope this works.
            dialoguePanelBeh.Hide();
            // Invoke("ClearDialogue",1f);
            ClearDialogue();
            curDialogue = null;
            curNode = null;
        }

        
        public DialogueNode GetDialogueNodeByName(Dialogue d, string nodeName)
        {
            DialogueNode n = d.GetRootNode();
            if (nodeName != null)
            {
                foreach (DialogueNode _n in d.GetAllNodes())
                {
                    if (_n.name == nodeName) return _n;
                }
            }
            return n;
        }

        public Dialogue GetCurDialogue()
        {
            if (curDialogue == null) return null;
            Debug.Log("You asked. CurDial is " + curDialogue.name);
            return curDialogue;
        }


        private void SetCurNode(DialogueNode n)
        {

            if (n == null) { 
                curNode = curDialogue.GetRootNode();
                TriggerEntryAction(curNode);
            } else {
                if (curNode != null) TriggerExitAction(curNode); // before we change the node, last thing.  // - null check as we may jump into the middle of the dialogue here and we use this to set curNode from null to something.
                curNode = n;
                TriggerEntryAction(curNode); // node changed, entry events.
            }
            interlocutor.currentNode = n;
        }

        private void TriggerExitAction(DialogueNode n)
        {
            string _action = n.GetOnExitAction();
            if (_action != "")
            {
                if (_action.Substring(0, 2) == "QC") // this is a dirty hack for modification of Quality without fussing with the triggers an other external thingamajigs
                    // basically syntax of QC <action number> <increment>
                {
                    string[] _actionSplit = _action.Split(' ');

                    int _qualityToChange = int.Parse(_actionSplit[1]);
                    int _increment = int.Parse(_actionSplit[2]);

                    Quality.Instance.IncrementQuality(_qualityToChange, _increment);
                    return;
                }
                Debug.Log("Exit Action triggered: " + _action);
                interlocutor.transform.GetComponent<ActionTrigger>().Trigger(_action);
            }
        }
        private void TriggerEntryAction(DialogueNode n)
        {
            string _action = n.GetOnEnterAction();
            if (_action != "")
            {
                if (_action.Substring(0, 2) == "QC") // this is a dirty hack for modification of Quality without fussing with the triggers an other external thingamajigs
                                                     // basically syntax of QC <action number> <increment>
                {
                    string[] _actionSplit = _action.Split(' ');

                    int _qualityToChange = int.Parse(_actionSplit[1]);
                    int _increment = int.Parse(_actionSplit[2]);

                    Quality.Instance.IncrementQuality(_qualityToChange, _increment);
                    return;
                }
                Debug.Log("Enter Action triggered: " + _action);
                interlocutor.transform.GetComponent<ActionTrigger>().Trigger(_action);
            }
        }

        #region Condition evaluation
        private IEnumerable<DialogueNode> FilterOnCOndition(IEnumerable<DialogueNode> InputNode)
        {
            foreach(DialogueNode node in InputNode)
            {
                if (node.CheckCondition(GetEvaluators()) && !burnt.Contains(node.name) && !disabledNodes.Contains(node.name))
                {
                    yield return node;
                }
            }
        }

        public Color GetRiskColor(float _r)
        {
            return risk.Evaluate(_r);
        }


        private IEnumerable<IPredicateEvaluator> GetEvaluators()
        {
            return GetComponents<IPredicateEvaluator>();
        }

        #endregion

        // debug only
        private void PurgeLists()
        {
            burnt.Clear();
            disabledNodes.Clear();
        }

        public object CaptureState()
        {
            Dictionary<string, object> savedata = new Dictionary<string, object>();
            savedata["burnt"] = burnt;
            savedata["disabledNodes"] = disabledNodes;

            return savedata;
        }

        public void RestoreState(object state)
        {

            Dictionary<string, object> savedata = (Dictionary<string, object>)state;
            burnt = ((List<string>)savedata["burnt"]);
            disabledNodes = ((List<string>)savedata["disabledNodes"]);
            
        }
    }

}
