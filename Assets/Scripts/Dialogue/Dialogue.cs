using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace RPG.Dialogue
{
    [CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogue", order = 0)]
    public class Dialogue : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField]
        List<DialogueNode> nodes = new List<DialogueNode>();
        [SerializeField] public string speaker;

        [NonSerialized]
        Vector2 newNodeOffset = new Vector2(250, 0);
        [NonSerialized]
        Dictionary<string, DialogueNode> nodeLookup = new Dictionary<string, DialogueNode>();


        private void Awake()
        {
            OnValidate();
        }

        private void OnValidate()
        {
            nodeLookup.Clear();
            foreach (DialogueNode node in GetAllNodes())
            {
                nodeLookup[node.name] = node;
            }
        }

        public IEnumerable<DialogueNode> GetAllNodes()
        {
            return nodes;
        }

        public DialogueNode GetRootNode()
        {
            return nodes[0];
        }

        public IEnumerable<DialogueNode> GetAllChildren(DialogueNode parentNode)
        {
            foreach (string childID in parentNode.GetChildren())
            {
                if (nodeLookup.ContainsKey(childID))
                {
                    yield return nodeLookup[childID];
                }
            }
        }
        public IEnumerable<DialogueNode> GetAllEnablees(DialogueNode parentNode) // returns nodes enabled by this node being accessed.
        {
            foreach (string childID in parentNode.GetEnablees())
            {
                if (nodeLookup.ContainsKey(childID))
                {
                    yield return nodeLookup[childID];
                }
            }
        }
        public IEnumerable<string> GetAllEnableeNames(DialogueNode n) // returns nodes disabled by this node being accessed.
        {
            foreach (string en in n.GetEnablees())
            {
                //if (nodeLookup.ContainsKey(en))
                    yield return en;
            }
        }
        public IEnumerable<DialogueNode> GetAllDisablees(DialogueNode parentNode) // returns nodes disabled by this node being accessed.
        {
            foreach (string childID in parentNode.GetDisablees())
            {
                //if (nodeLookup.ContainsKey(childID))
                
                    yield return nodeLookup[childID];
                
            }
        }

        public IEnumerable<string> GetAllDisableeNames(DialogueNode parentNode) // returns nodes disabled by this node being accessed.
        {
            foreach (string childID in parentNode.GetDisablees())
            {
                if (nodeLookup.ContainsKey(childID))
                {
                    yield return childID;
                }
            }
        }


        public int CountAllChildren(DialogueNode parentNode)
        {
            int i = 0;
            foreach (string childID in parentNode.GetChildren())
            {
                if (nodeLookup.ContainsKey(childID))
                {
                    i++;
                }
            }
            return i;
        }





#if UNITY_EDITOR
        public void CreateNode(DialogueNode parent)
        {
            DialogueNode newNode = CreateInstance<DialogueNode>();
            newNode.name = Guid.NewGuid().ToString();
            if (parent != null)
            {
                parent.AddChild(newNode.name);
                newNode.SetPlayerIsSpeaking(!parent.IsPlayerSpeaking());
                newNode.SetPosition(parent.GetRect().position + newNodeOffset);
            }
            //Undo.RegisterCreatedObjectUndo(newNode, "Created Dialogue Node");
            //Undo.RecordObject(this, "Added Dialogue Node");
            nodes.Add(newNode);
            OnValidate();
        }

        public void DeleteNode(DialogueNode nodeToDelete)
        {
            Undo.RecordObject(this, "Deleted Dialogue Node");
            nodes.Remove(nodeToDelete);
            OnValidate();
            CleanDanglingChildren(nodeToDelete);
            Undo.DestroyObjectImmediate(nodeToDelete);
        }

        private void CleanDanglingChildren(DialogueNode nodeToDelete)
        {
            foreach (DialogueNode node in GetAllNodes())
            {
                node.RemoveChild(nodeToDelete.name);
            }
        }
#endif

        public void OnBeforeSerialize()
        {
#if UNITY_EDITOR
            if (nodes.Count == 0)
            {
                CreateNode(null);
            }

            if (AssetDatabase.GetAssetPath(this) != "")
            {
                foreach (DialogueNode node in GetAllNodes())
                {
                    if (AssetDatabase.GetAssetPath(node) == "")
                    {
                        AssetDatabase.AddObjectToAsset(node, this);
                    }
                }
            }
#endif
        }

        public void OnAfterDeserialize()
        {
        }
    }
}
