using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace RPG.Dialogue
{
    public class DialogueNode : ScriptableObject
    {
        [SerializeField]        bool isPlayerSpeaking = false;
        [SerializeField]        string speaker;
        [SerializeField]        string text;
        [SerializeField]        List<string> children = new List<string>();
        [SerializeField]        List<string> enablees = new List<string>();
        [SerializeField]        List<string> disablees = new List<string>();
        [SerializeField]        Rect rect = new Rect(0, 0, 200, 100);
        [SerializeField]        private string OnEnterAction;
        [SerializeField]        private string OnExitAction;
        [SerializeField]        Condition condition;
        [SerializeField]        bool SingleUse;
        [SerializeField]        bool Enabled = true;
        bool burned = false; // no longer used, I think.
        // test to be able to click the node:
        // [SerializeField]        SkillTestElement skillTest = new SkillTestElement(0,0,false);

        // syntax for Quality Change:
        // QC  <action number> <increment>
        [Serializable]
        public struct SkillTest
        {
            public int skillNumber;
            public float chance;
            public bool absolute;

        }
        [SerializeField]        private SkillTest skillTest;




        public Rect GetRect()
        {
            return rect;
        }

        public string GetText()
        {
            return text;
        }

        public List<string> GetChildren()
        {
            return children;
        }
        public List<string> GetEnablees()
        {
            return enablees;
        }
        public List<string> GetDisablees()
        {
            return disablees;
        }


        public bool IsPlayerSpeaking()
        {
            return isPlayerSpeaking;
        }

        public int GetSpeaker()
        {
            if (isPlayerSpeaking) return 1;
            return 0;
        }

        public string GetOnEnterAction()
        {
            return OnEnterAction;
        }
        public string GetOnExitAction()
        {
            return OnExitAction;
        }
        internal bool CheckCondition(IEnumerable<IPredicateEvaluator> evaluators)
        {
            return condition.Check(evaluators);
        }

        public string GetSpeakerName()
        {
            return speaker;
        }

        public SkillTest GetSkillTest()
        {
            return skillTest;
        }

        public void BurnNode()        {            burned = true;        }
        public bool IsSingleUse() { return SingleUse; }
        public bool IsBurnt() { return burned; }
        public bool IsEnabled() { return Enabled; }






#if UNITY_EDITOR
        public void SetPosition(Vector2 newPosition)
        {
            Undo.RecordObject(this, "Move Dialogue Node");
            rect.position = newPosition;
            EditorUtility.SetDirty(this);
        }

        public void SetText(string newText)
        {
            if (newText != text)
            {
                Undo.RecordObject(this, "Update Dialogue Text");
                text = newText;
                EditorUtility.SetDirty(this);
            }
        }

        public void SetSpeakerName(string newText)
        {
            if (newText != speaker)
            {
                Undo.RecordObject(this, "Update Node Speaker Name");
                speaker = newText;
                EditorUtility.SetDirty(this);
            }
        }

        public void SetPlayerIsSpeaking(bool newIsPlayerSpeaking)
        {
            Undo.RecordObject(this, "Changed Dialogue Node Speaker");
            isPlayerSpeaking = newIsPlayerSpeaking;
            EditorUtility.SetDirty(this);
        }

        public void AddChild(string childID)
        {
            Undo.RecordObject(this, "Add Dialogue Link");
            children.Add(childID);
            EditorUtility.SetDirty(this);
        }

        public void RemoveChild(string childID)
        {
            Undo.RecordObject(this, "Remove Dialogue Link");
            children.Remove(childID);
            EditorUtility.SetDirty(this);
        }
        public void AddEnablee(string childID)
        {
            Undo.RecordObject(this, "Add Enablee Link");
            enablees.Add(childID);
            EditorUtility.SetDirty(this);
        }

        public void RemoveEnablee(string childID)
        {
            Undo.RecordObject(this, "Remove Enablee Link");
            enablees.Remove(childID);
            EditorUtility.SetDirty(this);
        }
        public void AddDisablee(string childID)
        {
            Undo.RecordObject(this, "Add Disablee Link");
            disablees.Add(childID);
            EditorUtility.SetDirty(this);
        }

        public void RemoveDisablee(string childID)
        {
            Undo.RecordObject(this, "Remove Disablee Link");
            disablees.Remove(childID);
            EditorUtility.SetDirty(this);
        }




#endif
    }
}
