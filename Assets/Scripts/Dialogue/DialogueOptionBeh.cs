using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Dialogue
{
    public class DialogueOptionBeh : MonoBehaviour
    {

        public DialogueNode myNode;

        public void OnBtnClicked()
        {
            Conversant.Instance.NodeClicked(myNode);
        }
    }
}
