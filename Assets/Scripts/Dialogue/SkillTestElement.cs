using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RPG.Dialogue
{

    public class SkillTestElement : MonoBehaviour
    {
        public int skillNumber;
        public float chance;
        public bool absolute;

        
        public SkillTestElement(int _skill, float _chance, bool _abs)
        {
            skillNumber = _skill;
            chance = _chance;
            absolute = _abs;

        }
    }
}