using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CreateAssetMenu(fileName = "New Dockable", menuName = "Dockable", order = 0)]
public class Dockable : ScriptableObject
{
    [SerializeField] public List<invlist> inv = new List<invlist>();
    [SerializeField] public List<int> notBuying = new List<int>();


    [Serializable]
    public struct invlist
    {
        public int id;
        public int quantity;

    }


}
