using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Drogowskaz : MonoBehaviour
{
    [SerializeField] MeshRenderer mr;
    [SerializeField] Material mat_gibberish;
    [SerializeField] Material mat_legible;
    [SerializeField] int legibilityDifficulty = 3;
    [SerializeField] int skilltotest;


    void Awake()
    {
        Humans.OnCrewChange += CrewChange;
    }

    private void OnDestroy()
    {
        Humans.OnCrewChange -= CrewChange;

    }


    void Start()
    {
        mr.material = mat_gibberish;

    }

    private void CrewChange(string name) // we don't really need name here.
    {
        Invoke("CheckCrewSkills", 1f);
    }

    private void CheckCrewSkills()
    {
        if (PlayerCharacter.Instance.GetSkillValue(skilltotest) >= legibilityDifficulty)
        {
            mr.material = mat_legible;
        } else
        {
            
            mr.material = mat_gibberish;
            
        }
    }

}
