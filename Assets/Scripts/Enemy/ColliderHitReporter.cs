using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderHitReporter : MonoBehaviour
{
    [SerializeField] EnemyShooterBeh Listener;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.name == "Player")
        {
            // transform.localPosition.Set(0,0,0);

            transform.localRotation = transform.parent.localRotation;
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<Rigidbody>().detectCollisions = false;
        }
    }
}
