using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Contact 
{



    public Transform trans;
    // public string fingerprint;
    public Vector3 contactCener;
    public float precisionRange; // a sphere in which the contact may be, we suspect
    public int time;
    public float threat; // up to 1 is manageable. Over 1 is too risky.
    public float damageDealt; // how much damage received from this contact.
    public enum DetectionMethod { Heat, Visual, Astral, Intel, Other }
    public DetectionMethod detectionMethod;
    public Contact(Transform trans, Vector3 contactCener, float precisionRange, int time, float threat, DetectionMethod detectionMethod, float damageDealt = 0)
    {
        this.trans = trans;
        this.contactCener = contactCener;
        this.precisionRange = precisionRange;
        this.time = time;
        this.threat = threat;
        this.damageDealt = damageDealt;
        this.detectionMethod = detectionMethod;
    }

    
}
