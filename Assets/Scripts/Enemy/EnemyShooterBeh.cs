using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using GameDevTV.Saving;

public class EnemyShooterBeh : MonoBehaviour, GameDevTV.Saving.ISaveable
{
    private float deadDrag = 100f;
    private float aliveDrag = 0f;

    [SerializeField] private Transform turretCore;
    //[SerializeField] private Vector3 aimAt; // what we're aiming weapons at
    [SerializeField] private Health health;
    [SerializeField] bool debugToConsole = false;


    private enum Mission {Transport, Scavenge, Patrol, Recon, Sentry, Assault, Support, Ambush } // {Attack, Hide, Search, Evade, DistanceIncrease, DistanceDecrease, Idle }
    [SerializeField] Mission mission;
    private enum Intent { Idle, Attack, Escape, Sneak, Travel, Search, BackOut }
    [SerializeField] Intent intent;
    [SerializeField] Transform destination;
    [SerializeField] Transform aimAtRepresentation;
    private float defaultPrecision = 1f; // taki placeholder initial value
    private Clock clock;

    Contactable.Faction faction;
    [SerializeField] private float listeningRange = 30;
    [SerializeField] private float MaxHeat = 20; // heat value after which shooting is no longer possible
    public float reload;
    public float elevation; // x axie 10 deg to -15 up
    public int ammo;
    private float elevationMin = -10;
    private float elevationMax = 5; // that's barrel down, mind you!
    private float accellerationConstantCoefficient = 1f;
    private float currentAimPrecision =0;
    [SerializeField] GameObject projectile;
    [SerializeField] private Transform muzzle;
    [SerializeField] private ParticleSystem muzzleFlash;
    [SerializeField] private float PlayersAwareness; // do I know where the player is? 10 is doubting, 0 is dormant, 20 is searching, 30 is fix .. or something like that
    [SerializeField] private Vector3 PlayersLastLocation;
    [SerializeField] private Transform EscapePoint;
    [SerializeField] List<Transform> PatrolRoutePoints = new List<Transform>();
    [SerializeField] private int currentPatrolPoint;
    [SerializeField] private float attackRange = 7;
    [SerializeField] private string ContactRaycasting;
    private float turretSearchSwivelRange = 0.25f;
    private bool turretSwivelEnabled = false;
    [SerializeField] Light szperacz;
    private float acceptableEscapeHomeDistance = 5;

    [SerializeField] bool testRun;
    [SerializeField] float distanceToPlr;
    //[SerializeField] Transform hitpoint; // only for debug to see where it's aiming
    private NavMeshAgent navAgent;
    [SerializeField] float AttackSpeed;

    [SerializeField] float SpeedMultiplier = 4; // this is to compensate as playr speed is different from enemies and I want to keep the gears the same.
    private float[] speeds = { -0.5f, -0.3f, 0f, 0.2f, 0.5f, 0.9f, 1f };
    private int gear = 2;
    private string debugConsoleString = "";

    /* this enemy will need waypoints and a list (stack?) of tasks (FIFO)
     * The tasks should be like flags that tell him what he's doint now.
     * there needs to be Attack, Hide, Search, Evade, DistanceIncrease or Decrease,
     * and that's it, I guess.
     * */

    private bool activated = false;
    private bool dead = false;
    [SerializeField] List<Transform> disableOnDead = new List<Transform>();
    [SerializeField] Transform turretDead;
    [SerializeField] GameObject explosionPrefab;
    [SerializeField] Transform flames;
    private RigidbodyConstraints aliveConstraints;
    private RigidbodyConstraints deadConstraints;

    private List<Contact> contacts = new List<Contact>();
    //. private Contact nullContact;

    // treat global vars
    private Contact highestThreatContact;
    private float highestThreat;
    private Contact closestThreatContact;
    private float closestThreat;
    private Contact target; // what we're aiming weapons at, because AI may decide to fire at another one depending on intent.




    private void Awake() // must be awake to inputs subscrive
    {
        GameManager.OnTestScenarioChange += TestScenarioHandler;
        Health.OnDeath += DeathHandle;
        Health.OnHit += HealthHitHandle;
        deadConstraints = RigidbodyConstraints.None;
        aliveConstraints = GetComponent<Rigidbody>().constraints;

    }

    private void OnDestroy()
    {
        GameManager.OnTestScenarioChange -= TestScenarioHandler;
        Health.OnDeath -= DeathHandle;
        Health.OnHit -= HealthHitHandle;
    }

    void Start()
    {
        // nullContact = new Contact(transform, Vector3.zero, 0, 0, 0, Contact.DetectionMethod.Other);
        clock = GameManager.Instance.GetComponent<Clock>();
        intent = Intent.Idle; // SwitchIntent(Intent.Idle); // to avoid errors befor e navmesh
        health = transform.GetComponent<Health>();

        if (!activated && (GameManager.Instance.isHexaWorld)) StartCoroutine(ActivationChecks());
        if (!activated && !GameManager.Instance.isHexaWorld)
        {
            // hexless activation
            GetComponent<Rigidbody>().drag = aliveDrag;
            navAgent = GetComponent<NavMeshAgent>();
            navAgent.enabled = true;
         
            navAgent.isStopped = true;
            activated = true;
        }

        currentPatrolPoint = 0;
        SetDestinationTransformProperties(PatrolRoutePoints[currentPatrolPoint]);
        StartCoroutine(ComputationalBusiness());
        StartCoroutine(MechanicalBusiness());
        faction = GetComponent<Contactable>().GetFaction();
    }




    // Update is called once per frame
    void Update()
    {
        if ((GameManager.Instance.stage == GameManager.Stage.game) && (GameManager.Instance.isMovementEnabled) && activated && !dead) // 
        {
            // process passive sensors and gather environment data: is done in the coroutine
            debugConsoleString = "";

            // act on mission 
            // Mission is not active. it's a passive decision factor. These will be taken on events or on idle.
            // Scratch that. Mission switches between intents.
            switch (mission)
            {
                case Mission.Ambush:
                    MissionAmbush();
                    break;
                case Mission.Patrol:
                    MissionPatrol();
                    break;

            }

            // act on intent
            switch (intent)
            {
                case Intent.Idle:
                    IntentIdle();
                    break;
                case Intent.Attack:
                    IntentAttack();
                    break;
                case Intent.Sneak:
                    IntentSneak();
                    break;
                case Intent.Travel:
                    IntentTravel();
                    break;
                case Intent.Search:
                    IntentSearch();
                    break;
                case Intent.Escape:
                    IntentEscape();
                    break;
                case Intent.BackOut:
                    IntentBackOut();
                    break;

            }



            // if (PlayersAwareness > 10) ChasePlayer();
            // TurretOperations(); // removed && (!GameManager.Instance.isDocked) && (!GameManager.Instance.isDocking) because docking should allow reloading and such. 

            // PerformAIFunctions();
            if (PlayersAwareness > 0 ) PlayersAwareness -= Time.deltaTime;

            if (debugToConsole) DebugConsoleVisibility();
        } 
    }

    private void DebugConsoleVisibility()
    {
        debugConsoleString += "myHeat " + GetHeat()  + "<br>";
        debugConsoleString += "sensorrng " + (listeningRange - GetHeat()) + "<br>";
        
        foreach (Contact _c in contacts)
        {
            debugConsoleString += _c.trans.name + "<br>thr:" + _c.threat + "<br>age:" + (clock.GetIntDate() - highestThreatContact.time) + "<br>" + _c.detectionMethod.ToString() + "<br>aimp:" + currentAimPrecision + "<br>PrecRang:" + _c.precisionRange.ToString()+ "<br><br>";
        }
        GameManager.Instance.UpdateDebugConsole(debugConsoleString);

    }

    #region mission actions
    // ----------------------------------------------------------------------------------------------------------------------
    // ------------------------                     mission actions                            ------------------------------
    // ----------------------------------------------------------------------------------------------------------------------

    private void MissionAmbush()
    {
        if (health.GetHealth() < 0.3f || highestThreat > 1.6 || intent == Intent.Escape) // I'll derail this condition with intent == escape
        { if (intent != Intent.Escape && Vector3.Distance(transform.position, EscapePoint.position) > acceptableEscapeHomeDistance + 1) SwitchIntent(Intent.Escape); } // escape if things get tough
        else
        {
            if (contacts.Count > 0)
            {
                Contact priorityC = highestThreatContact; // search for highest threat.
                if (priorityC.threat > 0)
                {
                    debugConsoleString += "PrioC "+priorityC.trans.name+"<br>";


                    if (priorityC.precisionRange > 3 || clock.GetIntDate() - priorityC.time > 100000)  // if stale (Destination last known location (or prediction)) intent search
                    {
                        if (Vector3.Distance(priorityC.contactCener, transform.position) > 2) // it's far enough. There is a point in lookking for it.
                        {
                            destination.position = priorityC.contactCener;
                            destination.rotation = transform.rotation;
                            target = priorityC;
                            SwitchIntent(Intent.Search);
                        }
                        else
                        {
                            // they must be gone by now. 
                            // maybe some search pattern here?
                            contacts.Remove(priorityC);
                            target = null;
                            SwitchIntent(Intent.Idle);
                            Debug.Log("Idled due to contact quality");
                        }
                    }
                    else
                    {   //attack highest threat - we have a pretty good idea what it is
                        target = priorityC;

                        // check attackRange.
                        if (attackRange > Vector3.Distance(priorityC.contactCener, transform.position)) // if in weapons' range
                        {
                            // check raycast visibility. 
                            Vector3 direction = (priorityC.contactCener - muzzle.position).normalized;
                            Ray ray = new Ray(muzzle.position, direction);
                            RaycastHit hit;
                            if (Physics.Raycast(ray, out hit, attackRange)) // should have targetheat of some sort instead of attackRange
                            {
                                turretSwivelEnabled = false;
                                ContactRaycasting = hit.transform.name;
                                if (hit.transform == priorityC.trans) SwitchIntent(Intent.Attack);
                            }
                        }
                        else
                        {
                            // come closer!
                            destination.position = priorityC.contactCener;
                            destination.rotation = Quaternion.LookRotation(new Vector3(priorityC.contactCener.x, transform.position.y, priorityC.contactCener.z) - transform.position);
                            target = priorityC;
                            SwitchIntent(Intent.Travel);
                        }
                    }
                }
            }
            else
            {
                target = null;
                turretSwivelEnabled = false;
                // no contacts - continue patrol.
                // in practice, search should expire and set to Idle
            }



            // last but not least, if youy're still doiung nothing
            if (intent == Intent.Idle)        // check if turned to next leg done in pre-idle.
            {

                // waiting for new destination assignment
                if (PatrolRoutePoints.Count > 0)
                {
                    if (Vector3.Distance(PatrolRoutePoints[0].position, transform.position) > 3)
                    {
                        currentPatrolPoint = 0; // I shouldn't need it really
                        target = null;
                        turretSwivelEnabled = false;
                        SetDestinationTransformProperties(PatrolRoutePoints[0]);
                        SwitchIntent(Intent.Sneak);
                    }
                }
            }
        }
    }
    private void MissionPatrol()
    {
        if (health.GetHealth() < 0.1f || highestThreat > 1 || intent == Intent.Escape) // I'll derail this condition with intent == escape
        { if (intent != Intent.Escape && Vector3.Distance(transform.position, EscapePoint.position) > acceptableEscapeHomeDistance +1 )  SwitchIntent(Intent.Escape); } // escape if things get tough
        else
        {
            if (contacts.Count > 0)
            {
                Contact priorityC = highestThreatContact; // search for highest threat.
                if (priorityC.threat > 0)
                {
                    if (priorityC.precisionRange > 3 || clock.GetIntDate() - priorityC.time > 100000)  // if stale (Destination last known location (or prediction)) intent search
                    {
                        if (Vector3.Distance(priorityC.contactCener, transform.position) > 2) // it's far enough. There is a point in lookking for it.
                        {
                            destination.position = priorityC.contactCener;
                            destination.rotation = transform.rotation;
                            target = priorityC;
                            SwitchIntent(Intent.Search);
                        } else
                        {
                            // they must be gone by now. 
                            // maybe some search pattern here?
                            contacts.Remove(priorityC);
                            target = null;
                            SwitchIntent(Intent.Idle);
                        }
                    }
                    else
                    {   //attack highest threat - we have a pretty good idea what it is
                        target = priorityC;

                        // check attackRange.
                        if (attackRange > Vector3.Distance(priorityC.contactCener, transform.position)) // if in weapons' range
                        {
                            // check raycast visibility. 
                            Vector3 direction = (priorityC.contactCener - muzzle.position).normalized;
                            Ray ray = new Ray(muzzle.position, direction);
                            RaycastHit hit;
                            if (Physics.Raycast(ray, out hit, attackRange)) // should have targetheat of some sort instead of attackRange
                            {
                                turretSwivelEnabled = false;
                                ContactRaycasting = hit.transform.name;
                                if (hit.transform == priorityC.trans) SwitchIntent(Intent.Attack);
                            }
                        }
                        else
                        {
                            // come closer!
                            destination.position = priorityC.contactCener;
                            destination.rotation = Quaternion.LookRotation(new Vector3(priorityC.contactCener.x, transform.position.y, priorityC.contactCener.z) - transform.position);
                            target = priorityC;
                            SwitchIntent(Intent.Travel);
                        }
                    }
                }
            }   else
            {
                target = null;
                turretSwivelEnabled = true;
                // no contacts - continue patrol.
                // in practice, search should expire and set to Idle
            }



            // last but not least, if youy're still doiung nothing
            if (intent == Intent.Idle)        // check if turned to next leg done in pre-idle.
            {

                // waiting for new destination assignment
                if (PatrolRoutePoints.Count > 0)
                {
                    if (Vector3.Distance(PatrolRoutePoints[currentPatrolPoint].position, transform.position) < 3)
                    {
                        Debug.Log("Incrementing partol route destination");
                        currentPatrolPoint++;        // increment
                        if (currentPatrolPoint > PatrolRoutePoints.Count - 1) currentPatrolPoint = 0;
                    } else
                    {
                        Debug.Log("Resuming partol");
                    }
                    target = null;
                    turretSwivelEnabled = true;
                    SetDestinationTransformProperties(PatrolRoutePoints[currentPatrolPoint]);
                    SwitchIntent(Intent.Travel);
                }
            }
        }
    }


    #endregion

    #region intent actions
    // ----------------------------------------------------------------------------------------------------------------------
    // ------------------------                     intent actions                             ------------------------------
    // ----------------------------------------------------------------------------------------------------------------------

    private void SwitchIntent(Intent i) // intents have stuff to do on start. this method handles that.
    {
        if (intent == i) return; // this shouldn't be really happening if I knew what I'm doing
        // act on intent
        switch (i)
        {
            case Intent.Idle:
                navAgent.isStopped = true;
                break;
            case Intent.Attack:
                
                break;
            case Intent.Travel:
                target = null;
                break;

            case Intent.Sneak:
                target = null;
                // switch off lights and decrease speed.
                break;

            case Intent.Search:
                turretSwivelEnabled = true;
                GoToPoint(destination.position, 5);
                break;
            case Intent.Escape:
                turretSwivelEnabled = false;
                SetDestinationTransformProperties(EscapePoint);
                GoToPoint(destination.position, 6);            // proceed to destination
                // Debug.Log(transform.name + " is escaping!");
                break;

            case Intent.BackOut:
                BackOut(5f);
                break;

        }
        Debug.Log(transform.name +" is switching intent from " + intent + " to " + i);
        intent = i;
    }

    private void IntentIdle()
    {

        // if health !critical, if fuel !critical, if ammo !critical, 
        // I should check if I don't have something better to do.
        // else remain in base hidden. or Escape?
        // this should be decided by mission
        if (!navAgent.isStopped) navAgent.isStopped = true;
    }

    private void IntentAttack()
    {
        // aim at target
        // if aimed and in range, fire
        // else if !inrange get closer
        // if smart enough, evade

        // if target no longer visible (what does this mean?!) change intent to search
        if (!IsContact(target.trans))
        {
            target = null;
            SwitchIntent(Intent.Idle);
        }
        if (Vector3.Distance(target.contactCener, transform.position) < attackRange)
        {
            navAgent.isStopped = true;
            destination.position = target.contactCener;
            TurnTowardsTarget();

            if (Vector3.Distance(target.contactCener, transform.position) < attackRange / 2) BackOut(5f*Time.deltaTime);

            if (currentAimPrecision < 5)
            {
                if (reload <= 0 && ammo > 0 && GetHeat() < MaxHeat)
                {
                    FireProjectile();
                }
            }
        } else
        {
            // outside the range. Decrease distance
            GoToPoint(target.contactCener, 5);
        }
    }

    private void IntentEscape()
    {
        // set destination to base
        // flank speed and enable navmeshagent
        // if reached the destination, intent idle
        // normal speed
        if (Vector3.Distance(transform.position, destination.position) < acceptableEscapeHomeDistance) // check if point reached.
        {
            // Debug.Log("Distance to Escape point was " + Vector3.Distance(transform.position, destination.position));
            if (TurnToDestinationRotation()) SwitchIntent(Intent.Idle);
        }

    }

    private void IntentSneak()
    {
        // set gear to crawl
        // stow turret
        // light off
        // proceed to destination

        if (Vector3.Distance(transform.position, destination.position) < 2f) // check if point reached.
        {
            if (TurnToDestinationRotation())
            {
                SwitchIntent(Intent.Idle);
            }
        }
        else
        {
            if (navAgent.isStopped) GoToPoint(destination.position, 4);            // proceed to destination
        }

    }

    private void IntentTravel()
    {
        // normal speed
        if (Vector3.Distance(transform.position, destination.position) < 2f) // check if point reached.
        {
            if (TurnToDestinationRotation())
            {
                SwitchIntent(Intent.Idle);
            }
        }  else
        {
            if (navAgent.isStopped) GoToPoint(destination.position,6);            // proceed to destination
        }

    }


    private void IntentBackOut()
    {
        navAgent.isStopped = true;
        BackOut(3f * Time.deltaTime);
    }

    private void IntentSearch()
    {
        // turret and light enabled
        // scan with the turret light.

        // normal speed
        if (Vector3.Distance(transform.position, destination.position) < 2f) // check if point reached.
        {
            //if (TurnToDestinationRotation())
            //{
                SwitchIntent(Intent.Idle);
            //}
        }

    }


    #endregion

    private IEnumerator ComputationalBusiness()
    {
        float computationalDelay = GameManager.Instance.computationalDelay;
        // this COROUTINE reads in realtime (well, almost; hopefully (see waitforseconds)) all the NPCs senses. 
        while (true) 
        {
            yield return new WaitForSeconds(computationalDelay);

            if ((GameManager.Instance.stage == GameManager.Stage.game) && activated && !dead)
            {
                PassiveSensorsRead();
                if (contacts.Count > 0) ThreatAssessment();
                UpdateHeat(computationalDelay);
            }
        }
    }

    private IEnumerator MechanicalBusiness()
    {
        while (true)
        {

            if ((GameManager.Instance.stage == GameManager.Stage.game) && activated && !dead)
            {
                if (target != null && highestThreat > 0) {
                    AimTurret(); // have to aim at the target
                }
                else
                {
                    if (turretSwivelEnabled)
                    {
                        AimTurretLeftRight(turretSearchSwivelRange);
                        AimTurret(); // have to aim somewhere
                        if (currentAimPrecision < 5) turretSearchSwivelRange = -turretSearchSwivelRange;
                    } else
                    {
                        AimTurretForward();
                        AimTurret();
                    }
                }

                if (reload > 0) reload -= 1f * Time.deltaTime;

            }
            yield return null;

        }

    }

    private void PassiveSensorsRead()
    {
        // sphere of sensitivity read
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, listeningRange - GetHeat());
        foreach (var hitCollider in hitColliders)
        {
            Contactable newContact = null;
            if (hitCollider.CompareTag("Player")) newContact = hitCollider.transform.GetComponent<Contactable>();
            if (hitCollider.CompareTag("NPCVehicle")) newContact = hitCollider.transform.parent.parent.transform.GetComponent<Contactable>();


            if (newContact  != null)
            {
                Transform _t = newContact.transform;
                if (IsContact(newContact.transform))
                {
                    if (newContact.isDead)
                    {
                        RemoveContact(_t);
                        return;
                    }
                    // we already have this one as a contact. Here we update the values in Contacts ...why am I writing all that for enemy AI?!
                    Contact _c = GetContact(newContact.transform);
                    _c.contactCener = _t.position;
                    _c.time = clock.GetIntDate();


                }
                else
                {
                    // new contact
                    if (newContact != GetComponent<Contactable>() && !newContact.isDead)
                    {
                        float _threat = InitialThreatAssessment(newContact.GetFaction());
                        contacts.Add(new Contact(newContact.transform, _t.position, defaultPrecision, clock.GetIntDate(), _threat, Contact.DetectionMethod.Heat));
                    }
                }
            }
        }

        //  turret's eyes?
    }

    private void UpdateHeat(float computationalDelay)
    {
        float baseheat = (Mathf.Abs(gear - 2) * 2) * (navAgent.isStopped ? 0 : 1) +2f; // 2 being idle body heat and such
        float _h = Mathf.Max(GetComponent<Contactable>().heat - baseheat, 0);
        float heatdecremenator = _h / computationalDelay * 0.002f; // GetComponent<Contactable>().heat; //* 0.5f; ...I'm gonna get div by zero one of these days. Tellinya.
        if (_h > 0) _h -= heatdecremenator * computationalDelay; //0.3f is the waitforseconds.
        GetComponent<Contactable>().heat = _h + baseheat;
    }

    private void AddHeat(float _h)
    {
        GetComponent<Contactable>().heat = GetHeat() + _h;
    }

    private float GetHeat()
    {
        return GetComponent<Contactable>().heat;
    }


    private void ThreatAssessment()
    {
        /* THis one will be returning:
         * Highets threat contact
         * highest threat
         * closest contact
         * hybrid between these two, perhaps based on some cleverness.
         * 
         * 
         * it cannot return anything very dynamic, like aimAt.
         * ani zadnej rzeczy, ktora jego jest.
         */

        // nullify!
        closestThreatContact = null;
        closestThreat = 100;
        highestThreatContact = null;
        highestThreat = 0;

        // assign!
        float _thr = 0;
        float _range = 100;
        foreach (Contact c in contacts)
        {
            if (c.threat >= _thr)
            {
                highestThreatContact = c;
                highestThreat = c.threat;
                _thr = c.threat;
            }
            float _rangec = Vector3.Distance(c.contactCener, transform.position);
            if (_rangec < _range)
            {
                closestThreatContact = c;
                _range = _rangec;
                closestThreat = _rangec;
            }
        }

    }

    private bool IsContact(Transform _t)
    {
        foreach(Contact c in contacts)
        {
            if (c.trans == _t) return true;
        }
        return false;
    }

    private Contact GetContact(Transform _t)
    {
        foreach (Contact c in contacts)
        {
            if (c.trans == _t) return c;
        }
        return null;
    }

    private void RemoveContact(Transform _tc) // should be used for dead contacts only. You don't want to forget hurts.
    {
        foreach (Contact c in contacts)
        {
            if (c.trans == _tc)
            {
                contacts.Remove(c);
                return;

            }

        }
    }



    private void SetDestinationTransformProperties(Transform _t) // am I not overcomplicationg something simple?
    {
        destination.position = _t.position;
        destination.rotation = _t.rotation;
    }


    public void AddAwareness(float awa)
    {
        PlayersAwareness += awa;
    }

    private void TurretOperations()
    {


        Vector3 currentTransPos = new Vector3(transform.position.x, 1, transform.position.z);
        Vector3 currentPlayerPos = new Vector3(PlayerMovement.Instance.transform.position.x, 1, PlayerMovement.Instance.transform.position.z);

        /*
        distanceToPlr = Vector3.Distance(currentTransPos, currentPlayerPos);
        if (distanceToPlr < targetHeat) // Is it a hot enough target? 
        {
            Vector3 direction = (currentPlayerPos - currentTransPos).normalized;
            Ray ray = new Ray(currentTransPos, direction);
            RaycastHit hit;
            // Debug.DrawRay(currentTransPos, direction, Color.red, 10f);
            // is the player visible? 
            if (Physics.Raycast(ray, out hit, targetHeat))
            {
                if (hit.transform.name == "Player")
                {
                    PlayersAwareness = Mathf.Max(50, PlayersAwareness);
                    aimAt = new Vector3(hit.point.x, hit.point.y, hit.point.z);
                    if (health.GetHealth() > 0.2f) { SwitchIntent(Intent.Attack; } else { SwitchIntent(Intent.Escape; }
                    PlayersLastLocation = PlayerMovement.Instance.transform.position;

                }

                //  Debug.Log(hit.transform.name + "    " + hit.point.x + ", " + hit.point.y + ", " + hit.point.z);
                //hitpoint.position = hit.point;  // only for debug to see where it's aiming
            }
            else
            {
                PlayersAwareness = Mathf.Max(29, PlayersAwareness);
                // TODO act and search?
                    
                if (health.GetHealth() < 0.2f) SwitchIntent(Intent.Escape;
            }
        }
        else
        {


            // PlayersAwareness = Mathf.Max(24, PlayersAwareness);

            if (health.GetHealth() < 0.2f) SwitchIntent(Intent.Escape; ;
        }
        */

    }

 
    private float InitialThreatAssessment(Contactable.Faction f)
    {
        float _threat = 0;
        if (faction == Contactable.Faction.Pirates && f != Contactable.Faction.Pirates) // pirates attack everything but other pirates.
        {
            _threat += 0.1f;
            if (f == Contactable.Faction.Peacekeepers) _threat += 0.7f; // will try to attack peacekeepers, but without much conviction.
        }
        if (faction == Contactable.Faction.Peacekeepers && f == Contactable.Faction.Pirates) _threat += 0.1f;
        if (faction == Contactable.Faction.Neutrals && f == Contactable.Faction.Pirates) _threat += 1.1f; // avoid conflict and run
        return _threat;
    }

    




    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Enemy Tank collided with "+collision.collider.ToString());

    }

    private IEnumerator ActivationChecks()
    {
        while (!GetComponentInParent<HexTile>().GetActive())
        {
            yield return new WaitForSeconds(0.1f);
        }
            if (GetComponentInParent<HexTile>().GetActive())
        {
            GetComponent<Rigidbody>().drag = aliveDrag;
            navAgent = GetComponent<NavMeshAgent>();
            navAgent.enabled = true;
            // GetComponent<NavMeshAgent>().enabled = true;
            navAgent.isStopped = true;
            activated = true;
        }
        yield return null;
    }

    /*
    Transform GetClosestPoint(Transform[] enemies)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (Transform potentialTarget in enemies)
        { 
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }

        return bestTarget;
    }

    // no idea what this code was for.
    // it came from the sponger.

    

    private void PerformAIFunctions()
    {
        switch (intent)
        {
            case Intent.Attack:

                break;
            case Intent.Search:
                GoToPoint(PlayersLastLocation);
                break;
            case Intent.Idle:
                navAgent.isStopped = true;
                
                break;
        }
    }
    */
    private bool TurnToDestinationRotation()
    {
        float divergence = Vector3.Angle(destination.forward, transform.forward);
        if (divergence > 5f)  // we're not done with the rotation
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, destination.rotation, accellerationConstantCoefficient * Time.deltaTime);
        }
        else
        {
            return true;
        }
        return false; // we're still rotating the vehicle.
    }

    private bool TurnTowardsTarget()
    {
        float divergence = Vector3.Angle(destination.forward, transform.forward);
        if (divergence > 5f)  // we're not done with the rotation
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target.contactCener), accellerationConstantCoefficient * Time.deltaTime);
        }
        else
        {
            return true;
        }
        return false; // we're still rotating the vehicle.
    }



    private void GoToPoint(Vector3 _v, int _gear = 6) 
    {
        // Debug.Log("Setting destination" + currentPatrolPoint);
        navAgent.SetDestination(_v);
        navAgent.isStopped = false;
        SetNavAgentSpeed(_gear);
        destination.position = _v;
    }

    private void SetNavAgentSpeed(int _gear)
    {
        // todo here add gears
        
        navAgent.speed = speeds[_gear] * SpeedMultiplier;
        gear = _gear;
    }

    private void BackOut(float _v)
    {
        navAgent.Move(-transform.forward * _v);
        navAgent.isStopped = false;
    }


    private void AimTurretForward()
    {
        // so the turret can aim nowhere for illumination purposes only
        aimAtRepresentation.position = transform.position + transform.forward;
    }

    private void AimTurretLeftRight(float r) // r should be - or + (I'm thingking 0.25 is enough.
    {
        //if (target == null) 
        aimAtRepresentation.position = transform.position + transform.forward + ((transform.right * r));
    }

    private void AimTurret()
    {
        // we are going to return, in a way, currentAimPrecision. By assinging.
        Vector3 aimAt; //  = transform.position + transform.forward;
        if (target != null) { aimAt = target.contactCener; } else { aimAt = aimAtRepresentation.position; }
        // aimAtRepresentation.position = aimAt;
        Vector3 targetRotation = new Vector3(aimAt.x, turretCore.position.y, aimAt.z) - turretCore.position;
        // Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;  // this seems obsolete.
        currentAimPrecision = Vector3.Angle(targetRotation, turretCore.forward);
        if (currentAimPrecision > 1f)  // we're not done with the rotation
        {
            turretCore.rotation = Quaternion.Slerp(turretCore.rotation, Quaternion.LookRotation(targetRotation), accellerationConstantCoefficient * Time.deltaTime);
        } 
    }

    private void FireProjectile()
    {
        Debug.Log("Bang!");
        reload = 3f;
        AddHeat(1f);
        ammo--;
        
        // I hope this was player only....
        // GUIHandler.Instance.ProjectileFired(heat, ammo, reload, GetAmmoName()); // this method will pull all sorts of public vars from

        Quaternion q = Quaternion.Euler(muzzle.rotation.eulerAngles.x, muzzle.rotation.eulerAngles.y, muzzle.rotation.eulerAngles.z);
        GameObject proj = Instantiate(projectile, muzzle.position, q, null);
        proj.GetComponent<ProjectileBeh>().damagePower = 0.1f;
        proj.GetComponent<ProjectileBeh>().SetOriginator(transform);
        // Debug.Log("rotation of the projectile iiiis " + q);
        Rigidbody projRB = proj.GetComponent<Rigidbody>();
        projRB.AddRelativeForce(Vector3.forward * 250f);   // AddForce(Vector3.forward*1000f);
        muzzleFlash.Play();
    }





    private void DeathHandle()
    {
        if (health.GetHealth() <= 0 && !dead)
        {
            Vector3 yoffset = new Vector3(0, 0.72f, 0);
            GetComponent<Rigidbody>().drag = deadDrag;
            GetComponent<Rigidbody>().constraints = deadConstraints;
            Debug.Log("deading: "+transform.name);
            navAgent.isStopped = true;
            dead = true;
            GetComponent<Contactable>().isDead = true;
            turretDead.gameObject.SetActive(true);
            turretDead.transform.localPosition = disableOnDead[0].transform.localPosition;
            turretDead.transform.eulerAngles = disableOnDead[0].transform.eulerAngles;
            foreach (Transform _t in disableOnDead)
            {
                _t.gameObject.SetActive(false);
            }
            
            turretDead.transform.SetParent(transform.parent);
            flames.gameObject.SetActive(true);
            GameObject _explosion = Instantiate(explosionPrefab, transform.position+ yoffset, Quaternion.identity, null);
            turretDead.GetComponent<Rigidbody>().AddExplosionForce(10, transform.position + yoffset, 2, 0.5f);
            
        } 
    }

    private void HealthHitHandle(Health h, Transform lastHitTransform)
    {
        if (h == health) // if it's my health that's hit, do.
                         // no idea how other way capture publisher of the event.
        {
            Contact _c;

            if (!IsContact(lastHitTransform))
            {
                if (lastHitTransform == transform) return;
                Debug.Log(transform.name + " has been ambushed like a fish! Trying to get fingerprint of " + h.lastHitFingerprint);
                _c = new Contact(lastHitTransform, GameManager.Instance.GetTransformByFingerprint(h.lastHitFingerprint).position, defaultPrecision + 2, clock.GetIntDate(), InitialThreatAssessment(lastHitTransform.GetComponent<Contactable>().GetFaction())+0.1f, Contact.DetectionMethod.Heat);
                contacts.Add(_c);
            } else { 
            _c = GetContact(lastHitTransform);
            Debug.Log(transform.name + " received a hit! (Health event captured by EnemyShooterBeh) " + health.lastHitFingerprint + " increasing its threat by " + h.lastHitDamage);
            }
            _c.threat += h.lastHitDamage / health.GetHealth(); // this is supposed to make each hit more threatening.
            _c.damageDealt += health.lastHitDamage; // we're registering in case the AI chooses to make objective threat assessment or fire on the most dangerous enemy while escaping.
        }
    }


    private void RestoreToLifeHandle()
    {
        if (health.GetHealth() > 0 && dead) // checking just in case
        {
            Vector3 yoffset = new Vector3(0, 0.72f, 0);
            GetComponent<Rigidbody>().drag = aliveDrag;
            GetComponent<Rigidbody>().constraints = aliveConstraints;
            Debug.Log("aliving: " + transform.name);
            navAgent.isStopped = false;
            dead = false;
            turretDead.transform.localPosition = disableOnDead[0].transform.localPosition;
            turretDead.transform.eulerAngles = disableOnDead[0].transform.eulerAngles;
            foreach (Transform _t in disableOnDead)
            {
                _t.gameObject.SetActive(true);
            }

            turretDead.transform.SetParent(transform.parent);

            turretDead.gameObject.SetActive(false);
            flames.gameObject.SetActive(false);

        }
    }


    private void TestScenarioHandler(int test)
    {
        if (test == 2) // we know where the player is and we can't be deceived.
        {
            PlayersAwareness = 100000;
        }
    }

    public object CaptureState()
    {
        Dictionary<string, object> savedata = new Dictionary<string, object>();

        savedata["position"] = new SerializableVector3(transform.position);
        savedata["rotation"] = new SerializableVector3(transform.eulerAngles);
        savedata["dead"] = dead;
        savedata["activated"] = activated;
        savedata["currentPatrolPoint"] = currentPatrolPoint;
        savedata["PlayersLastLocation"] = new SerializableVector3(PlayersLastLocation);
        savedata["PlayersAwareness"] = PlayersAwareness;
        savedata["ammo"] = ammo;
        savedata["elevation"] = elevation;
        savedata["reload"] = reload;
        //savedata["heat"] = heat;

        return savedata;
    }

    public void RestoreState(object state)
    {

        Dictionary<string, object> savedata = (Dictionary<string, object>)state;

        transform.position = ((SerializableVector3)savedata["position"]).ToVector();
        transform.eulerAngles = ((SerializableVector3)savedata["rotation"]).ToVector();
        // dead = (bool)savedata["dead"]; // this should be restored in restore to life handle
        activated = (bool)savedata["activated"];
        currentPatrolPoint = (int)savedata["currentPatrolPoint"];
        PlayersLastLocation = ((SerializableVector3)savedata["PlayersLastLocation"]).ToVector();
        PlayersAwareness = (float)savedata["PlayersAwareness"];
        ammo = (int)savedata["ammo"];
        elevation = (float)savedata["elevation"];
        reload = (float)savedata["reload"];
        //heat = (float)savedata["heat"];

        RestoreToLifeHandle();
    }
}
