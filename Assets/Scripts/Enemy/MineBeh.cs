using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevTV.Saving;

public class MineBeh : MonoBehaviour, GameDevTV.Saving.ISaveable
{
    [SerializeField] float targetHeat; // serialising for visibility
    [SerializeField] float triggerthreshold;
    [SerializeField] string PlayerRay;
    [SerializeField] Light attackLight;
    [SerializeField] bool testRun;
    [SerializeField] float distanceToPlr;
    //[SerializeField] Transform hitpoint; // only for debug to see where it's aiming
    [SerializeField] float AttackSpeed;
    [SerializeField] WarningLightSwapper warn;
    [SerializeField] float sensitivity; // 0 = defused, 1 = sensitivity of a spider 
    [SerializeField] float blastRadius;
    [SerializeField] float damageStrength;
    [SerializeField] GameObject explosionPrefab;
    Rigidbody rb;
    private bool dead = false;

    // Start is called before the first frame update
    void Start()
    {
        targetHeat = PlayerMovement.Instance.GetHeat();
        rb = transform.GetComponent<Rigidbody>();
        warn = PlayerMovement.Instance.GetComponent<GUIHandler>().warn;

        //Renderer rend = explosionPrefab.GetComponent<Renderer>();
        //rend.material.mainTexture = Resources.Load("Textures/FireExplosion") as Texture;
        // I'm not doing it right. I'll do it some other time. It's not renderer. It's a material albedo sprite atlas?!

    }

    // Update is called once per frame
    void Update()
    {

        if ((GameManager.Instance.stage == GameManager.Stage.game) && (GameManager.Instance.isMovementEnabled) && !dead)
        {
            if (GetComponent<Health>().GetHealth() <= 0)                Explode(); 

            Vector3 currentTransPos = new Vector3(transform.position.x, 1, transform.position.z);
            Vector3 currentPlayerPos = new Vector3(PlayerMovement.Instance.transform.position.x, 1, PlayerMovement.Instance.transform.position.z);
            targetHeat = PlayerMovement.Instance.GetHeat();
            triggerthreshold = distanceToPlr - (targetHeat * sensitivity);
            distanceToPlr = Vector3.Distance(currentTransPos, currentPlayerPos);
            if ( triggerthreshold < 0) // Is it a hot enough target? 
            {
                Vector3 direction = (currentPlayerPos - currentTransPos).normalized;
                Ray ray = new Ray(currentTransPos, direction);
                RaycastHit hit;
                warn.ChangeSpr(1);
                // Debug.DrawRay(currentTransPos, direction, Color.red, 10f);
                // is the player visible? 
                if (Physics.Raycast(ray, out hit, targetHeat))
                {
                    PlayerRay = hit.transform.name;

                    //  Debug.Log(hit.transform.name + "    " + hit.point.x + ", " + hit.point.y + ", " + hit.point.z);
                    //hitpoint.position = hit.point;  // only for debug to see where it's aiming
                    if (hit.transform.name == "Player")
                    {

                        rb.AddForce(direction * AttackSpeed * Time.deltaTime); // I've rplaced rawsteering with gear
                        warn.ChangeSpr(2);


                        //. float newlintensity = Mathf.Lerp(attackLight.intensity, 2f, 0.01f);
                        attackLight.intensity = 1 / distanceToPlr; // = newlintensity;
                        

                    }
                    else
                    {
                        // nmalk.SetSpeed(0);
                        // I guess do nothing
                    }
                }
                else
                {
                    warn.ChangeSpr(0);
                }
            }
            else
            {

                warn.ChangeSpr(0);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        // if (collision.collider.name == "Player")
        // let it collide with everything
        if (collision.collider.transform.tag != "Floor" && !dead)
        {
            Explode();
            // Invoke("Destroy(gameObject)",10.2f);
        }

    }







    private void OnDestroy()
    {
        
    }

    private void Explode()
    {
        Debug.Log("Exploding!");

        dead = true;
        // get a list of Health in radius
        //apply damage depending on distance.

        Vector3 exp = transform.position;
        GameObject _explosion = Instantiate(explosionPrefab, exp, Quaternion.identity, null);
        Collider[] col = Physics.OverlapSphere(exp, blastRadius);
        foreach (Collider c in col)
        {
            Rigidbody crb = c.attachedRigidbody; 
            if (crb != null)
            {
                crb.AddExplosionForce(damageStrength, exp, blastRadius, 0.5f);
            }
            c.TryGetComponent<Health>(out Health h);
            if (h != null) h.AddHealth(-damageStrength / (1000 * Vector3.Distance(exp, c.transform.position)));
        }

        ToggleAlive();
    }

    public object CaptureState()
    {
        Dictionary<string, object> savedata = new Dictionary<string, object>();
        savedata["position"] = new SerializableVector3(transform.position);
        savedata["rotation"] = new SerializableVector3(transform.eulerAngles);
        savedata["dead"] = dead;


        return savedata;
    }

    public void RestoreState(object state)
    {
        Dictionary<string, object> savedata = (Dictionary<string, object>)state;
        transform.position = ((SerializableVector3)savedata["position"]).ToVector();
        transform.eulerAngles = ((SerializableVector3)savedata["rotation"]).ToVector();
        dead = (bool)savedata["dead"];
        ToggleAlive();
    }

    private void ToggleAlive() // why not take "dead" global instead?
    {
        GetComponent<BoxCollider>().enabled = !dead;
        transform.GetChild(0).gameObject.SetActive(!dead);
        attackLight.intensity = 0;
    }
}
