using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OldEnemyBehBackupBecauseitsHandy : MonoBehaviour
{
    /*
    private void OnDestroy()
    {
        GameManager.OnTestScenarioChange -= TestScenarioHandler;
        Health.OnDeath -= DeathHandle;
        Health.OnHit -= HealthHitHandle;
    }

    void Start()
    {
        targetHeat = PlayerMovement.Instance.GetHeat();
        aimAt = Vector3.zero;
        intent = Intent.Idle;
        health = transform.GetComponent<Health>();

        if (!activated && (GameManager.Instance.isHexaWorld)) StartCoroutine(ActivationChecks());
        if (!activated && !GameManager.Instance.isHexaWorld)
        {
            // hexless activation
            GetComponent<Rigidbody>().drag = aliveDrag;
            navAgent = GetComponent<NavMeshAgent>();
            navAgent.enabled = true;

            navAgent.isStopped = true;
            activated = true;
        }
        currentPatrolPoint = 0;
    }




    // Update is called once per frame
    void Update()
    {

        if ((GameManager.Instance.stage == GameManager.Stage.game) && (GameManager.Instance.isMovementEnabled) && activated && !dead)
        {
            targetHeat = PlayerMovement.Instance.GetHeat();
            Patrolling();
            if (PlayersAwareness > 10) ChasePlayer();
            TurretOperations(); // removed && (!GameManager.Instance.isDocked) && (!GameManager.Instance.isDocking) because docking should allow reloading and such. 

            PerformAIFunctions();
            if (PlayersAwareness > 0) PlayersAwareness -= Time.deltaTime;
        }
        else
        {
            // if (!activated) ActivationChecks(); 
            // I'll start a coroutine in the beginning instead.
        }
    }

    public void AddAwareness(float awa)
    {
        PlayersAwareness += awa;
    }

    private void TurretOperations()
    {

        if (reload > 0) reload -= 1f * Time.deltaTime;
        if (heat > 0) heat -= 0.05f * Time.deltaTime;


        Vector3 currentTransPos = new Vector3(transform.position.x, 1, transform.position.z);
        Vector3 currentPlayerPos = new Vector3(PlayerMovement.Instance.transform.position.x, 1, PlayerMovement.Instance.transform.position.z);
        distanceToPlr = Vector3.Distance(currentTransPos, currentPlayerPos);
        if (distanceToPlr < targetHeat) // Is it a hot enough target? 
        {
            Vector3 direction = (currentPlayerPos - currentTransPos).normalized;
            Ray ray = new Ray(currentTransPos, direction);
            RaycastHit hit;
            // Debug.DrawRay(currentTransPos, direction, Color.red, 10f);
            // is the player visible? 
            if (Physics.Raycast(ray, out hit, targetHeat))
            {
                if (hit.transform.name == "Player")
                {
                    PlayersAwareness = Mathf.Max(50, PlayersAwareness);
                    aimAt = new Vector3(hit.point.x, hit.point.y, hit.point.z);
                    if (health.GetHealth() > 0.2f) { enemyToDo = EnemyAction.Attack; } else { enemyToDo = EnemyAction.DistanceIncrease; }
                    PlayersLastLocation = PlayerMovement.Instance.transform.position;

                }

                //  Debug.Log(hit.transform.name + "    " + hit.point.x + ", " + hit.point.y + ", " + hit.point.z);
                //hitpoint.position = hit.point;  // only for debug to see where it's aiming
            }
            else
            {
                PlayersAwareness = Mathf.Max(29, PlayersAwareness);
                // TODO act and search?

                if (health.GetHealth() < 0.2f) enemyToDo = EnemyAction.Hide;
            }
        }
        else
        {


            // PlayersAwareness = Mathf.Max(24, PlayersAwareness);

            if (health.GetHealth() < 0.2f) enemyToDo = EnemyAction.Hide;
        }

    }

    private void Patrolling()
    {
        if (PlayersLastLocation != Vector3.zero && enemyToDo != EnemyAction.Attack)
        {
            enemyToDo = EnemyAction.Search;
            if (Vector3.Distance(transform.position, PlayersLastLocation) < 2)
            {
                currentPatrolPoint++;
                PlayersLastLocation = Vector3.zero;
            }
        }
        else
        {
            if (PatrolRoutePoints.Count > 0)
            {
                if (currentPatrolPoint > PatrolRoutePoints.Count - 1) currentPatrolPoint = 0;
                PlayersLastLocation = PatrolRoutePoints[currentPatrolPoint].position;
                enemyToDo = EnemyAction.Search;
            }
            else enemyToDo = EnemyAction.Idle;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Enemy Tank collided with " + collision.collider.ToString());

    }

    private IEnumerator ActivationChecks()
    {
        while (!GetComponentInParent<HexTile>().GetActive())
        {
            yield return new WaitForSeconds(0.1f);
        }
        if (GetComponentInParent<HexTile>().GetActive())
        {
            GetComponent<Rigidbody>().drag = aliveDrag;
            navAgent = GetComponent<NavMeshAgent>();
            navAgent.enabled = true;
            // GetComponent<NavMeshAgent>().enabled = true;
            navAgent.isStopped = true;
            activated = true;
        }
        yield return null;
    }

    /*
    Transform GetClosestPoint(Transform[] enemies)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (Transform potentialTarget in enemies)
        { 
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }

        return bestTarget;
    }

    // no idea what this code was for.
    // it came from the sponger.


    private void PerformAIFunctions()
    {
        switch (enemyToDo)
        {
            case EnemyAction.Attack:
                if (AimTurret())
                {
                    if (reload <= 0 && ammo > 0)
                    {
                        FireProjectile();
                    }
                }
                break;
            case EnemyAction.Search:
                GoToPoint(PlayersLastLocation);
                break;
            case EnemyAction.Idle:
                navAgent.isStopped = true;

                break;


        }
    }


    private bool AimTurret()
    {
        Vector3 targetRotation = new Vector3(aimAt.x, turretCore.position.y, aimAt.z) - turretCore.position;
        Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;
        float divergence = Vector3.Angle(targetRotation, turretCore.forward);
        if (divergence > 1f)  // we're not done with the rotation
        {
            turretCore.rotation = Quaternion.Slerp(turretCore.rotation, Quaternion.LookRotation(targetRotation), accellerationConstantCoefficient * Time.deltaTime);
        }
        else
        {
            return true;
        }


        return false; // we have not aimed yet
    }

    private void FireProjectile()
    {
        Debug.Log("Bang!");
        reload = 3f;
        heat += 0.1f;
        ammo--;

        // I hope this was player only....
        // GUIHandler.Instance.ProjectileFired(heat, ammo, reload, GetAmmoName()); // this method will pull all sorts of public vars from

        Quaternion q = Quaternion.Euler(muzzle.rotation.eulerAngles.x, muzzle.rotation.eulerAngles.y, muzzle.rotation.eulerAngles.z);
        GameObject proj = Instantiate(projectile, muzzle.position, q, null);
        proj.GetComponent<ProjectileBeh>().damagePower = 0.1f;
        proj.GetComponent<ProjectileBeh>().SetOriginator(transform);
        // Debug.Log("rotation of the projectile iiiis " + q);
        Rigidbody projRB = proj.GetComponent<Rigidbody>();
        projRB.AddRelativeForce(Vector3.forward * 250f);   // AddForce(Vector3.forward*1000f);
        muzzleFlash.Play();
    }


    private void ChasePlayer()
    {
        if (PlayersAwareness > 10 && distanceToPlr > 15) // decreasing distance
        {
            navAgent.SetDestination(PlayerMovement.Instance.transform.position);
            navAgent.isStopped = false;
        }
        else
        {
            if (distanceToPlr < 12f) navAgent.isStopped = true;
        }
    }

    private void GoToPoint(Vector3 _v)
    {
        navAgent.SetDestination(_v);
        navAgent.isStopped = false;
    }


    private void DeathHandle()
    {
        if (health.GetHealth() <= 0 && !dead)
        {
            Vector3 yoffset = new Vector3(0, 0.72f, 0);
            GetComponent<Rigidbody>().drag = deadDrag;
            GetComponent<Rigidbody>().constraints = deadConstraints;
            Debug.Log("deading: " + transform.name);
            navAgent.isStopped = true;
            dead = true;
            turretDead.gameObject.SetActive(true);
            turretDead.transform.localPosition = disableOnDead[0].transform.localPosition;
            turretDead.transform.eulerAngles = disableOnDead[0].transform.eulerAngles;
            foreach (Transform _t in disableOnDead)
            {
                _t.gameObject.SetActive(false);
            }

            turretDead.transform.SetParent(transform.parent);
            flames.gameObject.SetActive(true);
            GameObject _explosion = Instantiate(explosionPrefab, transform.position + yoffset, Quaternion.identity, null);
            turretDead.GetComponent<Rigidbody>().AddExplosionForce(10, transform.position + yoffset, 2, 0.5f);

        }
    }

    private void HealthHitHandle(Health h)
    {
        if (h == health) // if it's my health that's hit, do.
                         // no idea how other way capture publisher of the event.
        {
            Debug.Log(transform.name + " received a hit! (Health even cpatiyuiuitrwlfklsdn)");
        }
    }

    private void RestoreToLifeHandle()
    {
        if (health.GetHealth() > 0 && dead) // checking just in case
        {
            Vector3 yoffset = new Vector3(0, 0.72f, 0);
            GetComponent<Rigidbody>().drag = aliveDrag;
            GetComponent<Rigidbody>().constraints = aliveConstraints;
            Debug.Log("aliving: " + transform.name);
            navAgent.isStopped = false;
            dead = false;
            turretDead.transform.localPosition = disableOnDead[0].transform.localPosition;
            turretDead.transform.eulerAngles = disableOnDead[0].transform.eulerAngles;
            foreach (Transform _t in disableOnDead)
            {
                _t.gameObject.SetActive(true);
            }

            turretDead.transform.SetParent(transform.parent);

            turretDead.gameObject.SetActive(false);
            flames.gameObject.SetActive(false);

        }
    }


    private void TestScenarioHandler(int test)
    {
        if (test == 2) // we know where the player is and we can't be deceived.
        {
            PlayersAwareness = 100000;
        }
    }

    public object CaptureState()
    {
        Dictionary<string, object> savedata = new Dictionary<string, object>();

        savedata["position"] = new SerializableVector3(transform.position);
        savedata["rotation"] = new SerializableVector3(transform.eulerAngles);
        savedata["dead"] = dead;
        savedata["activated"] = activated;
        savedata["currentPatrolPoint"] = currentPatrolPoint;
        savedata["PlayersLastLocation"] = new SerializableVector3(PlayersLastLocation);
        savedata["PlayersAwareness"] = PlayersAwareness;
        savedata["ammo"] = ammo;
        savedata["elevation"] = elevation;
        savedata["reload"] = reload;
        savedata["heat"] = heat;

        return savedata;
    }

    public void RestoreState(object state)
    {

        Dictionary<string, object> savedata = (Dictionary<string, object>)state;

        transform.position = ((SerializableVector3)savedata["position"]).ToVector();
        transform.eulerAngles = ((SerializableVector3)savedata["rotation"]).ToVector();
        // dead = (bool)savedata["dead"]; // this should be restored in restore to life handle
        activated = (bool)savedata["activated"];
        currentPatrolPoint = (int)savedata["currentPatrolPoint"];
        PlayersLastLocation = ((SerializableVector3)savedata["PlayersLastLocation"]).ToVector();
        PlayersAwareness = (float)savedata["PlayersAwareness"];
        ammo = (int)savedata["ammo"];
        elevation = (float)savedata["elevation"];
        reload = (float)savedata["reload"];
        heat = (float)savedata["heat"];

        RestoreToLifeHandle();
    }
}
    */
}
