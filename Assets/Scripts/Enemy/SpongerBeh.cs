using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpongerBeh : MonoBehaviour
{
    float targetHeat;

    [SerializeField] bool testRun;
    [SerializeField] float distanceToPlr;
    //[SerializeField] Transform hitpoint; // only for debug to see where it's aiming
    [SerializeField] float AttackSpeed;
    [SerializeField] WarningLightSwapper warn;
    private NPCWalkController nmalk;
    private bool attached;
    [SerializeField] Transform[] latchPoints;
    private float randRot;
    // Start is called before the first frame update
    void Start()
    {
        targetHeat = PlayerMovement.Instance.GetHeat();
        nmalk = GetComponent<NPCWalkController>();
        randRot = Random.Range(0, 360);
    }

    // Update is called once per frame
    void Update()
    {

        if ((GameManager.Instance.stage == GameManager.Stage.game) && (GameManager.Instance.isMovementEnabled))
        {
            if (testRun) { nmalk.SetSpeed(AttackSpeed); return; }
            Vector3 currentTransPos = new Vector3(transform.position.x, 1, transform.position.z);
            Vector3 currentPlayerPos = new Vector3(PlayerMovement.Instance.transform.position.x, 1, PlayerMovement.Instance.transform.position.z);
            targetHeat = PlayerMovement.Instance.GetHeat();
            distanceToPlr = Vector3.Distance(currentTransPos, currentPlayerPos);
            if (distanceToPlr < targetHeat && !attached) // Is it a hot enough target? 
            {
                Vector3 direction = (currentPlayerPos - currentTransPos).normalized;
                Ray ray = new Ray(currentTransPos, direction);
                RaycastHit hit;
                warn.ChangeSpr(1);
                // Debug.DrawRay(currentTransPos, direction, Color.red, 10f);
                // is the player visible? 
                if (Physics.Raycast(ray, out hit, targetHeat))
                {

                    //  Debug.Log(hit.transform.name + "    " + hit.point.x + ", " + hit.point.y + ", " + hit.point.z);
                    //hitpoint.position = hit.point;  // only for debug to see where it's aiming
                    if (hit.transform.name == "Player")
                    {
                        nmalk.SetDirection(direction);
                        nmalk.SetSpeed(AttackSpeed);
                        warn.ChangeSpr(2);
                    }
                    else
                    {
                        nmalk.SetSpeed(0);
                    }
                }
                else
                {
                    warn.ChangeSpr(0);
                    nmalk.SetSpeed(0);
                }
            }
            else
            {

                warn.ChangeSpr(0);
                nmalk.SetSpeed(0);
                if (attached)
                {
                    PlayerMovement.Instance.fuel += -0.004f * Time.deltaTime; // SPONGING!
                    transform.localPosition = new Vector3(0, 0, 0);

                    transform.rotation = transform.parent.rotation; // new Quaternion(transform.parent.rotation.x, transform.parent.rotation.y, transform.parent.rotation.z, transform.parent.rotation.w); 
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.name == "Player" && !attached)
        {
            attached = true;
            transform.SetParent(GetClosestPoint(latchPoints));
            // transform.localPosition.Set(0,0,0);

            transform.localRotation = transform.parent.localRotation;
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<Rigidbody>().detectCollisions = false;
        }
    }

    Transform GetClosestPoint(Transform[] enemies)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (Transform potentialTarget in enemies)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }

        return bestTarget;
    }

}
