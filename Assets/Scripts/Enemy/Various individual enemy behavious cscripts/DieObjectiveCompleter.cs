using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieObjectiveCompleter : MonoBehaviour
{
    [SerializeField] RPG.Quests.Quest quest;
    [SerializeField] string objComplete;
    [SerializeField] string objActivate;

    public void OnDestroy()
    {

        if (objComplete.Length > 0) RPG.Quests.QuestListPlayer.Instance.ObjectiveStatusChange(quest, objComplete, RPG.Quests.QuestTaskStatus.Status.completed);
        if (objActivate.Length > 0) RPG.Quests.QuestListPlayer.Instance.ObjectiveStatusChange(quest, objActivate, RPG.Quests.QuestTaskStatus.Status.active);
    }

    void Update()
    {
        
    }
}
