using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour
{
    private Transform mainCam;
    [SerializeField] private bool absolute = true;
    void Start()
    {
        mainCam = Camera.main.transform;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 upup = Vector3.zero;
        if (absolute) { transform.LookAt(transform.position + mainCam.rotation * Vector3.forward, mainCam.rotation * Vector3.up);  }
        else { transform.LookAt(transform.position + mainCam.rotation * Vector3.up, transform.rotation * Vector3.up); }
        
    }
}
