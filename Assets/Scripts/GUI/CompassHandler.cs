using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
public class CompassHandler : MonoBehaviour
{
 

    public RawImage compass;
    public Transform player;
    public GameObject compassMarkPrefab;

    [HideInInspector] public static CompassHandler Instance { get; private set; }

    private void Awake()    {        
        if (Instance == null) { Instance = this; }
    }

    void Update()
    {
        //traditional
        //compass.uvRect = new Rect(player.localEulerAngles.y / 360f, 0, 1, 1); 
        //discworldian
        //compass.uvRect = new Rect(PlayerMovement.Instance.GetBearingToHub() / 360f, 0, 1, 1); // objective compass, no player heading
        compass.uvRect = new Rect(Wrap(player.localEulerAngles.y - PlayerMovement.Instance.GetBearingToHub(),360f) / 360f, 0, 1, 1); //  player heading computed



    }

    public float Wrap(float w, float lim)
    {
        if (w > lim) { w = w - lim; } else
        {
            if (w < 0) w = w + lim;
        }
        
        return w;
    }

    /*
     * Directions within the Discworld are not given as North, South, East, and West, 
     * but rather as directions relating to the disc itself: Hubward (towards the centre), 
     * Rimward (away from the centre) and to a lesser extent, 
     * turnwise and widdershins (relation to the direction of the disc's spin). 
     * */


}
 
