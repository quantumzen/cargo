
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.EventSystems;// Required when using Event data.
[RequireComponent(typeof(AudioSource))]
public class CrewButtonHandler : MonoBehaviour
{


    [SerializeField] Sprite sprOn;
    [SerializeField] Sprite sprOff;
    [SerializeField] Sound sndClick;
    [SerializeField] TMPro.TextMeshProUGUI buttonLabel;
    public CrewPanelHandler cp;

    AudioSource audioData;
    void Start()
    {
        audioData = GetComponent<AudioSource>();
    }

    public void SpriteOn()
    {
        transform.GetComponent<Image>().sprite = sprOn;
        audioData.volume = 0.5f;
        audioData.Play(0);
        cp.DrawCrewMmeber(buttonLabel.text);
    }

    

    public void SpriteOff()
    {
        transform.GetComponent<Image>().sprite = sprOff;
    }

    public void SetButtonLabel(string s)
    {
        buttonLabel.text = s;
    }

    public string GetName()
    {
        return buttonLabel.text;
    }



}
