using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CrewPanelHandler : MonoBehaviour
{

    PanelBeh pb;
    Humans humans;
    [SerializeField] Image speakture;
    [SerializeField] TextMeshProUGUI speakerName;
    [SerializeField] TextMeshProUGUI speakerDescription;
    [SerializeField] Transform buttonParentPanel;
    [SerializeField] GameObject buttonPrefab;
    [SerializeField] List<CrewButtonHandler> spawnedButtons = new List<CrewButtonHandler>();

    [SerializeField]  bool isDeployed;


    void Start()
    {
        pb = GetComponent<PanelBeh>();
        humans = GameManager.Instance.GetComponent<Humans>();
        isDeployed = false;
    }

    // TODO: Spawn humans.aboard like shop buttons.
    

    void Update()
    {
        if (!pb.isDeployed)    { 
            isDeployed = false;
            return;        
        }

        if (!isDeployed)
        {
            isDeployed = true;
            RefreshCrewButtons();
            // TODO if notpresent anymore, text and all has to be reset to captain
        }
    }

    public void DrawCrewMmeber(string name)
    {
        speakture.sprite = humans.GetFace(name);
        speakerDescription.text = humans.GetDescription(name);
        speakerName.text = name;
        foreach (CrewButtonHandler cbt in spawnedButtons)
        {
            if (cbt.GetName() != name)
            {

                //Debug.Log("Disabling Sprite for " + cbt.name +" as it's different from "+ name);
                cbt.SpriteOff();
            }
        }
    }

    private void RefreshCrewButtons()
    {
        bool isDrawnPresentAboard = false;
        // Delete old buttons
        foreach (CrewButtonHandler cbh in spawnedButtons)
        {
            Destroy(cbh.gameObject);
        }
        spawnedButtons.Clear();
        // spawn new ones
        foreach (string s in humans.GetHumansAboard())
        {
            GameObject g = Instantiate(buttonPrefab, buttonParentPanel);
            g.GetComponent<CrewButtonHandler>().cp = this;
            g.GetComponent<CrewButtonHandler>().SetButtonLabel(s);
            spawnedButtons.Add(g.GetComponent<CrewButtonHandler>());
            if (speakerName.text == s) isDrawnPresentAboard = true;
        }
        if (!isDrawnPresentAboard) DrawCrewMmeber(humans.GetHumansAboard()[0]); // karkolomny straight to the point

    }
}
