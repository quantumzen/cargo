using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class GUIHandler : MonoBehaviour
{

    [HideInInspector] public static GUIHandler Instance { get; private set; }

    // this script should contain references to all GUI stuff, so anything in the game can access it by going player.GUIHandler.Blah.
    // or through GUIHandler.Instance - HAH!

    [SerializeField] private Transform lowerGUIText;
    [SerializeField] private Transform upperGUIText;
    [SerializeField] private Transform SpeedWheelSwitcher;
    [SerializeField] private Transform ElevationWheel;
    [SerializeField] private Transform ElevationSwitcher;
    [SerializeField] private Transform InteractorRestTarget;

    private int ut_gold;
    private int ut_bearing;
    private int ut_speed;
    private int ut_gear;
    private Vector2 ut_position;
    private float ut_heat;
    private string ut_buffs;


    [SerializeField] private Slider fuelbar;
    [SerializeField] private Slider heatbar;
    [SerializeField] private Slider ammobar;
    [SerializeField] private Slider reloadbar;
    [SerializeField] private Gradient fuelGradient;
    [SerializeField] private Gradient heatGradient;
    [SerializeField] private Gradient ammoGradient;
    [SerializeField] private Gradient reloadGradient;
    [SerializeField] private Image fuelfill;
    [SerializeField] private Image heatfill;
    [SerializeField] private Image ammofill;
    [SerializeField] private Image reloadfill;
    public WarningLightSwapper warn;

    [SerializeField] private Cinemachine.CinemachineVirtualCamera mainCam;
    [SerializeField] private Cinemachine.CinemachineVirtualCamera portCam;
    [SerializeField] private Cinemachine.CinemachineVirtualCamera aimCam;
    [SerializeField] private Cinemachine.CinemachineVirtualCamera CaptianCam;


    private void Awake() // must be awake to inputs subscrive
    {

        if (Instance == null) { Instance = this; }
        PlayerMovement.OnGearChange += GUIGearChange;
        ut_gear = PlayerMovement.Instance.GetGear();
        TurretBehaviour.OnElevationChange += GUIElevationChange;
        SetUpperTextGear(ut_gear);
        mainCam.enabled = true;
        portCam.enabled = false;
        float scrheight = Screen.height;
        scrheight -= 1080; // this is default
        InteractorRestTarget.position = new Vector3(InteractorRestTarget.position.x, InteractorRestTarget.position.y - scrheight, InteractorRestTarget.position.z );
    }
    private void OnDestroy()
    {

        TurretBehaviour.OnElevationChange -= GUIElevationChange;
        PlayerMovement.OnGearChange -= GUIGearChange;
    }


    private void Update()
    {
        float _heat = PlayerMovement.Instance.GetHeat() / 25;
        heatbar.value = _heat;
        heatfill.color = heatGradient.Evaluate(_heat);
        
        if (reloadbar.value > 0)
        {
            reloadbar.value = TurretBehaviour.Instance.reload;
            reloadfill.color = reloadGradient.Evaluate(TurretBehaviour.Instance.reload);
        }

        fuelbar.value = PlayerMovement.Instance.fuel;
        fuelfill.color = fuelGradient.Evaluate(fuelbar.normalizedValue);


    }

    // GUIHandler.Instance.CycleAimCam();

    public void SetLowerGUIText(string t)
    {
        lowerGUIText.GetComponent<TMP_Text>().text = t;
        lowerGUIText.gameObject.SetActive(true);
    }
    public void ClearLowerGUIText()
    {
        lowerGUIText.GetComponent<TMP_Text>().text = "";
        lowerGUIText.gameObject.SetActive(false);
    }
    public void SetUpperGUIText()
    {
        upperGUIText.GetComponent<TMP_Text>().text = " " + ut_gold + "    Health: " + Mathf.RoundToInt(PlayerMovement.Instance.transform.GetComponent<Health>().GetHealth() * 100) + "%   "+ ut_buffs; //  DisttoHub: " + PlayerMovement.Instance.GetDistanceToHub() + "  BeartoHub: " + PlayerMovement.Instance.GetBearingToHub() ;    // don't need it anyyumore:  Heat: " + (Mathf.Round(ut_heat*100))/100;"    Bearing: " + ut_bearing + "    Gear: "+(ut_gear-2)+"       "    Position: " + Mathf.RoundToInt(ut_position.x) + ", " + Mathf.RoundToInt(ut_position.y) + 
        upperGUIText.gameObject.SetActive(true);
    }
    public void ClearUpperGUIText()
    {
        upperGUIText.GetComponent<TMP_Text>().text = "";
        upperGUIText.gameObject.SetActive(false);
    }

    public void SetUpperTextBuffs(string b)
    {
        ut_buffs = b;
        SetUpperGUIText();
    }


    public void SetUpperTextGold(int gold)
    {
        ut_gold = gold;
        SetUpperGUIText();
    }

    public void SetUpperTextBearing(int b)
    {
        ut_bearing = b;
        SetUpperGUIText();
    }
    public void SetUpperTextSpeed(int s)
    {
        ut_speed = s;
        SetUpperGUIText();
    }
    public void SetUpperTextPos(Vector2 v)
    {
        ut_position = v;
        SetUpperGUIText();
    }
    public void SetUpperTextGear(int g)
    {
        ut_gear = g;
        SetUpperGUIText();
    }
    //public void SetUpperTextHeat(float h)
    //{
    //    ut_heat = h;
    //    SetUpperGUIText();
    //}

    public void EnablePortCam(Transform t)
    {
        Debug.Log("Enabling Port Cam");
        portCam.transform.position = t.position;
        portCam.transform.rotation = t.rotation;
        portCam.enabled = true;
        CaptianCam.enabled = false;
        mainCam.enabled = false;
        aimCam.enabled = false;
    }

    public void CycleAimCam()
    {
        if (aimCam.enabled == true)        { EnableMainCam(); } else { EnableAimCam(); }
    }
    public void EnableMainCam()
    {
        Debug.Log("Enabling Main Cam");
        mainCam.enabled = true;
        portCam.enabled = false;
        aimCam.enabled = false;
        CaptianCam.enabled = false;
    }
    public void EnableAimCam()
    {
        aimCam.enabled = true;
        portCam.enabled = false;
        mainCam.enabled = false;
        CaptianCam.enabled = false;
    }

    public void EnableCaptainCam()
    {

        Debug.Log("Enabling Captian Cam");
        CaptianCam.enabled = true;
        portCam.enabled = false;
        mainCam.enabled = false;
        aimCam.enabled = false;
    }


    private void GUIElevationChange(float e)
    {

        Quaternion q = new Quaternion(0f, 0f, e, 90);
        Quaternion q2 = new Quaternion(0f, 0f, e*52.42f, 90);
        ElevationSwitcher.DOLocalRotateQuaternion(q, 2f);
        // ElevationWheel.DOLocalRotateQuaternion(q2, 2f);
        ElevationWheel.DOLocalRotate(new Vector3(0f, 0f, e * 52.42f), 2f);
    }

    public void ProjectileFired(int ammo, float reload, string ammoName)
    {

        heatbar.value = PlayerMovement.Instance.GetHeat();
        ammobar.value = ammo;
        ammofill.color = ammoGradient.Evaluate(ammobar.normalizedValue); // I'll fix it later . TODO: change to maxammo of some sort.   
        UpdateReloadBar(reload);
        ammobar.transform.GetComponentInChildren<TMP_Text>().text = ammoName;



    }

    public void UpdateReloadBar(float reload)
    {
        reloadbar.value = reload;
    }

    private void GUIGearChange(int g)
    {
        float shiftAngle;
        // int gearChangeMagnitude;
        // I can't believe how I have overcomplicated this nonsense. I've spent an entire day on it!

        do
        {


            // Debug.Log("gear: " + ut_gear + "  requested gear: " + g + "   calculated next gear: " + (ut_gear + ((ut_gear < g) ? 1 : -1)));
            switch (g)
            {
                case 0:
                    shiftAngle = 30f;
                    break;
                case 1:
                    shiftAngle = 30f;
                    break;
                case 2:
                    shiftAngle = (ut_gear > g) ? 20f : 30f;
                    break;
                case 3:
                    shiftAngle = 20f;
                    break;
                case 4:
                    shiftAngle = 20f;
                    break;
                case 5:
                    shiftAngle = 20f;
                    break;
                case 6:
                    shiftAngle = 20f;
                    break;
                default:
                    shiftAngle = 0f;
                    break;

            }

            if (ut_gear < g)
            {
                shiftAngle = -shiftAngle;
                SetUpperTextGear(ut_gear + 1);
            }
            else
            {
                SetUpperTextGear(ut_gear - 1);
            }


            // Debug.Log("curretn z rot " + SpeedWheelSwitcher.eulerAngles.z);
            // Debug.Log("shifting to " + shiftAngle);
            SpeedWheelSwitcher.Rotate(0f, 0f, shiftAngle, Space.Self);
        } while (g != ut_gear);
    }

    public void OnClick(InputAction.CallbackContext val)
    {

        Vector2 mousepos = UnityEngine.InputSystem.Mouse.current.position.ReadValue();
        // Debug.Log("clicked! "+mousepos.x+", "+mousepos.y);
    }


}
