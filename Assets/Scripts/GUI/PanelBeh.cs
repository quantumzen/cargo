using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class PanelBeh : MonoBehaviour
{
    Vector3 deployedPosition;
    // public float directionOfEmergence;
    public bool Trig;
    private enum PathOfEmergence { top, bottom, left, right};
    [SerializeField] PathOfEmergence pathOfEmergence;
    [SerializeField] Transform RestTarget;
    private Vector2 retrectedPosition;
    [SerializeField] bool isEscapist = true; // this means it retracts on Esc. If it's not deployable, like the shop panel, it should have the restTarget == null
    // [SerializeField] bool verticalPathOfEmergence;
    [SerializeField] float emergenceSpeed = 1f;
    [SerializeField] float panelHeight;
    public bool isDeployed;

    public static event Action<bool, string> OnPanelMove;

    void Start()
    {

        Invoke("CalculateRestPosition", 0.2f);

        if (isEscapist) PlayerMovement.OnEscPressed += EscCheck;
        
        // transform.position.Set(RestTarget.position.x, RestTarget.position.y, RestTarget.position.z);
        // transform.position.x = -xstart;

    }

    private void CalculateRestPosition()
    {
        deployedPosition = transform.position;
        if (RestTarget != null)
        {
            if (pathOfEmergence == PathOfEmergence.left || pathOfEmergence == PathOfEmergence.right) retrectedPosition = new Vector2(RestTarget.position.x, deployedPosition.y);
            if (pathOfEmergence == PathOfEmergence.top || pathOfEmergence == PathOfEmergence.bottom) retrectedPosition = new Vector2(deployedPosition.x, RestTarget.position.y);
            transform.DOMove(retrectedPosition, 0f).SetEase(Ease.InOutQuint);
            // Debug.Log("Moving the panel " + transform.name + " to " + retrectedPosition.x.ToString() + ", " + retrectedPosition.y.ToString());
        }
    }

    void Update()
    {
        if (Trig) // this is only for debug, really. I can't imagine using it ingame
        {
            Cycle();
            Trig = false;
        }
    }


    public void KeyboardPanelTrigger(UnityEngine.InputSystem.InputAction.CallbackContext val) 
    {
        if (val.started)
        {
            if (GameManager.Instance.stage == GameManager.Stage.game) Deploy();
        }
    }

    private void EscCheck()
    {
        if (isDeployed) { Hide(); }
    }
    public void Cycle()
    {
        if (isDeployed)     {   Hide();        }
        else                {   Deploy();      }
    }

    public void Deploy()
    {
        if (isDeployed) return;
        // if (isEscapist) PlayerMovement.Instance.escapists.Push(transform);
        transform.DOMove(deployedPosition, emergenceSpeed).SetEase(Ease.InOutQuint);

        // Debug.Log("Deploying the panel " + transform.name + " to " + deployedPosition.x.ToString() + ", " + deployedPosition.y.ToString());
        isDeployed = true;
        OnPanelMove?.Invoke(true, transform.name);
    }

    public void DelayedDeploy(float s)
    {
        Invoke("Deploy", s);
    }


    public void Hide()
    {
        if (!isDeployed) return;
        float heights = 0 ;
        foreach (RectTransform rt in transform.GetComponentsInChildren<RectTransform>()) {
            if (rt.rect.height > heights) heights = rt.rect.height + (Screen.height - 1080);
        }

        // Debug.Log("rest Height for " + transform.name + " : " + heights);
        if (pathOfEmergence == PathOfEmergence.top) retrectedPosition = new Vector2(deployedPosition.x, Screen.height + heights);
        if (pathOfEmergence == PathOfEmergence.bottom) retrectedPosition = new Vector2(deployedPosition.x, -heights);
        transform.DOMove(retrectedPosition, emergenceSpeed).SetEase(Ease.InOutQuint);

        // Debug.Log("Hiding the panel " + transform.name + " to " + retrectedPosition.x.ToString() + ", " + retrectedPosition.y.ToString());
        isDeployed = false;

        OnPanelMove?.Invoke(false, transform.name);
    }
}
