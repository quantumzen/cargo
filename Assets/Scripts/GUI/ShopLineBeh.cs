using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using TMPro;

public class ShopLineBeh : MonoBehaviour
{
    public TMP_Text itemName;
    public TMP_Text buyPrice;
    public TMP_Text sellPrice;
    public TMP_Text available;
    public TMP_Text owned;
    public Transform rarity;
    public portBeh pb; // we will inform the port authorities of any transactions
    public int num; // item number, so it can communicate with portbeh

    public Button buy;
    public Button sell;
    [SerializeField] bool isHeader = false;

    private void Start()
    {
        if (isHeader)
        {
            itemName.text = "commodity";
            buyPrice.text = "buy";
            sellPrice.text = "sell";
            available.text = "shop";
            owned.text = "cargo";
            rarity.GetComponent<Image>().sprite = null;
            buy.interactable = false;
            sell.interactable = false;
            buy.transform.GetComponent<Image>().enabled = false;
            sell.transform.GetComponent<Image>().enabled = false;
            rarity.transform.GetComponent<Image>().enabled = false;
            // buy.gameObject.SetActive(false);
            // sell.gameObject.SetActive(false);

        }
    }

    public void OnBuyPressed()
    {
        pb.OnBuyClicked(num);
    }
    public void OnSellPressed()
    {

        pb.OnSellClicked(num);
    }
}
