using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabHandler : MonoBehaviour
{

    public enum tab { dialogue, crew, character, cargo, log, quests, help, pause };
    public tab currentTab;
    [SerializeField] List<PanelBeh> Subpanels = new List<PanelBeh>();
    [SerializeField] InventoryPanel2 invView;


    // Start is called before the first frame update
    void Start()
    {
        currentTab = tab.character;
        // currentTab = tab.dialogue; //temporary switch
        //DeployTab((int)currentTab); 
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ButtonClicked(int b)
    {
        // Debug.Log(b + "  " + (int)currentTab);

        if (b == (int)currentTab) return;
        DeployTab(b);
    }

    //TODO on deploy: Deploys itself + subpanel.
    // in destroy - analogically.

    private void DeployTab(int i)
    {

        if (Subpanels[i] == null) return;
        if (GameManager.Instance.stage != GameManager.Stage.game && GameManager.Instance.stage != GameManager.Stage.paused) return;
        Subpanels[(int)currentTab].Hide();
        Subpanels[i].Deploy();
        currentTab = (tab)i;

        switch ((tab)i) // refresh tabs when they come into focus.
        {
            case tab.cargo:
                //invView.PrepareButtonTable();
                break;

            case tab.dialogue:
                if (RPG.Dialogue.Conversant.Instance.GetCurDialogue() == null )
                {
                    Interactible inte = GetComponent<Interactible>();
                    RPG.Dialogue.Conversant.Instance.OpenDialogue(inte.NPCDialogue, inte.currentNode, true, inte);  // TODO: Close the dialogue if plr moves away. Handle the "mustPause"
                    
                }
                
                break;

            default:
                break; // so it's nice
                
        }
    }

}
