using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircaWorld : MonoBehaviour
{


    [HideInInspector] public static CircaWorld Instance { get; private set; }

    void Awake()
    {
        if (Instance == null) { Instance = this; }

    }
    // Start is called before the first frame update
    void Start()
    {
        Invoke("AllowTestScenarioLoad", 0.5f);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void AllowTestScenarioLoad()
    {
        GameManager.Instance.TestScenarioChange(); // nie za szybko?
    }

}
