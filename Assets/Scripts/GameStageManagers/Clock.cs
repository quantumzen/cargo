using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevTV.Saving;
using System;

public class Clock : MonoBehaviour, ISaveable
{
    private bool isGame = false;
    private string[] monthNames;
    private string[] yearNames;
    private int currentMonth = -1;
    private int currentYear = -1;
    private string currentMonthName = "";
    private string currentYearName = "";
    public double timePlayed { get; private set; } = 0;
    [SerializeField] TMPro.TextMeshProUGUI ClockDisplay;



    public static event Action<int> OnMonthChange;
    public static event Action<int> OnYearChange;


    private void Awake()
    {
        GameManager.OnStageChange += ManageStageChange;
        monthNames = new string[12] { "Goat", "Squid", "Fish", "Ram", "Bull", "Butterfly", "Crab", "Lion", "Cat", "Dragonfly", "Scorpion", "Centaur" }; // {"January", "Fevruary", "March", "Aprill", "Mai", "Juni", "Juli", "Augi", "Septi", "Octi", "Novi", "Duodecci" };
        yearNames = new string[5] { "Wooden", "Dust", "Crystal", "Iron", "Water" };

        // timePlayed = 1179864318; // TODO: clock fails big time if big numbers
        timePlayed = 11798643;

    }

    private void Start()
    {
        if (GameManager.Instance.stage == GameManager.Stage.game) isGame = true;
    }

    private void OnDestroy()
    {
        GameManager.OnStageChange -= ManageStageChange;
    }

    // Update is called once per frame
    void ManageStageChange(GameManager.Stage sOld, GameManager.Stage sNew)
    {
        if (sNew == GameManager.Stage.game) isGame = true; else isGame = false;

        if (sOld != GameManager.Stage.menu && sNew == GameManager.Stage.menu)
        {

            timePlayed = 0;
        }

         

    }

    private void Update()
    {
        if (isGame && (!GameManager.Instance.isDocked)) {
            SetTimePlayed(timePlayed + (Time.deltaTime*60));
            ClockDisplay.text = GetTextDate();
        }
        // (int)System.DateTimeOffset.Now.ToUnixTimeSeconds();
    }

    public void SetTimePlayed(double t)
    {
        timePlayed = t;
    }








    public int GetIntDate()
    {
        long _t = (long)(timePlayed - (timePlayed % 1));
        return (int)_t;
    }

    public string GetTextDate()
    {
        return IntDateToText(GetIntDate());
    }

    public string IntDateToText(int date)
    {

        /*
         * 
         * 
         calculate minutes 
        minutes = seconds / 60;
        seconds -= minutes * 60;
         calculate hours 
        hours = minutes / 60;
        minutes -= hours * 60;
         calculate days 
        days = hours / 24;
        hours -= days * 24;

         Unix time starts in 1970 on a Thursday 
        year = 1970;
        dayOfWeek = 4;

        */
        string finDate = "";
        int minutes = Mathf.FloorToInt(date / 60);
        int hours = Mathf.FloorToInt(minutes / 60);
        minutes -= hours * 60;
        int days = Mathf.FloorToInt(hours / 24);
        hours -= days * 24 ;
        int months = Mathf.FloorToInt(days / 28);
        days -= months * 28 ;
        days++; // because no zero day.
        int years = Mathf.FloorToInt(months / 12);
        months -= years * 12;
        SetMonthYearNames(months, years);
        // no ++ as is converted to name.
        string minutesWithZero = "";
        if (minutes < 10) minutesWithZero = "0" + minutes.ToString(); else minutesWithZero =  minutes.ToString();
        finDate = currentYearName + " " + currentMonthName + " " + days.ToString() + " - " + hours.ToString() + ":" + minutesWithZero;// + " t:" + timePlayed.ToString("R");
        return finDate;
    }


    private void SetMonthYearNames(int m, int y) 
    {
        if (currentMonth != -1)
        {
            if (currentMonth != m)
            {
                currentMonthName = monthNames[m];
                currentMonth = m;
                // trigger an event here
                OnMonthChange?.Invoke(m);
            }
        } else
        {
            // initialise
            currentMonthName = monthNames[m];
            currentMonth = m;
        }

        if (currentYear != -1)
        {
            if (currentYear != y)
            {
                currentYearName = yearNames[y % 5];
                currentYear = y;
                // trigger an event here
                OnYearChange?.Invoke(y);
            }
            
        }
        else
        {
            // initialise
            currentYearName = yearNames[y % 5]; 
            currentYear = y;


        }
        /*
    private string[] monthNames;
    private string[] yearNames;
    private int currentMonth = -1;
    private int currentYear = -1;
        */

    }




    // -------------------------

    public object CaptureState()
    {
        return timePlayed;
    }

    public void RestoreState(object state)
    {
        SetTimePlayed((double)state);
    }
}
