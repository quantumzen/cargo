using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevTV.Saving;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour, ISaveable
{


    [HideInInspector] public static GameManager Instance { get; private set; }
    // [SerializeField] Transform player = null; // NO. Thirsty must come to the source.

    public bool isMovementEnabled = false;
    public bool isDocking { get; private set; } = false;
    public bool isDocked { get; private set; } = false;
    public bool isPaused  = true;
    bool stalwart = false; // setting to false fo tests of the system.
    public bool isHexaWorld = true;
    public int hasCompletedDrivingExam = 0;
    public float gameTime { get; private set; } = 0;



    [SerializeField] TMPro.TextMeshProUGUI DebugConsole;
    public List<Item> items = new List<Item>();
    private List<portBeh> ports = new List<portBeh>();

    public enum Stage { none, menu, game, paused, gameover, win, captain };
    public Stage stage = Stage.menu;

    private Dictionary<string, Transform> fingerprints = new Dictionary<string, Transform>();
    public static event Action<Stage, Stage> OnStageChange;
    public static event Action<int> OnTestScenarioChange;

    public Sprite[] raritySprite;
    public Sprite blankSprite;
    public Sprite[] minimapRepresentations;
    public Color normalText;
    public Color buttonText;
    public Color narrativeText;
    public Color attentionText;
    public Color failedQuestText;
    /*
        Kabul
        #5d5441
        Nomad
        #bab0a0
        Regent Gray
        #80919c
        Rock Blue
        #9ebcc9
        more palette stuff in the project folder on Desktop.
    */


    public int TestScenario;
    // this is an mportant element of where the player starts, what enemies are there and what quests are active.
    // 0 - no testing
    // 1 - populating few quests for quest testing.
    // 2 - enemy know the player's location
    // 5 - set up for Osbourne conclusion

    public List<Transform> StartPostions = new List<Transform>(); // positions for important quest events or start positions for test scenarios
    public float computationalDelay;
    public TMPro.TMP_FontAsset standardFont;

    void Awake()
    {
        if (PlayerPrefs.HasKey("hasCompletedDrivingExam")) hasCompletedDrivingExam = PlayerPrefs.GetInt("hasCompletedDrivingExam");
        if (Instance == null) { Instance = this; }
        LoadTextures();
        PanelBeh.OnPanelMove += PanelMoveCatcher;
        Invoke("SwitchToMenu",0.2f);
        if (gameObject.GetComponent<HexaWorld>() == null) isHexaWorld = false;
    }

    private void OnDestroy()
    {

        PanelBeh.OnPanelMove -= PanelMoveCatcher;
    }

    private void SwitchToMenu()
    {
        StageChange(GameManager.Stage.menu);
    }

    public void DebugPrint(InputAction.CallbackContext val) // X
    {
        if (val.started)
        {
            Quality.Instance.PrintDictionary();
        }
    }

    private void PanelMoveCatcher(bool comingOut, string transName)
    {
        string s = " has gone.";
        if (comingOut) s = " has arisen.";
        // Debug.Log(transName + s);

        if (comingOut && transName == "InteractorPanel")
        {
            StageChange(GameManager.Stage.paused);
        }
        if (!comingOut && transName == "InteractorPanel")
        {
            RPG.Dialogue.Conversant.Instance.CloseDialogue(); // this stack overflow loops?!
            TryUnpause();
        }




    }

    private void Start()
    {
        SetUpItems();
        Application.targetFrameRate = 60;
        // TestScenarioChange(); we will do it once the wolrd has finished Becoming
    }

    private void Update()
    {
        if (stage == Stage.game && !isDocked && !isDocked && isMovementEnabled)   gameTime += Time.deltaTime;
    }

    public void StageChange(Stage st)
    {
        // I'm informing everything that the game stages are changing. So I'm sending new stage and old stage. GUI will be very interested in the old stage, as it will retract panels.
        // except it's not GUI. It's the stage managers that does that.
        // Debug.Log(stage.ToString() + " gets changed to " + st.ToString());
        if (stage == st) return; // all sorts of evils come out of duplication here.
        OnStageChange?.Invoke(stage, st);
        stage = st;
    }

    public void SetDocking(bool d)
    {
        isDocking = d;
    }
    public void SetDocked(bool d)
    {
        isDocked = d;
    }

    public void TryUnpause() // this is called by external objects when they want to unpause (conversant or interactor or them things)
                             //  it centralises conditions. For example, if something is blocking, pause should remain. 
                             // or if the player is dead, for example.
    {
        if (PlayerMovement.Instance.GetComponent<Health>().GetHealth() <= 0)
        {
            StageChange(Stage.gameover);
            return;
        } else
        {
            StageChange(GetComponent<GameStageManager>().previousStage); // go to previous stage that was before the pause. This always should be game, but better ssafe.
        }
    }

    public void TestScenarioChange()
    {
        Debug.Log("Loading test scenario " + TestScenario);
        OnTestScenarioChange?.Invoke(TestScenario);
    }
    


    private void SetUpItems()
    {
        // this is Item: 
        // string newName, string newPlural, int newNumber, int newPrice, int newRarity, int newmaxPerSlot
        // zero is FROBIDDEN! it is NOT a real number. Actually it is, but it's besides the point!
        //items.Add(new Item("Credit", "Credits", 1, 1, 1, 99999));
        //items.Add(new Item("Mobilium", "Mobilium", 2, 5, 1, 10));
        //items.Add(new Item("Piercer", "Piercer", 3, 30, 1, 64));
        //items.Add(new Item("Splasher", "Splasher", 4, 35, 2, 64));
        //items.Add(new Item("Scorcher", "Scorcher", 5, 40, 3, 64));
        //-----------------------------------------------------------------
        //items.Add(new Item("Brukiew", "Brukwi", 10, 3, 1, 64));
        // items.Add(new Item("Sugar", "Sugars", 11, 5, 3, 64));

        //Item _i = ScriptableObject.CreateInstance<Item>();
        //_i.Item("A strawberry", "Strawberries", 12, 10, 4, 256);
        //items.Add(_i);
        //items.Add(new Item("Sand", "Sand", 13, 1, 0, 64));
        //items.Add(new Item("Calcium Carbonite", "Calcium Carbonite", 14, 2, 2, 64)); // not done
        //items.Add(new Item("Gravel", "Gravel", 15, 1, 5, 64));
        //items.Add(new Item("Sorrow", "Sorrow", 16, 5, 6, 16));
        //items.Add(new Item("Clarity", "Clarity", 17, 30, 6, 16));


        //-----------------------------------------------------------------
        //items.Add(new Item("Osbourne's Black Box", "Osbourne's Black Boxes", 500, 1, 6, 1));

        foreach (Item _i in items)
        {
            //Debug.Log(_i.number + "  " + _i.name + " Game Manager tying to add to Dictionary \r\n");
            Quality.Instance.InitialiseQualityEntry(_i.name, _i.number); // deprecated AddToDictionary
        }

    }

    public int FindItemsIndex(int number) // shameless copy paste from inventory
    {                                   // this one returns also the index. 
        var indFinal = -1;
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].number == number)
            {
                indFinal = i;
                break;
            }
        }
        return indFinal;
    }

    public Item GetItemByNumber(int num) // mind you, this returns Item!
    {

        for (int i = 0; i < items.Count; i++)
        {

            if (items[i].number == num)
            {
                return items[i];
            }
        }
        return null;
    }

    public Sprite GetRaritySpriteByItemNumber(int num)
    {
        int rar = GetItemByNumber(num).rarity;
        return raritySprite[rar];
    }

    public int GetItemPrice(int number)
    {
        return GetItemByNumber(number).price;
    }

    public void AddPort(portBeh i)
    {
        if (!ports.Contains(i)) ports.Add(i); // checking out of habit.
    }

    public portBeh GetPortByName(string _name)
    {

        return ports.Find(item => item.transform.name == _name); // whoa!

        // return null;
    }

    public bool IsStalwart()
    {
        return stalwart;
    }

    private void LoadTextures()
    {
        // should I load something here?
        
    }


    public void RegisterFingerprint(string f, Transform t)
    {
        Debug.Log("GameManaer fingerprint registration " + f + " for " + t.name);
        if (!fingerprints.ContainsKey(f)) fingerprints.Add(f, t);
    }

    public Transform GetTransformByFingerprint(string f)
    {
        if (fingerprints.ContainsKey(f)) return fingerprints[f];
        return null;
    }

    public void UpdateDebugConsole(string s)
    {
        DebugConsole.text = s;
    }

    public object CaptureState()
    {
        
        Dictionary<string, object> savedata = new Dictionary<string, object>();
        savedata["fogMap"] = FogOfWarManager.INSTANCE.GetTextureFromSerializer();
        savedata["testScenario"] = TestScenario;
        savedata["hasCompletedDrivingExam"] = hasCompletedDrivingExam; // just in case we allow saving while moving!
        return savedata;
    }

    public void RestoreState(object state)
    {

        Dictionary<string, object> savedata = (Dictionary<string, object>)state;
        FogOfWarManager.INSTANCE.SetTextureToSerializer(savedata["fogMap"]);
        TestScenario = (int)savedata["testScenario"];
        hasCompletedDrivingExam = (int)savedata["hasCompletedDrivingExam"];
        // haha! fooled you!
        // I can't test scenario if loading a game, as the change will override stuff.
        // TestScenario = 10;
        // and then again, loading the game 

    }
}
