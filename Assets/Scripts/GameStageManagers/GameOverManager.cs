using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverManager : MonoBehaviour
{
    [SerializeField] PanelBeh gameOverPanel;
    [SerializeField] PanelBeh winPanel;
    private void Awake()
    {
        GameManager.OnStageChange += GameOver;
    }

    private void OnDestroy()
    {
        GameManager.OnStageChange -= GameOver;
    }

    private void GameOver(GameManager.Stage sOld, GameManager.Stage sNew)
    {

        if (sOld != GameManager.Stage.gameover && sNew == GameManager.Stage.gameover)
        {
            Debug.Log("We have caught the GameOver Event");
            PlayerMovement.Instance.ToggleLights(false);
            GUIHandler.Instance.EnableMainCam();
            gameOverPanel.Deploy();
        }

        if (sOld == GameManager.Stage.gameover && sNew != GameManager.Stage.gameover)
        {
            gameOverPanel.Hide();
        }

        if (sOld != GameManager.Stage.win && sNew == GameManager.Stage.win)
        {
            Debug.Log("We have caught the Win Event");
            PlayerMovement.Instance.ToggleLights(false);
            GUIHandler.Instance.EnableMainCam();
            winPanel.Deploy();
        }
        if (sOld == GameManager.Stage.win && sNew != GameManager.Stage.win)
        {
            winPanel.Hide();
        }
    }

    public void OnEscGamoeOverPanel()
    {
        gameOverPanel.Hide();
        GameManager.Instance.StageChange(GameManager.Stage.menu);
    }


}
