using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStageManager : MonoBehaviour
{
    [SerializeField] PanelBeh lowerInstrumentPanel;
    [SerializeField] PanelBeh rightInstrumentPanel;
    [SerializeField] PanelBeh topGUIPanel;
    [SerializeField] List<AudioSource> audioSources = new List<AudioSource>();
    public GameManager.Stage previousStage; // we'll use it for pause stage, so it always returns to normal stage.

    private void Awake()
    {
        GameManager.OnStageChange += GamePanels;

        // because the main menu may not me the first stage. especially in testing
        Invoke("PanelInitialStatusCheck", 0.5f);

        audioSources[1].Pause();
        audioSources[2].Pause();
        audioSources[3].Pause();
        audioSources[0].Pause();
    }

    private void OnDestroy()
    {
        GameManager.OnStageChange -= GamePanels;
    }

    private void GamePanels(GameManager.Stage sOld, GameManager.Stage sNew)
    {

        if (sOld != GameManager.Stage.game && sNew == GameManager.Stage.game)
        {
            if (sOld != GameManager.Stage.paused) PlayerMovement.Instance.ToggleLights(true);
            lowerInstrumentPanel.Deploy(); 
            rightInstrumentPanel.Deploy();
            topGUIPanel.Deploy();

            if (PlayerMovement.Instance.IsReversing()) audioSources[1].UnPause();
            audioSources[2].UnPause();
            audioSources[3].UnPause();
            audioSources[0].UnPause();
        }


        if (sOld == GameManager.Stage.game && sNew != GameManager.Stage.game && sNew != GameManager.Stage.paused)
        {
            lowerInstrumentPanel.Hide();
            rightInstrumentPanel.Hide();
            topGUIPanel.Hide();
            PlayerMovement.Instance.ToggleLights(false);
            PlayerMovement.Instance.ClearInteractible();

            audioSources[1].Pause();
            audioSources[2].Pause();
            audioSources[3].Pause();
            audioSources[0].Pause();
        }

        // pausing the game
        if (sOld == GameManager.Stage.game && sNew == GameManager.Stage.paused)
        {
            previousStage = sOld;
            lowerInstrumentPanel.Hide();
            rightInstrumentPanel.Hide();
            // topGUIPanel.Hide();
            // PlayerMovement.Instance.ToggleLights(false);
            // PlayerMovement.Instance.ClearInteractible();


            audioSources[1].Pause();
            audioSources[2].Pause();
            audioSources[3].Pause();
            audioSources[0].Pause();
        }
    }

    private void PanelInitialStatusCheck()
    {
        if (GameManager.Instance.stage != GameManager.Stage.game)
        {
            lowerInstrumentPanel.Hide();
            rightInstrumentPanel.Hide();
            topGUIPanel.Hide();
            PlayerMovement.Instance.ToggleLights(false);
            PlayerMovement.Instance.ClearInteractible();
        }
        else
        {
            PlayerMovement.Instance.ToggleLights(true);
            lowerInstrumentPanel.Deploy();
            rightInstrumentPanel.Deploy();
            topGUIPanel.Deploy();
        }
    }



}
