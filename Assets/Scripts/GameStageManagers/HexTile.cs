using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.AI;

public class HexTile : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI label;
    //[SerializeField] NavMeshSurface surf; 
    private float alphaIndex = 0;
    public string tileName; // it has to be humanely recognisable.
    public List<HexTile> neighbours = new List<HexTile>();
    private HexTile mostPopulousNeighbour;
    public Vector2Int coords;
    public int ringNumber; // how many from the center
    public float population;
    public float weight;
    private float vertDiameter = 150f; // + 10f; // in reality it's 20, but the teeth overlap
    private float horiDiameter = 173f; // + 10f;
    private bool activated = false;     // objects on this tile may hjave properties that should not be active before the tile is placed.
                                        // therefore activation is done by the game manager ot HexaWorld in GameManager on assignment of a tile position. 

    /* weight - probability of being close to the hub. SHould be set in the inspector
     * population - set in inspector. not to cluster towns. they should be spaced with fillers. 
     * coodrs - set by HexaWorld on world creation (or seed reset)
     * 
     */


    private void Start()
    {
        if (GameManager.Instance.isHexaWorld) HexaWorld.Instance.Announce(this);
    }

    private void Update()
    {
        if (alphaIndex > 0) { 
            transform.GetComponent<Renderer>().material.color = new Color(transform.GetComponent<Renderer>().material.color.r, transform.GetComponent<Renderer>().material.color.g, alphaIndex, alphaIndex); 
            alphaIndex -= Time.deltaTime; 
        }
    }

    public void SetCoords(Vector2Int v)
    {
        coords = v;
        if (v.y % 2 == 0)
        {
            transform.position = new Vector3(v.x * horiDiameter + horiDiameter/2, 0, v.y * vertDiameter);
        } else
        {
            transform.position = new Vector3(v.x * horiDiameter, 0, v.y * vertDiameter);
        }

        transform.GetComponent<Renderer>().material.color = new Color(weight / 10 * ((population > 1f) ? 3 : 1), weight / 10, alphaIndex, alphaIndex);
        // if (label != null) label.text = v.x + "," + v.y;
    }
    
    
    
    public void SetMostPopulous(HexTile h)
    {
        mostPopulousNeighbour = h;
    }
    
    public void PingHex()
    {
        alphaIndex = 1;
        
    }    


    public void SetActive()    
    {        
        activated = true;
        //if (surf != null) surf.BuildNavMesh();
    }
    public bool GetActive()    {        return activated;    }

}
