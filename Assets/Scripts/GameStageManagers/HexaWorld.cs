using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HexaWorld : MonoBehaviour, GameDevTV.Saving.ISaveable
{

    [HideInInspector] public static HexaWorld Instance { get; private set; }
    private List<HexTile> tiles = new List<HexTile>();
    [SerializeField] GameObject tilePref;
    [SerializeField] int seed;


    void Awake()
    {
        if (Instance == null) { Instance = this; }
        Invoke("SpawnBunchOfRandomHexTiles", 0.2f); // maybe coroutine woul be more elegant?
        Invoke("SortHexTileList", 0.3f); // maybe coroutine woul be more elegant?
        // Invoke("ArrangeHexTiles", 0.5f); // maybe coroutine woul be more elegant?
        StartCoroutine(ArrangeHexTiles());
    }

    public void Announce(HexTile h)
    {
        tiles.Add(h);
        // Debug.Log("adding hex tile " + h.coords);
    }

    private void SpawnBunchOfRandomHexTiles()
    {
        Random.InitState(seed);
        for (int i = 0; i < 128; i++)
        {
            GameObject _tile = Instantiate(tilePref, transform);
            _tile.GetComponent<HexTile>().weight = Random.Range(0.5f, 6f);
            _tile.GetComponent<HexTile>().population = Random.Range(-20f, 5f);
            if (_tile.GetComponent<HexTile>().population < 0) _tile.GetComponent<HexTile>().population = Random.Range(0f, 0.3f); // only one fifth of tiles is populated.
            _tile.GetComponent<HexTile>().SetCoords(new Vector2Int(100, 100));

        }

    }

    public Vector2Int GetHexTileCoordinatesByName(string name)
    {
        foreach (HexTile ht in tiles)
        {
            if (name == ht.tileName) return ht.coords;
        }

        return Vector2Int.zero;

    }
    public Vector3 GetHexTileWorldCoordinatesByName(string name)
    {
        foreach (HexTile ht in tiles)
        {
            if (name == ht.tileName) return ht.transform.position;
        }

        return Vector3.zero;

    }

    IEnumerator ArrangeHexTiles()
    {

        yield return new WaitForSeconds(0.5f);

        List<HexTile> _tiles = tiles.OrderBy(t => t.weight).ToList(); // ToList optional

        int _ring = 0;
        Vector2Int nextTile = new Vector2Int(0, 0);
        // here we spawn the HUB tile.

        _tiles[0].GetComponent<HexTile>().SetCoords(nextTile);
        _tiles[0].GetComponent<HexTile>().name = "The Hub";
        _tiles[0].GetComponent<HexTile>().ringNumber = _ring;
        _tiles.RemoveAt(0);

        for (int i = 0; i < 6; i++) // number of rings
        {
            _ring++;

            //dir4
            nextTile = new Vector2Int(nextTile.x + ((nextTile.y % 2 == 0) ? 1 : 0), nextTile.y + 1);
            //GameObject _tile = Instantiate(tilePref, transform);
            //_tile.GetComponent<HexTile>().SetCoords(nextTile);
            //_tile.GetComponent<HexTile>().ringNumber = _ring;
            //_tile.GetComponent<HexTile>().name = nextTile.x + ", " + nextTile.y + ", in ring " + _ring;
            for (int dir = 0; dir < 6; dir++) // mamy 6 bok�w
            {

                for (int btch = 0; btch < _ring; btch++) // dlugosc kazdej sciany
                {
                    switch (dir)
                    {
                        case 0:
                            nextTile = new Vector2Int(nextTile.x + ((nextTile.y % 2 == 0) ? 1 : 0), nextTile.y - 1);
                            break;
                        case 1:
                            nextTile = new Vector2Int(nextTile.x - ((nextTile.y % 2 == 0) ? 0 : 1), nextTile.y - 1);
                            break;
                        case 2:
                            nextTile = new Vector2Int(nextTile.x - 1, nextTile.y);
                            break;
                        case 3:
                            nextTile = new Vector2Int(nextTile.x - ((nextTile.y % 2 == 0) ? 0 : 1), nextTile.y + 1);
                            break;
                        case 4:
                            nextTile = new Vector2Int(nextTile.x + ((nextTile.y % 2 == 0) ? 1 : 0), nextTile.y + 1);
                            break;
                        case 5:
                            nextTile = new Vector2Int(nextTile.x + 1, nextTile.y);
                            break;

                    }


                    int _ind = 0;
                    float maxPopNeighbour = GetNeighboursPopulation(nextTile).Max();

                    while (_tiles[_ind].population > 1f && maxPopNeighbour > 1f)
                    {
                        //Debug.Log("As we couldn't neighbour population of "+ _tiles[_ind].population + " to Maximum population in this neighbourhood is " + maxPopNeighbour + "   in coords "+ nextTile.x + ", " + nextTile.y);
                        _ind++;
                        //Debug.Log("We've increased index to " + _ind + " and will try with population of " + _tiles[_ind].population);

                    }
                    //Debug.Log("Maximum population in this neighbourhood is " + maxPopNeighbour + " in coords "+ nextTile.x + ", " + nextTile.y + " ...placing tile now");
                    _tiles[_ind].GetComponent<HexTile>().SetCoords(nextTile);
                    _tiles[_ind].GetComponent<HexTile>().name = nextTile.x + ", " + nextTile.y + ", in ring " + _ring + ", dir " + dir;
                    _tiles[_ind].GetComponent<HexTile>().ringNumber = _ring;
                    _tiles[_ind].GetComponent<HexTile>().SetActive();
                    _tiles.RemoveAt(_ind);

                }
            }
            yield return new WaitForSeconds(1);

        }
        GameManager.Instance.TestScenarioChange();
        yield return null;

    }


    private void SortHexTileList()
    {

        IEnumerable<HexTile> query = tiles.OrderBy(t => t.weight);
        Vector2Int v = new Vector2Int(0,0);
        
        foreach (HexTile t in query)
        {
            // Debug.Log(t.tileName + "   "  + t.weight);
        }
    }

    private float[] GetNeighboursPopulation(Vector2Int coords)
    {
        float[] itl = new float[6]; // i FEEL IT SHOULD BE 5, BUT IT WAS THROWING ERRORS IF MORE PRE-MADE TILES WERE ADDED.
                                    // and thge reason is, in the beginning, all the tiles have 0,0 coords
                                    // as long as new tiles have BIG coords that don't pretend neighbours, all is well.
        int i = 0;
        foreach (HexTile t in tiles)
        {
            if ((t.coords.y == coords.y && (coords.x - 1 == t.coords.x || t.coords.x == coords.x + 1))
                || (t.coords.y - 1 == coords.y && (coords.x + ((coords.y % 2 == 0)? 1 : -1) == t.coords.x || t.coords.x == coords.x))
                || (t.coords.y + 1 == coords.y && (coords.x + ((coords.y % 2 == 0) ? 1 : -1) == t.coords.x || t.coords.x == coords.x))
                ) 
            {
                // Debug.Log("Adding to itl " + coords.x + " " + coords.y + "  samo " + t.coords.x + " " + t.coords.y + " ");
                GameManager.Instance.UpdateDebugConsole(t.tileName + "  " + i.ToString());
                itl[i] = t.population; 
                i++;
                t.PingHex();
                
            }
        }

        return itl;
    }


    public object CaptureState()
    {

        Debug.Log("seed saved as " + seed);
        return seed;
    }

    public void RestoreState(object state)
    {
        seed = (int)state;
        Debug.Log("seed set to " + seed);
    }
}
