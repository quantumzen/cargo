using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] PanelBeh mainMenuPanel;
    [SerializeField] PanelBeh captainPanel;
    [SerializeField] Transform cmspot;

    [SerializeField] Transform cmCaptainspot;
    [SerializeField] GameObject MenuStage;
    [SerializeField] Transform triLightForCaptain;
    [SerializeField] GameObject captainLinePrefab;
    [SerializeField] Transform captainLineParent;
    [SerializeField] TMPro.TextMeshProUGUI availableSkillPointsDisplay;
    [SerializeField] Button captainReady;
    [SerializeField] TMPro.TextMeshProUGUI captainNameEditBox;
    public int availableSkillPoints;
    Dictionary<int, CaptainSkillLine> skillLines = new Dictionary<int, CaptainSkillLine>();

    private void Awake()
    {
        GameManager.OnStageChange += MainMenu;

        // because the main menu may not me the first stage. especially in testing
        //Invoke("PanelInitialStatusCheck", 0.5f);

    }

    private void Start()
    {
        PopulateCaptainSkillLines();
    }

    private void OnDestroy()
    {
        GameManager.OnStageChange -= MainMenu;
    }

    private void MainMenu(GameManager.Stage sOld, GameManager.Stage sNew)
    {
        
        Debug.Log("We have caught stagechange Event [" + sOld.ToString() + "] to [" + sNew.ToString() + "]");
        if (sOld != GameManager.Stage.menu && sNew == GameManager.Stage.menu)
        {
            MenuStage.SetActive(true);
            WipePlayer();
            PlayerMovement.Instance.ToggleLights(false);
            GUIHandler.Instance.EnablePortCam(cmspot);
            mainMenuPanel.DelayedDeploy(0.5f);
        }


        if ((sOld == GameManager.Stage.menu || sOld == GameManager.Stage.captain) && sNew != GameManager.Stage.menu && sNew != GameManager.Stage.captain)
        {
            MenuStage.SetActive(false);
            mainMenuPanel.Hide();
            GUIHandler.Instance.EnableMainCam();
            triLightForCaptain.gameObject.SetActive(true);
        }


        if (sOld == GameManager.Stage.menu && sNew == GameManager.Stage.captain) // tweorZymy kapitana i take dalej =------------------------------
        {

            GUIHandler.Instance.EnableCaptainCam();
            triLightForCaptain.gameObject.SetActive(true);

            mainMenuPanel.Hide();
            captainPanel.DelayedDeploy(1);

            UpdateAllButtons();
        }
        if (sOld == GameManager.Stage.captain && sNew != GameManager.Stage.captain) // tweorZymy kapitana i take dalej =------------------------------
        {

            //GUIHandler.Instance.EnableCaptainCam();
            triLightForCaptain.gameObject.SetActive(false);
            captainPanel.Hide();
        }


    }

    private void WipePlayer()
    {
        availableSkillPoints = 4;

    }

    private void PanelInitialStatusCheck()
    {
        if (GameManager.Instance.stage != GameManager.Stage.menu)
        {
            if (mainMenuPanel.isDeployed) mainMenuPanel.Hide();
            MenuStage.SetActive(false);
        }
        else
        {
            MenuStage.SetActive(true);
            if (!mainMenuPanel.isDeployed) mainMenuPanel.Deploy();
            GUIHandler.Instance.EnableCaptainCam();
            
        }
    }

    public void OnButtonNewGame()
    {
        // move to capitan creation
        GameManager.Instance.StageChange(GameManager.Stage.captain);
        
    }


    public void OnButtonCapitanDone()
    {
        // doesn't load the game // or loads a default one
        // sets up the initial placement and quests

        GetComponent<PlayerCharacter>().SetCaptainName(captainNameEditBox.text);
        GameManager.Instance.StageChange(GameManager.Stage.game);
        GameManager.Instance.GetComponent<Humans>().InvokeUpdateOfStats("You"); 
        GUIHandler.Instance.EnableMainCam();
        // here load from Resources.
        GetComponent<SaveWrapper>().Load("sc0" + GameManager.Instance.TestScenario);
    }

    public void OnButtonMainMenu()
    {
        PlayerMovement.Instance.EscButtonPressedOnPanel();
        GameManager.Instance.StageChange(GameManager.Stage.menu);
    }

    public void OnButtonQuit()
    {
        Application.Quit();
    }
    public void OnButtonContinue()
    {
        // load game and resume
        GameManager.Instance.StageChange(GameManager.Stage.game);
        GetComponent<SaveWrapper>().Load("quicksave");
    }

    public void OnButtonBackToMenu()
    {
        GameManager.Instance.StageChange(GameManager.Stage.menu);
    }

    private void LoadGame()
    {
        // multiple game files?
        // needs to load inventories of every port, player, enemy
    }




    
    #region methods used by captain skill assignment line


    private void PopulateCaptainSkillLines()
    {
        for (int i = 0; i < 11; i++)
        {
            if (PlayerCharacter.Instance.GetSkillName(i) != "")
            {
                CaptainSkillLine csl = Instantiate(captainLinePrefab, captainLineParent).GetComponent<CaptainSkillLine>();
                csl.pb = this;
                csl.SetSkillName(PlayerCharacter.Instance.GetSkillName(i));
                csl.SetSkillValue(PlayerCharacter.Instance.GetCaptainOnlySkillValue(i));
                csl.skillNumber = i;
                skillLines.Add(i, csl);
            }
        }

    }
    public void OnBuyClicked(int skillNumber)
    {
        // get  current skill amount (value) of skillNumber.
        // use a table or some logic to assess how much
        //if (pb.availableSkillPoints <= 0 || (pb.availableSkillPoints <= 1 && skillValue >= 12) || (pb.availableSkillPoints <= 3 && skillValue >= 16) || (pb.availableSkillPoints <= 4 && skillValue >= 18)) buy.interactable = false;
        // subtract skill points
        // foreach and  UpdateButtons(); on all

        int cost = 1;
        if (GetSkillValue(skillNumber) >= 12) cost = 2;
        if (GetSkillValue(skillNumber) >= 16) cost = 3;
        if (GetSkillValue(skillNumber) >= 18) cost = 4;


        availableSkillPoints -= cost;

        PlayerCharacter.Instance.SetSkillValue(skillNumber, GetSkillValue(skillNumber) + 1);
        UpdateAllButtons();

    }

    public void OnSellClicked(int skillNumber)
    {

        int cost = 1;
        if (GetSkillValue(skillNumber) > 12) cost = 2;
        if (GetSkillValue(skillNumber) > 16) cost = 3;
        if (GetSkillValue(skillNumber) > 18) cost = 4;


        availableSkillPoints += cost;

        PlayerCharacter.Instance.SetSkillValue(skillNumber, GetSkillValue(skillNumber) - 1);
        UpdateAllButtons();

    }

    public int  GetSkillValue(int skillNumber)
    {
        return PlayerCharacter.Instance.GetCaptainOnlySkillValue(skillNumber);
    }

    private void UpdateAllButtons()
    {
        foreach (KeyValuePair<int, CaptainSkillLine> _e in skillLines)
        {
            _e.Value.SetSkillValue(GetSkillValue(_e.Key));
            _e.Value.UpdateButtons();
        }
        availableSkillPointsDisplay.text = availableSkillPoints.ToString();
        if (availableSkillPoints == 0 && captainNameEditBox.text.Length > 1)
        {
            captainReady.interactable = true;
        } else
        {
            captainReady.interactable = false;
        }
    }

    public void OnCaptainNameInputFieldChanged()
    {
        UpdateAllButtons();
    }

    #endregion


}
