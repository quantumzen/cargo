using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour, GameDevTV.Saving.ISaveable
{
    [SerializeField] float health = 1f; // let 1 be max
    [SerializeField] bool DisplayHealthBar;
    [SerializeField] Slider healthBar;
    Vector3 healthBarInitialPosition;
    private float initialHealth;
    public string lastHitName { get; private set; }
    public string lastHitFingerprint { get; private set; }
    public float lastHitDamage { get; private set; }
    public Transform lastHitTransform { get; private set; }
    bool isPlayer;
    private float healthBarBackgroundAlpha;
    private float healthBarAlpha;
    [SerializeField] private Image healthBarBackgroundImage;
    [SerializeField] private Image healthBarImage;
    private float healthBarPersistenceCount;

    public static event Action OnDeath;
    public static event Action<Health, Transform> OnHit;

    void Start()
    {
        if (transform.CompareTag("Player"))
        {
            isPlayer = true; // buy locally
        }
        initialHealth = health;
        if (healthBar != null)
        {
            healthBarInitialPosition = healthBar.transform.position - transform.position;
            healthBarBackgroundAlpha = healthBarBackgroundImage.color.a;
            healthBarAlpha = healthBarImage.color.a;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (DisplayHealthBar)
        {
            healthBar.value = health/initialHealth;
            healthBar.transform.position = healthBarInitialPosition +transform.position;
            if (health < initialHealth && healthBarPersistenceCount > 0 && health > 0)  
            {  
                healthBar.gameObject.SetActive(true);
                healthBarBackgroundImage.color = new Color(healthBarBackgroundImage.color.r, healthBarBackgroundImage.color.g, healthBarBackgroundImage.color.b, healthBarBackgroundAlpha * Mathf.Min(1f, healthBarPersistenceCount));
                healthBarImage.color = new Color(healthBarImage.color.r, healthBarImage.color.g, healthBarImage.color.b, healthBarAlpha * Mathf.Min(1f, healthBarPersistenceCount));
                healthBarPersistenceCount -= Time.deltaTime;
            } else { healthBar.gameObject.SetActive(false); }
        }
    }


    public float GetHealth()
    {
        return health;

    }
    public void SetHealth(float h)
    {
        h = Mathf.Min(h, initialHealth);
        health = h;
        if (health <= 0) DeathTrigger();

    }

    public void SetLastHit(string s)    {        lastHitName = s;    }
    public void SetLastHitTransform(Transform trans) { lastHitTransform = trans; }

    public void AddHealth(float h)
    {
        SetHealth(health + h);
        if (isPlayer) Quality.Instance.SetQuality(3003, Mathf.FloorToInt(health * 100));
        healthBarPersistenceCount += 1f;
    }

    void DeathTrigger()
    {
        Debug.Log(transform.name + " has been destroyed by " + lastHitName);
        if (isPlayer)
        {

            Debug.Log("Health is Gameovering");
            GameManager.Instance.StageChange(GameManager.Stage.gameover);

        }
        else
        {
            healthBar.gameObject.SetActive(false);
            OnDeath?.Invoke();
            // Destroy(gameObject); Health shouldn't be destroying things. Things should be destroying themselves, having checked health.
        }
        
        
    }

    // Health is handling collisions. I can't find a better place for it. Same for the player

    private void OnCollisionEnter(Collision collision)
    {
        // collision.collider.transform.GetComponent<Health>().AddHealth(-damagePower);
        // collision.collider.transform.GetComponent<Health>().SetLastHit(originatorName);
        if (collision.collider.transform.CompareTag("Projectile"))
        {
            ProjectileBeh p = collision.collider.transform.GetComponent<ProjectileBeh>();
            lastHitDamage = p.GetDamagePower();
            AddHealth(-lastHitDamage);
            lastHitName = p.GetOriginatorName();
            lastHitFingerprint = p.GetOriginatorFingerprint();
            lastHitTransform = p.GetOriginator();
            Debug.Log(transform.name + " was hit by " + lastHitName);
            if (isPlayer) GUIHandler.Instance.SetUpperGUIText();


            OnHit?.Invoke(this, lastHitTransform);


            //if (lastHitName == "Player")
            //{
            //    if(TryGetComponent<EnemyShooterBeh>(out EnemyShooterBeh esb))
            //    {
            //        esb.AddAwareness(10f);
            //    }

            //}
        }
    }

    

    public object CaptureState()
    {
        return health;
    }

    public void RestoreState(object state)
    {
        health = (float)state;
    }
}
