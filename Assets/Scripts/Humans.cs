using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Humans : MonoBehaviour
{
    [SerializeField] List<illustration> pics = new List<illustration>();
    private string currentName = "";
    [SerializeField] List<string> aboard = new List<string>();

    [Serializable] public struct illustration
    {
        public Sprite pic;
        public string name;
        public string description;
        public string role; // can't afford an enum here now. It will be a string : navigator or aurer or passenger
    }


    public static event Action<String> OnCrewChange;

    /*
    void YounolongerneedtoUpdate()
    
    {
        if (currentName == label.text) return;
        foreach (illustration i in pics)
        {
            if (i.name == label.text)
            {
                GetComponent<Image>().sprite = i.pic;
                currentName = label.text;
                Debug.Log("We've changed the name to the one of " + currentName);
                return;
            }
        }

        Debug.Log("We've have no picture of " + label.text);
        GetComponent<Image>().sprite = null;
        currentName = label.text; // so we don't waste cycles doing the same thing every frame expecting a different result.

    }
    */

    private void Start()
    {
        int i = 0;

        foreach (var ill in pics)
        {
            Quality.Instance.InitialiseQualityEntry(ill.name + " is on board", i + 2000); // deprecated AddToDictionary
            i++;
        }


    }

    public Sprite GetFace(string name)
    {
        foreach (illustration i in pics)
        {
            if (i.name == name)
            {
                return i.pic;
            }
        }
        return pics[0].pic;
    }

    public String GetDescription(string name)
    {
        // I know, a scriptable object would've ben beautiful, but really, I can't really afford it right now.
        foreach (illustration i in pics)
        {
            if (i.name == name)
            {
                if (name == aboard[0]) return i.description + GetCeptainBlurb();
                if (GetRole(name) == "navigator" || GetRole(name) == "aurer") return i.description + "<br><br>"+GetCrewmateBlurb(name);
                return i.description;
            }
        }
        return pics[0].description;

    }

    public String GetRole(string name)
    {
        foreach (illustration i in pics)
        {
            if (i.name == name)
            {
                return i.role;
            }
        }
        return null;
    }

    public List<string> GetHumansAboard()
    {
        return aboard;
    }

    public void AddHuman(string name)
    {
        // check if enough room - Vessel does this
        // check if not already aboard - vessel will do this, too.
        aboard.Add(name);
        int i = 0;
        for (i = 0; i < pics.Count; i++)
        {
            if (pics[i].name == name) break;
        }
        Quality.Instance.IncrementQuality(i + 2000, 1);
        if (GetRole(name) == "navigator" || GetRole(name) == "aurer") OnCrewChange?.Invoke(name);
    }

    public void InvokeUpdateOfStats(string name) // this method is a dirty hack fo the captain's stats to be updated anfter the start selection.
                                                 // it is going to be called by MainMenuManager.OnButtonCapitanDone()
    {
        Debug.Log("Adding Captain");
        OnCrewChange?.Invoke(name);
    }


    public void RemoveHuman(string name)
    {
        // check if enough room - Vessel does this
        // check if not already aboard - vessel will do this, too.
        aboard.Remove(name);

        int i = 0;
        for (i = 0; i < pics.Count; i++)
        {
            if (pics[i].name == name) break;
        }
        Quality.Instance.IncrementQuality(i + 2000, -1);
        if (GetRole(name) == "navigator" || GetRole(name) == "aurer") OnCrewChange?.Invoke(name);
    }


    public bool IsHumanAboard(string name)
    {
        return aboard.Contains(name);
    }

    public int CountHumansAboard()
    {
        return aboard.Count;

    }

    private string GetCeptainBlurb()
    {
        string s = "";
        PlayerCharacter pch = GetComponent<PlayerCharacter>();

        for (int i = 1; i < 11; i++)
        {
            if (pch.GetSkillName(i).Length > 0)
            {
                string _bonusValue = "";
                int _val = pch.GetSkillValue(i) - pch.GetCaptainOnlySkillValue(i);
                if (_val > 0) _bonusValue = " +" + _val.ToString();
                s += pch.GetSkillName(i) + "  <b>" + pch.GetCaptainOnlySkillValue(i).ToString() + "</b>" + _bonusValue + "<br>";
            }
        }



        return s;
    }
    private string GetCrewmateBlurb(string name)
    {
        string s = "";
        PlayerCharacter pch = GetComponent<PlayerCharacter>();
        
        for (int i = 1; i < 11; i++)
        {
            int _val = pch.GetCrewmateSkillValue(name, i);
            if (pch.GetSkillName(i).Length > 0 && _val != 0)
            {
                string _bonusValue = "";
                if (_val > 0) _bonusValue = " +" + _val.ToString();
                if (_val < 0) _bonusValue = " -" + Mathf.Abs(_val).ToString();
                s += pch.GetSkillName(i) + "  <b>" + _bonusValue + "</b>"+"<br>";
            }
        }



        return s;
    }

}
