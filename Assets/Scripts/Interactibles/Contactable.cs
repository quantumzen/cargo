using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevTV.Saving;

public class Contactable : MonoBehaviour, ISaveable
{
    // this means the object can trigger a "contact".
    [SerializeField] string fingerprint;
    public enum Faction { Pirates, Neutrals, Hattso, Peacekeepers }
    [SerializeField] Faction faction;
    public float heat = 0;
    public bool isDead = false; // this is updated by enemyshooterbeh in Health ondeathHandle

    public object CaptureState()
    {
        return fingerprint;
    }

    public void RestoreState(object state)
    {
        fingerprint = (string)state;
    }

    void Start()
    {
        if (fingerprint == "") fingerprint = System.Guid.NewGuid().ToString();
        GameManager.Instance.RegisterFingerprint(fingerprint, transform);
    }

    public string GetFingerprint() { return fingerprint; }
    public Faction GetFaction() { return faction; }

}
