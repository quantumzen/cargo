using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevTV.Saving;


public class Interactible : MonoBehaviour, ISaveable
{

    public bool dockable;
    public bool collectable;
    public bool hailable;
    public bool interactionTrigger;
    public RPG.Dialogue.Dialogue NPCDialogue; // these used for hailable only
    public RPG.Dialogue.DialogueNode currentNode;
    public portBeh proxyPort; // this is needed in multibay ports like petrol stations to hand over to another interactible.




    private void OnTriggerEnter(Collider other)
    {

        if (other.name == "dockingAnchor")  // I don't need this anymore: gameObject.CompareTag("Player"))
        {
            // must check if the player is the interactible.
            //GUIHandler gh = other.gameObject.GetComponentInParent<GUIHandler>();
            //gh.SetLowerGUIText("press E to dock");
            PlayerMovement.Instance.SetInteractible(transform);

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "dockingAnchor")  // I don't need this anymore: gameObject.CompareTag("Player"))
        {
            //GUIHandler gh = other.gameObject.GetComponentInParent<GUIHandler>();
            //gh.SetLowerGUIText("");
            PlayerMovement.Instance.ClearInteractible();

            // I have a feeling this will result in a bug, but I can't see how trigger exit should be relevant to a dialogue.
            //if (RPG.Dialogue.Conversant.Instance.GetCurDialogue() == NPCDialogue)
            //{
            //    RPG.Dialogue.Conversant.Instance.CloseDialogue();
            //}
        }

    }

    public void SetDialogue(RPG.Dialogue.Dialogue d)
    {
        NPCDialogue = d;
    }

    public void ClearHailable()
    {
        hailable = false;
    }

    public void ClearInteractionTrigger()
    {
        interactionTrigger = false;
    }

    public object CaptureState()
    {
        Dictionary<string, object> savedata = new Dictionary<string, object>();
        savedata["dockable"] = dockable;
        savedata["collectable"] = collectable;
        savedata["hailable"] = hailable;
        savedata["interactionTrigger"] = interactionTrigger;
        if (currentNode == null)
        {
            savedata["curNodeName"] = "";
        }
        else
        {
            savedata["curNodeName"] = currentNode.name;
        }
        // Debug.Log("the node of " + transform.name + " dialogue " + NPCDialogue.name + " -> " + savedata["curNodeName"]);
        return savedata;
    }

    public void RestoreState(object state)
    {

        Dictionary<string, object> savedata = (Dictionary<string, object>)state;
        dockable = ((bool)savedata["dockable"]);
        collectable = ((bool)savedata["collectable"]);
        hailable = ((bool)savedata["hailable"]);
        interactionTrigger = ((bool)savedata["interactionTrigger"]);
        currentNode = GameManager.Instance.GetComponent<RPG.Dialogue.Conversant>().GetDialogueNodeByName(NPCDialogue,((string)savedata["curNodeName"]));
    }
}
