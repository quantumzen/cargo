using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
    quest marker has:
    pinnacle - the point where it is to be resolved or interacted with? This contains all data

    circle - perimeter with trigger collider. itaractibnle. collider may cascade the collision ti interactible on the parent.



    quest marker does:
    marks the center of the minimapMark on the compass
    marks edge of circle with particles. this should not be dependent on the isVisible, as it's marked by NPC

    On circle enter, cilcle disappears. Pinnacle appears (becomes enabled?). Are some pinnacles invisible? they might if isVisible is off.

    what do I need:
    Draw circle k  (line renderer)
    1000 particles per 5 radius
*/


[RequireComponent(typeof(LineRenderer))]
public class InteractibleCircle : MonoBehaviour
{

    LineRenderer lr;
    SphereCollider collider;

    [SerializeField] ParticleSystem part;
    [SerializeField] float radius;
    [SerializeField] GameObject toActivate;
    [SerializeField] string ActionToTrigger;
    private Transform compassMark;
    [SerializeField] GameObject compassMarkCustom;
    [SerializeField] bool compassTracked;
    [SerializeField] bool SingleUse;
    [SerializeField] bool MinimapCircleVisible = true;
    void Start()
    {
        lr = GetComponent<LineRenderer>();
        collider = GetComponent<SphereCollider>();
        Invoke("DrawCircleInvDelayer", 1f);
        // DrawCircle(32, 5);
    }

    // Update is called once per frame
    void Update()
    {
        // must keep bearing of my compass mark updated
        if (compassMark != null)  compassMark.GetComponent<RawImage>().uvRect = new Rect(CompassHandler.Instance.Wrap(PlayerMovement.Instance.transform.localEulerAngles.y - GetBearingToCircle(), 360f) / 360f, 0, 1, 1);

    }

    private float GetBearingToCircle()
    {
        return Vector3.SignedAngle(Vector3.back, transform.position - PlayerMovement.Instance.transform.position + PlayerMovement.Instance.GetComponent<Rigidbody>().centerOfMass, Vector3.up) + 180;
    }

    void DrawCircleInvDelayer()
    {
        if (MinimapCircleVisible) DrawCircle(33); // adding one zero segment
        if (compassTracked) DrawCompassMark();
    }

    void DrawCircle(int segments)
    {

        // here we set the parameters for the particla system
        
        var em = part.emission;
        em.enabled = true;
        em.rateOverTime = 30f * radius;

        ParticleSystem.ShapeModule shapeModule = part.shape;
        shapeModule.radius = radius;

        // collider to match
        collider.radius = radius;


        // here we draw the line renderer circle for the minimap
        lr.positionCount = segments;
        lr.startWidth = 1;
        lr.endWidth = 1;
        for (int i = 0; i<segments; i++)
        {
            float progress = (float)i / (segments -1);
            float curRadian = progress * 2 * Mathf.PI;
            float _x = Mathf.Cos(curRadian);
            float _y = Mathf.Sin(curRadian);
            Vector3 curPos = new Vector3(_x * radius, 0, _y * radius);
            lr.SetPosition(i, curPos+transform.position);
        }
    }

    private void DrawCompassMark()
    {


        {
            // We need to make compass point representation!
            // if the navigator is skilled, maybe?
            if (compassMarkCustom == null) compassMarkCustom = CompassHandler.Instance.compassMarkPrefab;
            GameObject _g = Instantiate(compassMarkCustom, CompassHandler.Instance.transform, true); 
            // must be parented to the one that doesn't move. I guess. I'ts a hack. If my brain was fresh
            // NO! then it is not visible, damnit.
            // ...unless! ....unless I check its local position so it aligns with compass thing.
            // WRONG.


            compassMark = _g.transform;
            compassMark.localPosition = new Vector3(0, 0, 0);  // CompassHandler.Instance.transform.position; // 
        }
        // else // compassMark.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.name == "Player")
        {
            if (toActivate != null) toActivate.SetActive(true);
            if (ActionToTrigger != "") GetComponent<ActionTrigger>().Trigger(ActionToTrigger); //should be null? or ""?
            DisableCircle();
        }
    }

    public void DisableCircle()
    {
        part.Stop();
        if (SingleUse) collider.enabled = false;
        lr.positionCount = 0;
        //delete compasss point
        if (compassTracked && compassMark != null) Destroy(compassMark.gameObject);
    }

    public void OnDisable()
    {
        if (compassTracked && compassMark != null) compassMark.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        if (compassTracked && compassMark != null) compassMark.gameObject.SetActive(true);

    }
}
