using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loot : MonoBehaviour, GameDevTV.Saving.ISaveable
{
    public Dictionary<int, int> loot = new Dictionary<int, int>();
    private bool looted = false;
    [SerializeField] LootEdit[] lootEdit;
    [System.Serializable] public struct LootEdit
    {
        public int itemNum;
        public int quantity;
    }

    private void Start()
    {
        foreach (LootEdit _l in lootEdit)
        {
            loot.Add(_l.itemNum, _l.quantity);
        }
    }

    public object CaptureState()
    {
        return loot;
    }

    public void RestoreState(object state)
    {
        loot = (Dictionary<int, int>)state;
    }

    public void Looted()
    {
        if (loot.Count == 0)
        {
            looted = true;
            gameObject.SetActive(false);
        }
    }
}
