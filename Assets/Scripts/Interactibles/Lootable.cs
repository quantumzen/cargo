using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lootable : MonoBehaviour
{

    private Health health;
    private bool looted = false;
    [SerializeField] Transform lootToActivate;

    private void Start()
    {
        health = GetComponent<Health>();
    }

    private void Update()
    {
        if (health.GetHealth() <= 0)
        {
            if (!looted)
            {
                lootToActivate.gameObject.SetActive(true);
                lootToActivate.position = new Vector3(transform.position.x, 0, transform.position.z);
                looted = true;
                gameObject.SetActive(false);
            }
        }
    }
}
