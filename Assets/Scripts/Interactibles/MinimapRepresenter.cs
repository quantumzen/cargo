using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * There are two ways of handling this. 
 * First, display the map sprites. keep them disabled 
 * I will need to implement a different system for dynamic objects, a system where sprites appear/disappear.
 * 
 * Second, construct fog of war.
 * I will need to do it anyway
 * 
 */

public class MinimapRepresenter : MonoBehaviour
{
    private bool visible;
    private bool illuminationIntensity; // I guess how bright it is on the map?
    [SerializeField] float visibilityDecayFactor = 0; // 0 means it will remain permanently visible. Used for static stuff.
    [SerializeField] float affinity = 0.5f; // 0 is hostile, 1 is friendly, 0.5 is the default 
    [SerializeField] Image bgSprite;
    [SerializeField] Image overSprite;

    private Sprite[] circles;
    

    private void Start()
    {
        circles = GameManager.Instance.minimapRepresentations;
        if (overSprite == null)
        {
            if (GetComponent<EnemyShooterBeh>() != null)
            {
                overSprite.sprite = circles[4];
                affinity = 0;
            }
            if (GetComponent<Loot>() != null)
            {
                overSprite.sprite = circles[5];
                affinity = 0.5f;
            }
            if (GetComponent<portBeh>() != null)
            {
                overSprite.sprite = circles[6];
                affinity = 1f;
            }
        }


        UpdateSprite();
    }



    void UpdateSprite()
    {
        bgSprite.sprite = circles[1];
        if (affinity < 0.33f) bgSprite.sprite = circles[0];
        if (affinity > 0.66f) bgSprite.sprite = circles[2];

    }

    void DecayVisibility()
    {

    }


}
