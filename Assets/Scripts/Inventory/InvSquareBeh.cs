using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvSquareBeh : MonoBehaviour
{

    public InventoryPanel2 pb;
    [SerializeField] TMPro.TextMeshProUGUI qty;
    [SerializeField] Image ico;
    private InventorySlot invSlot;

    public void SetQuantity(int q)
    {
        // Debug.Log("Invslot " + transform.name + " update quantity " + q);
        qty.text = q.ToString();
        if (q == 0) qty.text = "";
    }

    public void SetIcon(Sprite s)
    {
        ico.sprite = s;
        if (s == null) ico.sprite = GameManager.Instance.blankSprite; 
    }

    public void SetInventorySlot(InventorySlot i)    
    {        
        invSlot = i;
        i.invSquare = this;
    }
    public InventorySlot GetInventorySlot() { return invSlot; }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
