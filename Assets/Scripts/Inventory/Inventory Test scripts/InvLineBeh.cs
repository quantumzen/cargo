using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InvLineBeh : MonoBehaviour
{

    public TMP_Text itemName;
    public TMP_Text owned;
    public Transform rarity;
    public InventoryView pb; // we will inform the port authorities of any transactions
    public int num; // item number, so it can communicate with portbeh

    [SerializeField] bool isHeader = false;

    private void Start()
    {
        if (isHeader)
        {
            itemName.text = "commodity";
            owned.text = "cargo";
            rarity.GetComponent<Image>().sprite = null;
            rarity.transform.GetComponent<Image>().enabled = false;


        }
    }

}
