using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevTV.Saving;

public class Inventory : MonoBehaviour, ISaveable
{
    public List<InventorySlot> items = new List<InventorySlot>();

    [System.Serializable]
    public struct invslotSave
    {
        public int itemNumber;
        public int itemQuantity;
        public float quality;
    }

    private void Start()
    {
        //items.Clear();
        //Debug.Log("Clearing inventory of " + transform.name);
    }

    public InventorySlot CreateInventorySlot() // player will use this one to set up its inventory thing. In fact anything that holds inventory should.
    {
        InventorySlot islot = new InventorySlot(null,0,0); // inventorySlot is instantiating new Item. It's not nice to instantiate a scriptableObject.
        items.Add(islot);

        // Debug.Log("Creating invslot for " + transform.name + "  " + items.Count);
        return islot;
    }

    /* public void SetGold(int g) // this should be callsed only be PlayerMovement.SetGold
    {
        SetItemQuantity(1, g);
        //Debug.Log("Gold set in the LIST: " + items[gInd].item.name + " : " + items[gInd].quantity); // this line is throwing an error when the item hasn't been created yet.
    }
    */
    public int GetGold()
    {
        return GetItemQuantity(1);
    }

    // this method makes no sense. Why would I code it this way?!
    /*private int FindItemsIndex(int number) // returns -1 if not found. 
    {                                   // takes item.number  returnns index in the List items
        var indFinal = -1;
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].item.number == number) { 
                indFinal = i;
                break;
            }
        }
        return indFinal;
    }
    */

    private void ModifyQuality(int n, int quantity) // sends changes to Quality in order to change it. Mas?o ma?lane. 
    {
        if (IsPlayer()) Quality.Instance.IncrementQuality(n, quantity);
    }

    private bool IsPlayer()
    {
        return (transform.name == "Player");
    }

    public int AddItem(int num, int quantity) // it returns what couldn't be added.
    {
        int initialQ = quantity;
        // Debug.Log("giving " + quantity + " of " + num + " item: " + GameManager.Instance.GetItemByNumber(num).itemName + "  to: "+transform.name);
        if (num == 1) GUIHandler.Instance.SetUpperTextGold(quantity+ GetItemQuantity(num));

        if (quantity < 0) 
        {
            return SubtractItem(num, quantity);
        }

        while (quantity > 0)
        {
            InventorySlot islot = FindInventorySlotForItem(num);
            if (islot == null)
            {
                ModifyQuality(num, initialQ-quantity);
                return quantity;
            }
            islot.item = GameManager.Instance.GetItemByNumber(num); // in case the slot is blank
            quantity = AppendItemSlotQuantity(islot, quantity);
            islot.InvSquareUpdate();
        }

        ModifyQuality(num, initialQ - quantity);
        return quantity;


        // old way of doing things
        // SetItemQuantity(num, GetItemQuantity(num)+quantity);
    }
    public int SubtractItem(int num, int quantity) // it returns what couldn't be taken out.
    {
        int initialQ = quantity;
        while (quantity < 0)
        {
            InventorySlot islot = GetInventorySlotWIthFewestOfItem(num);
            if (islot == null)
            {

                ModifyQuality(num, -(initialQ - quantity));
                return quantity;
            }
            // check how much in slot
            // subtract quantity from it.
            // set
            quantity += islot.quantity;
            // Debug.Log("We've subtracted the quantity of this slot. Currently  qty is: " + quantity + ". Setting the slot to "+ Mathf.Max(0, quantity));
            islot.quantity = Mathf.Max(0,quantity);
            if (quantity > 0) quantity = 0; // as we've deposited the nadmiar back
            if (islot.quantity == 0) islot.item = null;
            islot.InvSquareUpdate();
        }

        ModifyQuality(num, -(initialQ - quantity));
        return quantity;   // 0 because if we're here,we'll have chaff quantity leftover
                    // No, because we may have stripped what we can and we're returning the remainder.

    }

    public int AppendItemSlotQuantity(InventorySlot islot, int quantity) // this function stuffs the inventory slot with all the items it can handle and returns what's left.
    {                                                                    // It needs a weird name to represent the witchcraft it does
        if (islot.item == null) return -1;
        int max = islot.item.maxPerSlot - islot.quantity;
        islot.quantity += Mathf.Min(quantity, max);
        if (quantity > max) { return quantity - max; }
        return 0;
    }

    public int GetItenSlotQuantity(InventorySlot islot) // this function is a bit silly...
    {
        return islot.quantity;
    }

    public InventorySlot FindInventorySlotForItem(int number) // Item.number
    {
        foreach (InventorySlot islot in items)
        {
            if (islot.item == null) { return islot; } // else { Debug.Log("found not null slot "+islot.item.name); }
            if (islot.item.number == number && islot.quantity < islot.item.maxPerSlot) return islot;
        }
        // Debug.Log("No Free Slots in "+ transform.name+"! From among "+ items.Count);
        return null;
    }

    public int GetSlotSpaceForItem(int number)
    {
        InventorySlot islot = FindInventorySlotForItem(number);
        if (islot == null)
        {
            return 0;
        } else
        {
            if (islot.item ==  null)  return GameManager.Instance.GetItemByNumber(number).maxPerSlot;
            if (islot.item != null) return GameManager.Instance.GetItemByNumber(number).maxPerSlot - islot.quantity;
        }
        return 0; // catch all just in case? Not like this can ever happen, right?

    }

    public InventorySlot GetInventorySlotWIthFewestOfItem(int number)
    {
        InventorySlot result = null;
        foreach (InventorySlot islot in items)
        {   
            if (islot.item != null) if (islot.item.number == number)
            {
                if (result == null)
                {
                    result = islot;
                } else
                {
                    if (result.quantity >= islot.quantity) result = islot; // >= so we pick from the endiest end
                }
            }
        }
        return result; // we basically have none of that item, then null..

    }

    /*
     * obsolete version:
    public void SetItemQuantity(int num, int quantity) 
    {
        Debug.Log("giving " + quantity + " of " + num);
        int ind = FindItemsIndex(num);
        if (ind >= 0 && (!IsSlotFull(items[ind])))
        {
            items[ind].quantity = quantity;
        } else
        {
            items.Add(new InventorySlot(GameManager.Instance.items[GameManager.Instance.FindItemsIndex(num)], quantity, 1));
        }
        if (num == 1) GUIHandler.Instance.SetUpperTextGold(quantity);
    }
    */

    public int GetItemQuantity(int num)
    {
        int result = 0; // by default you have nothing
        foreach (InventorySlot islot in items)
        {
            if (islot.item != null) if (islot.item.number == num) result += islot.quantity;
        }
        return result;
    }



    public void AddOneOfItem(int i)
    {
        AddItem(i, 1);
    }
    public void SubtractOneOfItem(int i)
    {
        AddItem(i, -1);
    }

    private bool IsSlotFull(InventorySlot islot)
    {
        if (islot.item == null) return false;
        if (islot.quantity < islot.item.maxPerSlot) return false;
        return true;
    }




    public void ListSlots() // this is a debug only method to spit out the stuff
    {
        List<int> existingitems = new List<int>();
        string output = "";
        foreach (InventorySlot islot in items)
        {
            if (IsSlotFull(islot)) output += "full ";
            if (islot.item == null) { output += " --------------"+ "\r\n"; }
            else
            {
                output += islot.item.itemName + " : " + islot.quantity + "  max: " + islot.item.maxPerSlot + "\r\n";
                existingitems.Add(islot.item.number);
            }

        }
        output += "\r\n";

        var existingunique = new HashSet<int>(existingitems); // Distinct!
        foreach (int et in existingunique)
        {
            
            output += et + " : " + GetItemQuantity(et) + "\r\n";
        }
        Debug.Log(output);

        if (items.Count == 0) Debug.Log(transform.name + " has no slots! *************");
    }

    public void PurgeInvslots() // Port Needs You
    {
        foreach (InventorySlot islot in items)
        {
            islot.item = null;
            islot.quantity = 0;
        }
    }

    // ----------------------- saving -----------------------------------

    public object CaptureState()
    {
        // we need:
        // int itemNum, int quantity, float quality
        List<invslotSave> islotSave = new List<invslotSave>();

        foreach (InventorySlot islot in items)
        {

            invslotSave _islotSaveTempSlot = new invslotSave();
            if (islot.item == null)
            {
                _islotSaveTempSlot.itemNumber = 0;
                _islotSaveTempSlot.itemQuantity = 0;
                _islotSaveTempSlot.quality = 0f;
                islotSave.Add(_islotSaveTempSlot); }
            else
            {
                _islotSaveTempSlot.itemNumber = islot.item.number;
                _islotSaveTempSlot.itemQuantity = islot.quantity;
                _islotSaveTempSlot.quality = islot.condition;
                islotSave.Add(_islotSaveTempSlot);
            }

        }
        return islotSave;
    }

    public void RestoreState(object state)
    {
        PurgeInvslots();
        List<invslotSave> islotSave = (List<invslotSave>)state;

        foreach (InventorySlot islot in items)
        {

            if (islotSave[0].itemNumber != 0)
            {
                islot.item = GameManager.Instance.GetItemByNumber(islotSave[0].itemNumber);

                islot.quantity = islotSave[0].itemQuantity;
                islot.condition = islotSave[0].quality;
            }
            else
            {
                islot.item = null;
                islot.condition = 0;
                islot.quantity = 0;
            }
            islotSave.RemoveAt(0);
            islot.InvSquareUpdate();
        }

    }
}
