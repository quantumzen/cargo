using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryPanel2 : MonoBehaviour
{
    // this is ver 2 of inv panel. 
    // here I'm taking a stab at squares with items in them.
    // it won't be pretty. I may wind up turning inv items into scriptable objects so they can contain sprites

    /* Algo
     * get how many cargo slots
     * divide into a neat number - we can make a convention that cargo space is divisible by 4
     *        or by certain number - will balance it later
     * spawn a grid in the gridParent. It has to be a grid. it will be easier than messing with layout things. It will also make it easier later. I surely hope.
     * 
     * square has:  inv item (or -1 or 0 or whatever for empty)
     *              quantity of the inv item
     *              
     * square does: onclick
     *              ondrop
     *              square sends these events to this invPanel2 thing.
     * 
     * Additional buttons need to be used and captured. Minecraft inventory standards:
     *              Shift moves an entire stack instead of one item.
     *              
     * This one should manage clicked item. Make the sprite appear under the pointer (with the number) so it can be dragged somewhere.
     * 
     */

    [SerializeField] Transform gridParent;
    [SerializeField] GameObject invSquarePrefab;
    float margin = 5f; // spacing between cells
    private List<InvSquareBeh> InvSquares = new List<InvSquareBeh>();
    Vessel.vesselData vessel;
    Inventory inv;

    private void Start()
    {
        vessel = GameManager.Instance.GetComponent<Vessel>().GetCurrentVesselData();
        inv = PlayerMovement.Instance.GetComponent<Inventory>();
        //Invoke("SpawnInvSquares", 0.5f);
        SpawnInvSquares();
    }





    // TODO  add price modifiers?

    private void SpawnInvSquares()
    {
        int column, row;

        column = 5; // purely arbitrary, and quite evil.
        row = vessel.cargoSlots / column;
        


        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < column; j++)
            {

                GameObject g = Instantiate(invSquarePrefab, gridParent);
                g.transform.localPosition = new Vector3(j * (100 + margin)+75, -i * (100 + margin) -75, 0);
                InvSquares.Add(g.transform.GetComponent<InvSquareBeh>());
                g.transform.GetComponent<InvSquareBeh>().SetInventorySlot(inv.CreateInventorySlot());
                g.transform.GetComponent<InvSquareBeh>().pb = this;
                g.transform.GetComponent<InvSquareBeh>().GetInventorySlot().InvSquareUpdate(); // clear default junk
                g.transform.name = "InvSquare_" + i + "_" + j;
            }

        }

         // (g.transform.localPosition.x, g.transform.localPosition.y + 64 * i, g.transform.localPosition.z);
        
        /*
        InvSquares[i].rarity.GetComponent<Image>().sprite = GameManager.Instance.GetRaritySpriteByItemNumber(ButtonNums[i]);
        if (ButtonNums[i] > 1) InvSquares[i].itemName.text = GameManager.Instance.GetItemByNumber(ButtonNums[i]).pluralname;
        else InvSquares[i].itemName.text = GameManager.Instance.GetItemByNumber(ButtonNums[i]).name;
        InvSquares[i].num = ButtonNums[i];
        InvSquares[i].owned.text = PlayerMovement.Instance.inv.GetItemQuantity(ButtonNums[i]).ToString(); // I can't directly reference here.
                                                                                                          // in fact, there is something wrong with PlayerMovement.Instance.inv.GetItemQuantity. It's not returning what I want.
        InvSquares[i].pb = this;
        */

        //Debug.Log("Player Owns " + PlayerMovement.Instance.PrintInvCount(inv.items[i].item.number).ToString() + " of " + inv.items[i].item.name.ToString()); //inv.GetItemQuantity(inv.items[i].item.number).
        //disable sell button if not buying item
        // if (IsNotBuying(InvSquares[i].num)) InvSquares[i].sell.interactable = false;


    }

    public void RefreshInventoryDisplay() // action trigger from a dialogue wants to use it.
    {
        foreach (InvSquareBeh sh in InvSquares)
        {

            sh.GetInventorySlot().InvSquareUpdate();

        }
    }


}
