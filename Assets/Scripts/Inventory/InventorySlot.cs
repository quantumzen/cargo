using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySlot 
{
    public Item item;
    public int quantity;
    public float condition;
    public InvSquareBeh invSquare;
    // public int slot;

    public InventorySlot(Item newItem, int newQuantity, float newCondition)
    {
        item = newItem;
        quantity = newQuantity;
        condition = newCondition;
        invSquare = null;
    }

    public void InvSquareUpdate()
    {
        if (invSquare != null)
        {
            if (item != null)
            {
                invSquare.SetIcon(item.sprite);
                invSquare.SetQuantity(quantity);
            } else
            {
                invSquare.SetIcon(null);
                invSquare.SetQuantity(0);
            }
        } 
    }

}
