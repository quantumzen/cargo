using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryTooltipSpawner : ToolTipSpawner
{
        public override bool CanCreateTooltip()
{
        if (GetComponent<InvSquareBeh>().GetInventorySlot().item != null) return true;
        else return false;
}

public override void UpdateTooltip(GameObject tooltip)
{
        if (GetComponent<InvSquareBeh>().GetInventorySlot().item != null)
        {
            Item i = GetComponent<InvSquareBeh>().GetInventorySlot().item;
            tooltip.GetComponent<InventoryTooltipUI>().Setup(i);
        }
}



}
