
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using RPG.Quests;
using TMPro;
using UnityEngine.UI;

public class InventoryTooltipUI : MonoBehaviour
{

    [SerializeField] TextMeshProUGUI itemName;
    [SerializeField] TextMeshProUGUI rarityDescription;
    [SerializeField] Image raritySprite;
    [SerializeField] TextMeshProUGUI additionalDetails;
    [SerializeField] TextMeshProUGUI details;

    private string normalFont;
    string originator;


    public void Setup(Item i)
    {
        
        normalFont = "<color=#" + ColorUtility.ToHtmlStringRGBA(GameManager.Instance.normalText) + ">";
        itemName.text = normalFont + i.itemName;
        raritySprite.sprite = GameManager.Instance.raritySprite[i.rarity];

        string _rariteyDesc;

        switch (i.rarity)
        {
            case 0:
                _rariteyDesc = "very, very common";
                break;
            case 1:
                _rariteyDesc = "common";
                break;
            case 2:
                _rariteyDesc = "uncommon";
                break;
            case 3:
                _rariteyDesc = "rather rare";
                break;
            case 4:
                _rariteyDesc = "rare";
                break;
            case 5:
                _rariteyDesc = "legendary";
                break;
            case 6:
                _rariteyDesc = "epic";
                break;
            case 7:
                _rariteyDesc = "impossible";
                break;
            default :
                _rariteyDesc = "no way it's butter!";
                break;

        }
        rarityDescription.text = _rariteyDesc;
        details.text = i.description;

        additionalDetails.text = "";
        if (GameManager.Instance.GetComponent<PlayerCharacter>().GetSkillValue(3) > 10) additionalDetails.text += "Average price: "+i.price+"<br>";


    }





}
