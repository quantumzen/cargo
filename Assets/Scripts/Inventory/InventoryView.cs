using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryView : MonoBehaviour
{

    [SerializeField] Transform shopButtonsPanel;
    [SerializeField] GameObject shopButtonPrefab;
    private List<InvLineBeh> shopButtons = new List<InvLineBeh>();
    private List<int> ButtonNums = new List<int>();
    


    public void PrepareButtonTable()
    {
        ButtonNums.Clear();
        ClearShopButtons();
        foreach (Item _itm in GameManager.Instance.items)
        {
            if (PlayerMovement.Instance.inv.GetItemQuantity(_itm.number) > 0) ButtonNums.Add(_itm.number);
        }
        SpawnShopButtons();
    }


    // TODO  add price modifiers?

    private void SpawnShopButtons()
    {
        int buttonCount = 0; //legacy and laziness
        for (int i = 0; i < ButtonNums.Count; i++)
        {
            GameObject g = Instantiate(shopButtonPrefab, shopButtonsPanel);
            g.transform.localPosition = new Vector3(400 + 32, -64 * buttonCount - 80, 0); // (g.transform.localPosition.x, g.transform.localPosition.y + 64 * i, g.transform.localPosition.z);
            shopButtons.Add(g.transform.GetComponent<InvLineBeh>());
            shopButtons[i].rarity.GetComponent<Image>().sprite = GameManager.Instance.GetRaritySpriteByItemNumber(ButtonNums[i]);
            if (ButtonNums[i] > 1) shopButtons[i].itemName.text = GameManager.Instance.GetItemByNumber(ButtonNums[i]).pluralname;
            else shopButtons[i].itemName.text = GameManager.Instance.GetItemByNumber(ButtonNums[i]).itemName;
            shopButtons[i].num = ButtonNums[i];
            shopButtons[i].owned.text = PlayerMovement.Instance.inv.GetItemQuantity(ButtonNums[i]).ToString(); // I can't directly reference here.
                                                                                                               // in fact, there is something wrong with PlayerMovement.Instance.inv.GetItemQuantity. It's not returning what I want.
            shopButtons[i].pb = this;

            //Debug.Log("Player Owns " + PlayerMovement.Instance.PrintInvCount(inv.items[i].item.number).ToString() + " of " + inv.items[i].item.name.ToString()); //inv.GetItemQuantity(inv.items[i].item.number).
            //disable sell button if not buying item
            // if (IsNotBuying(shopButtons[i].num)) shopButtons[i].sell.interactable = false;
            buttonCount++;
            //Debug.Log("button count: " + buttonCount);
        }
    }
     

    public void RefreshInventoryDisplay() // action trigger from a dialogue wants to use it.
    {
        foreach (InvLineBeh sh in shopButtons)
        {

            sh.owned.text = PlayerMovement.Instance.inv.GetItemQuantity(sh.num).ToString(); // I can't directly reference here.
            // if (IsNotBuying(sh.num)) sh.sell.interactable = false;

        }
    }

    private void ClearShopButtons()
    {
        foreach (InvLineBeh sh in shopButtons)
        {
            Destroy(sh.gameObject);
        }
        shopButtons.Clear();
    }

}
