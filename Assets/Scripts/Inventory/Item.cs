using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Item", menuName = "Item", order = 0)]
public class Item : ScriptableObject
{
    public string itemName;
    public string pluralname;
    public int number;
    public int price;
    public int rarity;
    public int maxPerSlot;
    public Sprite sprite;
    public string description;

    public Item(string newName, string newPlural, int newNumber, int newPrice, int newRarity, int newmaxPerSlot)
    {
        itemName = newName;
        pluralname = newPlural;
        number = newNumber;
        price = newPrice;
        rarity = newRarity;
        maxPerSlot = newmaxPerSlot;

    }
}
