using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogLine : MonoBehaviour
{

    public Image eventType;
    public TMPro.TextMeshProUGUI date;
    public TMPro.TextMeshProUGUI eventNarrative;
}
