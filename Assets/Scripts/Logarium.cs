using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logarium : MonoBehaviour
{
    [SerializeField] GameObject logLinePrefab;
    public enum LogType { uncategorised, discovery, questevent, questfail, fight, knowledge, loot }
    [SerializeField] Sprite[] logTypeSprites = new Sprite[7];
    [SerializeField] Transform logLineParent;
    [HideInInspector] public static Logarium Instance { get; private set; }
    private List<LogLineData> logLines = new List<LogLineData>();
    [SerializeField] PanelBeh logAlert;
    [SerializeField] LogLine logAlertLine;
    float logDisplayTime;


    public struct LogLineData
    {
        public LogType type;
        public int date;
        public string desc;
    }
    void Awake()
    {
        if (Instance == null) { Instance = this; }

        logAlertLine.date.color = GameManager.Instance.buttonText;
        logAlertLine.eventNarrative.color = GameManager.Instance.narrativeText;
    }

    public void LogEvent(LogLineData ll)
    {
        GameObject logLine = Instantiate(logLinePrefab, logLineParent);
        logLine.name = logLines.Count.ToString();
        LogLine lineScr = logLine.GetComponent<LogLine>();
        lineScr.eventType.sprite = logTypeSprites[(int)ll.type];
        lineScr.date.text = GameManager.Instance.GetComponent<Clock>().IntDateToText(ll.date);
        lineScr.eventNarrative.text = ll.desc;
        logLines.Add(ll);
        Debug.Log("Logged event " + ll.desc + " at " + lineScr.date.text);
        DisplayEvent(ll);
    }

    public void DisplayEvent(LogLineData ll) // without logging.
    {

        logAlertLine.eventType.sprite = logTypeSprites[(int)ll.type];
        logAlertLine.eventNarrative.text = ll.desc;
        logAlertLine.date.text = GameManager.Instance.GetComponent<Clock>().IntDateToText(ll.date);
        logDisplayTime = logAlertLine.eventNarrative.text.Length * 0.09f;
        logAlert.Deploy();
    }


    private void Update()
    {
        if (logDisplayTime > 0) {
            logDisplayTime -= Time.deltaTime;
            if (logDisplayTime < 0) logAlert.Hide();
        }
    }



}
