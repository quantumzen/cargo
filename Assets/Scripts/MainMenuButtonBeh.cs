using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.EventSystems;// Required when using Event data.


[RequireComponent(typeof(AudioSource))]
public class MainMenuButtonBeh : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] Sprite sprOn;
    [SerializeField] Sprite sprOff;
    [SerializeField] Sound sndClick;

    AudioSource audioData;
    void Start()
    {
        audioData = GetComponent<AudioSource>();
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (GetComponent<Button>().interactable == false) return;
        transform.GetComponent<Image>().sprite = sprOn;
        audioData.volume = 0.5f;
        audioData.Play(0);

    }

    public void OnPointerExit(PointerEventData eventData) //
    {

        transform.GetComponent<Image>().sprite = sprOff;

        audioData.volume = 0.1f;
        audioData.Play(0);
    }


}
