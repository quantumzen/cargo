using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class MapMark : MonoBehaviour
{
    public bool isVisible = false;
    Transform player;
    SpriteRenderer sr;
    float detectionAngle = 25f;


    // it will sign up for flare events

    // Start is called before the first frame update
    void Start()
    {
        player = PlayerMovement.Instance.transform;
        sr = GetComponent<SpriteRenderer>(); // I'm hoping it's on the same object! Will I remebert it? I should require it. Now I have.
        if (!isVisible) sr.color = new Color(1, 1, 1, 0);
        StartCoroutine(CheckLightCone());
    }

    private IEnumerator CheckLightCone()
    {
        yield return new WaitForSeconds(1f); // for some reason it flipped isVisible at the start

        while (true && !isVisible)
        {
            float angle = Vector3.Angle(transform.position - player.position, player.forward);
            float dist = Vector3.Distance(transform.position, player.position);
            // Debug.Log(angle + "   " + dist * PlayerMovement.Instance.GetDustDensity());
            if (angle < detectionAngle && dist * PlayerMovement.Instance.GetDustDensity() < 30)
            {
                sr.color = new Color(1, 1, 1, 1);
                isVisible = true;
            }
            yield return new WaitForSeconds(0.3f); // I'm tryong not to waste this every frame
        }

        yield return null;
    }

}
