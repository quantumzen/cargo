using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCWalkController : MonoBehaviour
{

    [SerializeField] float speed = 0f;
    [SerializeField] float runSpeedMult = 2f;

    [SerializeField]
    Animator animator;

    // [SerializeField] GameManager gm;


    Camera _cam;
    Rigidbody _rb;

    Vector3 movement;
    private bool isRunning;
    private bool dead;

    private void Awake()
    {
        _cam = Camera.main;
        _rb = GetComponent<Rigidbody>();
        animator.speed = runSpeedMult;
    }
    // Start is called before the first frame update
 

    private void FixedUpdate()
    {
        // _rb.velocity = Vector3.forward * Time.deltaTime * speed * runSpeedMult + new Vector3(0f, _rb.velocity.y, 0f);
        _rb.velocity = transform.forward * Time.deltaTime * speed; // + new Vector3(0f, _rb.velocity.y, 0f);
    }

    private void Update()
    {
        // animator.SetFloat("Speed", movement.normalized.magnitude * (isRunning ? 1f : .5f));
        animator.SetFloat("Speed", speed);

 
    }
     
    private void LookToward(Vector3 direction)
    {
        transform.rotation = Quaternion.Euler(0, -Mathf.Atan2(direction.z, direction.x) * Mathf.Rad2Deg, 0f);
    }
     


    public void SetSpeed(float _s)
    {
        speed = _s;
    }
    public void SetDirection(Vector3 v)
    {
        //movement = new Vector3(v.x, transform.position.y, v.z); // I don't want it to climb
        //LookToward(movement);
        // transform.Rotate(new Vector3(v.x, transform.position.y, v.z)); LOL

        //Quaternion q = new Quaternion(0f, Vector3.Angle(transform.forward, v), 0f, 90);
        //transform.rotation = q;
        transform.LookAt(PlayerMovement.Instance.transform);
         
    }


}
