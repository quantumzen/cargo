using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevTV.Saving;
using System;

public class PlayerCharacter : MonoBehaviour, ISaveable
{
    [SerializeField] int[] skill = new int[12];
    [SerializeField] string[] skillName = new string[12];

    [SerializeField] int[] navigatorSkill = new int[12];
    [SerializeField] int[] aurerSkill = new int[12];
    [SerializeField] Sprite[] buffSprite;
    [SerializeField] List<Buff> buffs = new List<Buff>();
    [SerializeField] GameObject buffSquare;
    [SerializeField] Transform buffParent;
    [SerializeField] string captainName;
    int[] zeroSkill = new int[12] { 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0 };

    int[] sharmaSkill = new int[12] { 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0 };
    // [SerializeField] List<string, int> skill = new List<string, int>();
    public enum BuffType { heat, speed, damage };
    private Humans humans;

    [System.Serializable]
    public class Buff // I had to allow mutability by classing it.
    {
        public BuffType buffType;
        public string description;
        public float timeLeft;
        public float strength;
        public float totalTime;
        public BuffBeh beh;
        public Buff (BuffType _b, string _desc, float _time, float _str)
        {
            buffType = _b;
            description = _desc;
            timeLeft = _time;
            strength = _str;
            totalTime = _time;
        }
    }

    [HideInInspector] public static PlayerCharacter Instance { get; private set; }

  

    void Awake()
    {
        if (Instance == null) { Instance = this; }
        Humans.OnCrewChange += CrewChange;
        GameManager.OnStageChange += WipePlayer;
        humans = GetComponent<Humans>();
    }

    private void OnDestroy()
    {
        Humans.OnCrewChange -= CrewChange;
        GameManager.OnStageChange -= WipePlayer;
    }


    public int GetSkillValue(int index)
    {
        return skill[index] + navigatorSkill[index] + aurerSkill[index];
    }

    public int GetNavigatorValue(int index)
    {
        return navigatorSkill[index];
    }
    public int GetAurerValue(int index)
    {
        return aurerSkill[index];
    }
    public int GetCrewmateSkillValue(string name, int index)
    {
        if (name == "Sharma") return sharmaSkill[index];
        if (name == "Zero") return zeroSkill[index];
        return 0;
    }

    public int GetCaptainOnlySkillValue(int index)
    {
        return skill[index];
    }
    public void SetSkillValue(int index, int val)
    {
        skill[index] = val;
    }
    public string GetSkillName(int index)
    {
        return skillName[index];
    }

    private void CrewChange(String name)
    {
        // doesn't need to add captian. if (name == "You")

        if (humans.IsHumanAboard(name))         // if present - ad skills
        {
            if (humans.GetRole(name) == "navigator")
            {
                switch (name)
                {
                    case "Zero":
                        navigatorSkill = zeroSkill;
                        Debug.Log("Added Zero's skills");
                        break;

                }
            }

            if (humans.GetRole(name) == "aurer")
            {
                switch (name)
                {
                    case "Sharma":
                        aurerSkill = sharmaSkill;

                        Debug.Log("Added Sharma's skills");
                        break;

                }

            }

        } else
        {
            // else : remove.

        }

    }

    public string GetCaptainName() { return captainName;  }
    public void SetCaptainName(string n) { captainName = n; }

    public void AddBuff(Buff b)
    {
        GameObject _buffSquare = Instantiate(buffSquare, buffParent);
        b.beh = _buffSquare.GetComponent<BuffBeh>();
        b.beh.buffSprite.sprite = buffSprite[(int)b.buffType];
        buffs.Add(b);
        Debug.Log("Added buff " + b.description);
        b.beh.GetComponent<PanelBeh>().isDeployed = true;
    }

    private void WipePlayer(GameManager.Stage sOld, GameManager.Stage sNew)
    {
        if (sOld != GameManager.Stage.menu && sNew == GameManager.Stage.menu)
        {
            SetSkillValue(1, 10);
            SetSkillValue(2, 10);
            SetSkillValue(3, 10);
            SetSkillValue(4, 10);
            SetSkillValue(5, 10);
            SetSkillValue(6, 10);
            SetCaptainName("");
        }
    }

    // -------------------------------- here ve manage and cycle thorgh active buffs --------------------------------
    void Update()
    {
        float _t = Time.deltaTime;
        string guib = "";
        if ((GameManager.Instance.stage == GameManager.Stage.game) && (!GameManager.Instance.isDocked) && buffs.Count > 0) // maybe more conditions here? see PlrMovement
        {
            for (int i=0; i < buffs.Count; i++)
            {
                switch (buffs[i].buffType)
                {
                    case BuffType.damage:
                        break;
                    case BuffType.heat:
                        PlayerMovement.Instance.SetHeatbuff(buffs[i].strength);
                        break;
                    case BuffType.speed:
                        break;
                }
                buffs[i].timeLeft -= _t;
                buffs[i].beh.buffTimer.fillAmount = buffs[i].timeLeft / buffs[i].totalTime;
                if (buffs[i].timeLeft < 0) {
                    buffs[i].beh.GetComponent<PanelBeh>().Hide();
                    buffs[i].beh.DelayDestruction();
                    buffs.RemoveAt(i);  
                } // else { guib += buffs[i].description + "  left: " + buffs[i].timeLeft.ToString();  }
            }
        }
        GUIHandler.Instance.SetUpperTextBuffs(guib);
    }


    #region Save

    [System.Serializable]
    public class BuffSave // I had to allow mutability by classing it.
    {
        public BuffType buffType;
        public string description;
        public float timeLeft;
        public float strength;
        public float totalTime;
        public BuffSave(BuffType _b, string _desc, float _timeLeft, float _totalTime, float _str)
        {
            buffType = _b;
            description = _desc;
            timeLeft = _timeLeft;
            totalTime = _totalTime;
            strength = _str;
        }
    }


    public object CaptureState()
    {
        Dictionary<string, object> savedata = new Dictionary<string, object>();
        savedata["skill"] = skill;  // int[12];
        savedata["captainName"] = captainName;

        List<BuffSave> bfsList = new List<BuffSave>();
        foreach (Buff bf in buffs)
        {
            BuffSave bfs = new BuffSave(bf.buffType, bf.description, bf.timeLeft, bf.totalTime, bf.strength);
            bfsList.Add(bfs);
        }
        savedata["buffs"] = bfsList;

        return savedata;
        
    }

    public void RestoreState(object state)
    {
        ClearBuffs();

        Dictionary<string, object> savedata = (Dictionary<string, object>)state;
        skill = (int[])savedata["skill"];
        SetCaptainName((string)savedata["captainName"]);
        List<BuffSave>bfsList = (List<BuffSave>)savedata["buffs"];
        //Debug.Log("Buufs loading: " + bfsList.Count);
        foreach (BuffSave bfs in bfsList)
        {
            //Debug.Log("Buuf: " + bfs.description);
            Buff bf = new Buff(bfs.buffType, bfs.description, bfs.totalTime, bfs.strength);
            bf.timeLeft = bfs.timeLeft;
            AddBuff(bf);
        }

        humans.InvokeUpdateOfStats("You"); // must. Loaded captain stats must be reflectsd

    }


    public void ClearBuffs()
    {
        for (int i = 0; i < buffs.Count; i++)
        {
            buffs[i].timeLeft = 0;
            buffs[i].beh.GetComponent<PanelBeh>().Hide();
            buffs[i].beh.DelayDestruction();
            buffs.RemoveAt(i);
        }
    }
    #endregion
}
