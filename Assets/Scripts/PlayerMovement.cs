using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using DG.Tweening;
using System;
using GameDevTV.Saving;

public class PlayerMovement : MonoBehaviour, GameDevTV.Saving.ISaveable
{

    /*
     * steering
     * mouse is look. circlepoints to the center of the look
     * turret is cross. follows the look slowly. Elevation is followed, too.
     * wasd is steering. WS acceleration/decelleration, AD - rotate vehicle
    */

    [HideInInspector] public static PlayerMovement Instance { get; private set; }

    private Vector3 rawSteering;
    public Vector2 _look;
    [SerializeField] Rigidbody rb;
    [SerializeField] private LayerMask floorLayerMask = new LayerMask();
    [SerializeField] private float maneuverability;
    [SerializeField] private float acceleration;
    [SerializeField] PanelBeh interactor; // handled on escpressed (undock)
    [SerializeField] PanelBeh mapBeh; // on M pressed, bigMap
    [SerializeField] private Transform turret;
    [SerializeField] private Light l1;
    [SerializeField] private Light l2;
    [SerializeField] private Light l3Reverse;
    [SerializeField] public Transform followTransform; // camera follow // it will be also used by the turret to aim??
    public float lookSensitivity = 30f; // default 30 is for my trackball.
    [SerializeField] private AudioSource reversingBeep;
    [SerializeField] private AudioSource engineSound;
    [SerializeField] private AudioClip[] engineSounds; //  = new AudioClip[7]

    private float lintensity;
    public bool isLights;
    private bool isLightsDimmed;
    private float illuminationIntensity;
    private bool isIlluminated;
    [SerializeField] List<Renderer> illuminators = new List<Renderer>();
    public bool isBrake;
    private bool isReversing;
    private float reversingLightIntensityTarget = 0;
    private bool isTriggerPressed;
    private int gear;
    private int gearNeutral;
    private float[] speeds;
    public float fuel;
    [SerializeField] private float heat;
    private float baseHeat = 10f;
    private float heatBuff = 0f;
    //public Stack<Transform> escapists = new Stack<Transform>();

    [SerializeField] Aura2API.AuraVolume globalDust;
    private float distanceDustDivider = 800;
    private float baseDustVolume = 1.7f;
    private float distanceToHub = 0;
    private float bearingToHub = 0;
    private float dustDensity = 0;

    // private GUIHandler gh;
    public Inventory inv;
    private Transform interactible;
    private Transform lowerGUITextOwner;
    private TurretBehaviour turBeh;
    private Sequence dotseq;


    // events --------------------------------------------------
    public static event Action<int> OnGearChange;
    public static event Action OnEscPressed;                // the way this works is: something that wants to hijack Esc's attention sails into view, then it registers itself in Stack<Transform> escapists
                                                            // It will need to remmeber to unregister after being Esc'd or closed in some other way.
                                                            // when the event is called, all Esc-ables check it transform == Stack<Transform> escapists.Peek(). If it does, this means you're on top and Esc is to close you.

    [SerializeField] Transform centerOfMass;
    private void Awake() // must be awake to inputs subscrive
    {
        GearChange(2);// the gear better be defined as neutral at the start!
        isTriggerPressed = false;
        isLights = true;
        isIlluminated = true;
        isLightsDimmed = false;
        isReversing = true; // because the light is on by default
        illuminationIntensity = 1f; //full alpha emmisive whatnot.
        fuel = 1f; // full tank
        // acceleration = 1f;
        if (Instance == null) { Instance = this; }
        // gh = transform.GetComponent<GUIHandler>();
        inv = transform.GetComponent<Inventory>();
        turBeh = turret.GetComponent<TurretBehaviour>();

        OnEscPressed += EscPressedBehaviour;
        GameManager.OnTestScenarioChange += TestScenarioHandler;
        Debug.Log("Awake Player Complete ");

        centerOfMass.position = GetComponent<Rigidbody>().centerOfMass;
    }

    private void OnDestroy()
    {

        OnEscPressed -= EscPressedBehaviour;
        GameManager.OnTestScenarioChange -= TestScenarioHandler;
    }


    // an example of invoking the event


    public void GearChange(int g)
    {
        gear = g;
        // Debug.Log("Gear to " + g);
        engineSound.clip = (engineSounds[g]);
        if (GameManager.Instance != null) if (!engineSound.isPlaying && GameManager.Instance.stage == GameManager.Stage.game) engineSound.Play(); // && GameManager.Instance.stage == GameManager.Stage.game
        Reversing((g < 2));
        OnGearChange?.Invoke(g);
    }



    public void EscPressed(InputAction.CallbackContext val) // Escape
    {
        if (val.started)
        {
            // Transform _t = escapists.Peek();
            //OnEscPressed?.Invoke(_t);     // I need to protect against empty stack with some default option
                                            // here we call EscPressedBehaviour
            OnEscPressed?.Invoke();
        }
    }
    public void EscButtonPressedOnPanel()
    {
        OnEscPressed?.Invoke();
    }
    

    void Start()
    {

        gearNeutral = gear;
        interactible = null;
        // lintensity = l1.intensity;
        speeds = new float[] { -0.5f, -0.3f, 0f, 0.2f, 0.5f, 0.9f, 1f };
        Debug.Log("Start Player Complete ");
        StartCoroutine(SetDustDependingOnDistance());
    }

    private void FixedUpdate() // just for the rigidbody interaction. Also remember to interpolate rb in the inspector.
    {
        if ((GameManager.Instance.stage == GameManager.Stage.game) && (!GameManager.Instance.isDocked) && (!GameManager.Instance.isDocking) && (GameManager.Instance.isMovementEnabled)){            ActualMovement();        }
        if (isBrake) { rb.drag = 20; } else { rb.drag = 3; }
        
    }
    void Update()
    {


        // GameManager.Instance.UpdateDebugConsole(GameManager.Instance.GetComponent<Clock>().GetTextDate());

        if ((GameManager.Instance.stage == GameManager.Stage.game) && (!GameManager.Instance.isDocked) && (!GameManager.Instance.isDocking) && (GameManager.Instance.isMovementEnabled))
        {
            RotateThirdPersonCamera();
            CalculateHeat();
            ReversingLightDimmer(); // stgeered by reversingLightIntensityTarget float
        }

        // for debug only!

        Vector2 mousepos = UnityEngine.InputSystem.Mouse.current.position.ReadValue();
    }


    void ActualMovement()
    {

        
        fuel -= Mathf.Abs(speeds[gear]) * acceleration * Time.deltaTime * 0.00002f;
        if (fuel <= 0) { Refuel(); return; }
        if (!isIlluminated) ToggleAllIllumination(true);
        rb.AddTorque(transform.up * rawSteering.x * maneuverability);
        //transform.position += rawSteering;
        rb.AddForce(transform.forward * speeds[gear] * acceleration * Time.deltaTime); // I've rplaced rawsteering with gear
        GUIHandler.Instance.SetUpperTextPos(new Vector2(transform.position.x, transform.position.z));
        GUIHandler.Instance.SetUpperTextBearing(Mathf.RoundToInt(transform.rotation.eulerAngles.y));
        // turret rotation
        //turBeh.targetRotationPoint = GetMouseFloorCoords();
        // turBeh.targetRotationPoint = followTransform.transform.position;
        // FIRING!
        if (isTriggerPressed) turBeh.FireProjectile();




    }

    void RotateThirdPersonCamera()
    {
        if (GameManager.Instance.stage == GameManager.Stage.game)
        {
            Quaternion q = followTransform.transform.rotation * Quaternion.AngleAxis(_look.x, Vector3.up); // 
            followTransform.transform.rotation = Quaternion.Lerp(followTransform.transform.rotation, q, Time.deltaTime * lookSensitivity);
        }
    }


    private void Refuel() // load another fuel cartridge
    {
        if (inv.GetItemQuantity(2) > 0)
        {
            inv.AddItem(2, -1);
            fuel = 1f;
        } else
        {
            // dead in the water. or dust. lights out.
            if (isLights) ToggleLights(false);
            if (isIlluminated) ToggleAllIllumination(false);
        }
    }

    public void OnSteer(InputAction.CallbackContext val)
    {
        Vector2 steeringInput = val.ReadValue<Vector2>(); // here we capture the steering
        float t = Time.deltaTime;
        rawSteering = new Vector3(steeringInput.x*t, 0, steeringInput.y*t);
    }

    public void OnGearSwitchUp(InputAction.CallbackContext val)
    {
        // rawSteering.z
        if (val.started && (GameManager.Instance.stage == GameManager.Stage.game) && (!GameManager.Instance.isDocked) && (!GameManager.Instance.isDocking) && (GameManager.Instance.isMovementEnabled))
        {
            if (gear < speeds.Length - 1) GearChange(gear+1);
            
        }
    }
    public void OnGearSwitchDown(InputAction.CallbackContext val)
    {
        // rawSteering.z
        if (val.started && (GameManager.Instance.stage == GameManager.Stage.game) && (!GameManager.Instance.isDocked) && (!GameManager.Instance.isDocking) && (GameManager.Instance.isMovementEnabled))
        {
            if (gear > 0) GearChange(gear - 1);
        }
    }
    public int GetGear() { return gear;  }

    public void OnFirePressed(InputAction.CallbackContext val)
    {
        if ((GameManager.Instance.stage == GameManager.Stage.game) && (!GameManager.Instance.isDocked) && (!GameManager.Instance.isDocking) && (GameManager.Instance.isMovementEnabled))
        {
            if (val.started)
            {
                isTriggerPressed = true;
            }

            if (val.canceled)
            {
                isTriggerPressed = false;
            }
        }
    }
    public void OnMapPressed(InputAction.CallbackContext val)
    {
        if ((GameManager.Instance.stage == GameManager.Stage.game) && (!GameManager.Instance.isDocked) && (!GameManager.Instance.isDocking) && (GameManager.Instance.isMovementEnabled))
        {
            if (val.started)
            {
                mapBeh.Cycle();
            }

        }
    }


    public void OnTurretToggle(InputAction.CallbackContext val)
    {
        if (val.started && (GameManager.Instance.stage == GameManager.Stage.game) && (!GameManager.Instance.isDocked) && (!GameManager.Instance.isDocking) && (GameManager.Instance.isMovementEnabled))
        {
            if (turBeh.isDeployed) // (!turretDeploy.)
            {
                // turretDeploy.SetTrigger("DeployTest");
                // turBeh.StowTurret();
                turBeh.CycleTurret();
            }
            else
            {
                // turBeh.DeployTurret();
                turBeh.CycleTurret();
            }
        }
    }

    public void OnMouseMovement(InputAction.CallbackContext val)
    {
        Vector2 mousepos = val.ReadValue<Vector2>();
        // Debug.Log(mousepos.x + " " + mousepos.y);
    }


    public void SetInteractible(Transform t) 
    {
        if ((GameManager.Instance.stage == GameManager.Stage.game))
        {
            interactible = t;
            Interactible i = interactible.GetComponent<Interactible>();
            if (i.dockable) { GUIHandler.Instance.SetLowerGUIText("Press E to dock."); lowerGUITextOwner = interactible; }
            if (i.hailable) { GUIHandler.Instance.SetLowerGUIText("Press E to interact."); lowerGUITextOwner = interactible; }
            if (i.collectable) { GUIHandler.Instance.SetLowerGUIText("Press E to collect."); lowerGUITextOwner = interactible; }
            if (i.interactionTrigger) { lowerGUITextOwner = interactible; OpenDialogue(i); i.ClearInteractionTrigger(); }
        }
    }

    public Transform GetInteractible()
    {
        return interactible;
    }

    public void ClearInteractible()
    {
        if (lowerGUITextOwner == interactible)
        {
            GUIHandler.Instance.ClearLowerGUIText();
            lowerGUITextOwner = null;
        }

        interactible = null;

        //GameManager.Instance.UpdateDebugConsole("NULL");
    }

    public void OnInteractPressed(InputAction.CallbackContext val) // default shoud be E key, but may need changind ae QE will be needed for strafing.
    {

        if (val.started && (GameManager.Instance.stage == GameManager.Stage.game)) {
            if (val.started)
            {
                if (interactible == null) return;
                Interactible i = interactible.GetComponent<Interactible>();
                if (i.dockable)
                {
                    gear = gearNeutral;
                    GearChange(gear);

                    GUIHandler.Instance.SetLowerGUIText("Docking... Press [Esc] to abort.");
                    GameManager.Instance.SetDocking(true);
                    MovePlayerToPosition(interactible.position, interactible.rotation);
                }
                else if (i.collectable)
                {
                    // coins and all that good stuff
                    // Destroy(i.gameObject); // you can't just derstroy stuff like that. Think of all the children
                    string eventString = "We have found and laid claim to:<br>";
                    int _totalGained = 0;
                    Dictionary<int, int> _d = new Dictionary<int, int>();                // I guess it's immutable, right?
                    foreach (KeyValuePair<int, int> pair in interactible.GetComponent<Loot>().loot)
                    {
                        int _left = inv.AddItem(pair.Key, pair.Value);

                        int _gained = pair.Value - _left;
                        _totalGained += _gained;
                        eventString += _gained + " " + (_gained == 1 ? GameManager.Instance.GetItemByNumber(pair.Key).itemName : GameManager.Instance.GetItemByNumber(pair.Key).pluralname) + "<br>";

                        // interactible.GetComponent<Loot>().loot.Remove(pair.Key);
                        if (_left > 0) _d.Add(pair.Key, _left); // TODO test multiple items, with spillover qnatities.
                    }
                    interactible.GetComponent<Loot>().loot.Clear();
                    if (_totalGained == 0) eventString = "No free space in our cargo hold. ";
                    if (_d.Count > 0)
                    {
                        eventString += "<br>We had to leave behind:<br>";
                        foreach (KeyValuePair<int, int> pair in _d)
                            eventString += pair.Value + " " + (pair.Value == 1 ? GameManager.Instance.GetItemByNumber(pair.Key).itemName : GameManager.Instance.GetItemByNumber(pair.Key).pluralname) + "<br>";
                    }

                    interactible.GetComponent<Loot>().loot = _d; // immutable mumblemumble
                    interactible.GetComponent<Loot>().Looted();
                    Logarium.LogLineData lld = new Logarium.LogLineData();
                    lld.type = Logarium.LogType.loot;
                    lld.date = GameManager.Instance.GetComponent<Clock>().GetIntDate();
                    // Debug.Log("Date: " + lld.date);
                    lld.desc = eventString;
                    if (_totalGained > 0) { Logarium.Instance.LogEvent(lld); } else { Logarium.Instance.DisplayEvent(lld); }
                    ClearInteractible();
                }
                else if (i.hailable)
                {
                    OpenDialogue(i);
                }
            }
        }
    }

    private void OpenDialogue(Interactible i)
    {
        // hailing frequencies open
        RPG.Dialogue.Conversant.Instance.OpenDialogue(i.NPCDialogue, i.currentNode, true, i); //TODO: Close the dialogue if plr moves away. Handle the "mustPause"
        gear = gearNeutral;
        GearChange(gear);
    }

    /*
     * this is how this context on inp[uts should be read.
    if (context.started)
        Debug.Log("Action was started");
    else if (context.performed)
        Debug.Log("Action was performed");
    else if (context.canceled)
        Debug.Log("Action was cancelled");
    */



    public void EscPressedBehaviour()
    {

        if (GameManager.Instance.isDocking)
        {
            GameManager.Instance.SetDocking(false);
            dotseq.TogglePause();
            dotseq.Kill(false);
            GUIHandler.Instance.ClearLowerGUIText();
            lowerGUITextOwner = null;
        } else if (GameManager.Instance.stage == GameManager.Stage.game)
        {
            // if you're not docking and you've pressed esc, means you want the interactor to pop up on pause.
            Invoke("InteractorDeploy", 0.1f);
            return;
        }


        if (GameManager.Instance.stage == GameManager.Stage.paused)
        {
            if (GameManager.Instance.isDocked)
            {
                // release docked vessel here ...I guess
                lowerGUITextOwner.GetComponent<portBeh>().DisableCanvas();
                interactor.Hide();
                GUIHandler.Instance.ClearLowerGUIText();
                lowerGUITextOwner = null;
                GameManager.Instance.SetDocked(false);
                ToggleLights(true);
                ToggleAllIllumination(true);
                GUIHandler.Instance.EnableMainCam();
                turBeh.TryReloadingIfEmpty();
            }
        }


    }

    private void InteractorDeploy() // I have to do this this way, so that the panel doesn't immediately hide on the same Esc event.
    {
        interactor.Deploy();
        interactor.GetComponent<TabHandler>().ButtonClicked(7);

    }

    public void OnAimPressed(InputAction.CallbackContext val)
    {

        if (val.started && (GameManager.Instance.stage == GameManager.Stage.game) && (!GameManager.Instance.isDocked) && (!GameManager.Instance.isDocking))
        {
            if (val.started)
            {
                GUIHandler.Instance.CycleAimCam();
                StartCoroutine(CycleLights());
            }
        }
    }

    public void MovePlayerToPosition(Vector3 v, Quaternion q)
    {
        dotseq = DOTween.Sequence();
        dotseq
            .Append(transform.DORotate(q.eulerAngles, 1f))
            .Append(transform.DOMove(v, 2f))
            .AppendCallback(DockingSuccessful);
        // Y(0, 1.5f).SetEase(Ease.InOutCirc).SetDelay(1f);
    }

    private void DockingSuccessful()
    {
        if (GameManager.Instance.isDocking)
        {
            GameManager.Instance.SetDocked(true);
            GameManager.Instance.SetDocking(false);
            GameManager.Instance.StageChange(GameManager.Stage.paused);
            GUIHandler.Instance.SetLowerGUIText("");
            ToggleLights(false);
            ToggleAllIllumination(false);
            if (interactible.transform.GetComponent<Interactible>().proxyPort != null)
            {
                interactible = interactible.transform.GetComponent<Interactible>().proxyPort.transform; // needed for proxy multibay docks like petrol stations
                lowerGUITextOwner = interactible; // Game Manager will insist.
            }

            Transform portCam = interactible.transform.GetComponent<portBeh>().cmspot;
            interactible.transform.GetComponent<portBeh>().PrepareButtonTable();
            Debug.Log("Docked. Portcam coords of "+ interactible.transform.name + ": " + portCam.position.z + " " + portCam.position.y + " " + portCam.position.z);
            GUIHandler.Instance.EnablePortCam(portCam);
            Interactible i = interactible.transform.GetComponent<Interactible>();
            RPG.Dialogue.Conversant.Instance.OpenDialogue(i.NPCDialogue, i.currentNode, true, i); //TODO: Close the dialogue if plr moves away. Handle the "mustPause"

        }
        else
        {
            Debug.Log("docking was aborted, but this is the finish");
        }
    }

    private void Reversing(bool v)
    {
        //if (v) Debug.Log("Attempting to reverse");
        //if (!v) Debug.Log("Attempting to UNreverse");
        //if (isReversing) Debug.Log("reverse gear was on");
        if (isReversing == v) return;
        isReversing = v;
        if (isReversing)
        {
            //Debug.Log("enabling reversing");
            // beep and rear light
            reversingBeep.Play();
            reversingLightIntensityTarget = 2f;
        }
        else
        {
            //Debug.Log("disabling reversing");
            // stop beeping
            // switch off light
            reversingBeep.Pause();
            reversingLightIntensityTarget = 0f;


        }
    }

    internal bool IsReversing()
    {
        return isReversing;
    }

    #region Light Controls

    public void SwitchLights(InputAction.CallbackContext val) // I have to do this, because toggle lights was barbarically misused
    {

        if (val.started && (GameManager.Instance.stage == GameManager.Stage.game) && (!GameManager.Instance.isDocked) )
        
        {
            ToggleLights(!isLights);
            //ToggleAllIllumination(!isLights);
        }
    }

    public void ToggleLights(bool t)
    {
        if (t)
        {
            lintensity = 2f;
            isLights = true;
            StartCoroutine(LightsDimmer());

        }
        else
        {
            lintensity = 0;
            isLights = false;
            StartCoroutine(LightsDimmer());
        }
    }

    private IEnumerator LightsDimmer()
    {
        // this really should be a coroutinwe
        // well, it is now, I hope you're happy.

        while (Mathf.Abs(lintensity - l1.intensity) > 0.01f)

        {
            float newlintensity = Mathf.Lerp(l1.intensity, lintensity, 0.05f);
            l1.intensity = newlintensity;
            l2.intensity = newlintensity;
            yield return new WaitForSeconds(0.01f);
        }

        yield return null;
    }
    private void ReversingLightDimmer()
    {
        // this really should be a coroutinwe
        // well, it is now, I hope you're happy.

        // no it shouldn't as I could start multiple contradicting instances.

        if ((l3Reverse.intensity < 0 && reversingLightIntensityTarget < 1f) || (l3Reverse.intensity > 2f && reversingLightIntensityTarget > 1f)) return; // (Mathf.Abs(reversingLightIntensityTarget - l3Reverse.intensity) > 0.01f)

        
        float newlintensity = Mathf.Lerp(l3Reverse.intensity, reversingLightIntensityTarget, 0.1f);
        l3Reverse.intensity = newlintensity;
        

        
    }

    public void SwitchAllIllumination(InputAction.CallbackContext val) // I have to do this, because toggle lights was barbarically misused
    {
        if (val.started && (GameManager.Instance.stage == GameManager.Stage.game) && (!GameManager.Instance.isDocked)) ToggleAllIllumination(!isIlluminated);
    }

    public void ToggleAllIllumination(bool t)
    {
        if (t)
        {
            illuminationIntensity = 1f;
            isIlluminated = true;
            StartCoroutine(AllIlluminationDimmer());
        }
        else
        {
            illuminationIntensity = 0;
            isIlluminated = false;
            StartCoroutine(AllIlluminationDimmer());
            Debug.Log("Illumination off!");
        }
    }


    private IEnumerator AllIlluminationDimmer()
    {
        // this really should be a coroutinwe
        // well, it is now, I hope you're happy.
        while (Mathf.Abs(illuminationIntensity - illuminators[0].material.GetFloat("_EmissionColor")) > 0.01f )
        {
            foreach (Renderer r in illuminators)
            {
                // Debug.Log(r.material.name + " is being changed from " + r.material.GetFloat("_EmissionColor") + " to " + illuminationIntensity);
                float newlintensity = Mathf.Lerp(r.material.GetFloat("_EmissionColor"), illuminationIntensity, 0.03f);
                // r.material.SetColor("_EmissionColor", new Color(r.material.GetColor("_EmissionColor").r, r.material.GetColor("_EmissionColor").g, r.material.GetColor("_EmissionColor").b, newlintensity));
                // r.material.SetColor("_EmissionColor", new Color(newlintensity, newlintensity, newlintensity, newlintensity));
                r.material.SetFloat("_EmissionColor", newlintensity);
            }
            yield return new WaitForSeconds(0.01f);
        }

        yield return null;
    }

    private IEnumerator CycleLights() // for OnAim only really
    {
        float normalScattering = 0.3f;
        float aimedScattering = 0.15f;
        float increment = 0.01f;
        float target = normalScattering;

        if (!isLightsDimmed)
        {
            target = aimedScattering;
            increment *= -1;

        }

        while ((l1.GetComponent<Aura2API.AuraLight>().overridingScattering > aimedScattering && !isLightsDimmed) || (l1.GetComponent<Aura2API.AuraLight>().overridingScattering < normalScattering && isLightsDimmed))
        {
            l1.GetComponent<Aura2API.AuraLight>().overridingScattering += increment;
            l2.GetComponent<Aura2API.AuraLight>().overridingScattering += increment;
            yield return new WaitForSeconds(0.1f);
        }
        isLightsDimmed = !isLightsDimmed;
        yield return null;
    }




    #endregion


    public void OnLook(InputAction.CallbackContext val)
    {
        _look = val.ReadValue<Vector2>();

        
    }

    private Vector3 GetMouseFloorCoords()
    {
        Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, floorLayerMask))
        {
            //Debug.Log("Clicked on " + hit.transform.name);
            //Debug.Log("Angle " + Vector3.Angle(transform.position, hit.transform.position) + "   posiotion " + (hit.point) + ", " + (hit.collider.tag) + ", " + (hit.transform.position.z - transform.position.z));
            //Debug.DrawRay(turret.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            return hit.point;
        }
        else
        {
            
            return Vector3.forward;
        }
    }

    public int PrintInvCount(int c)
    {
        Debug.Log(c);
       //  inv.SetItemQuantity(11, 10);

        return (inv.GetItemQuantity(c));

    }

    public void SetHeat(float h)    {        heat = h;    }
    public float GetHeat() { return heat; }
    private void CalculateHeat()
    {
        float h = (Mathf.Abs(speeds[gear]) * acceleration /50) + (isLights ? 3f : 0) + (turBeh.isDeployed ? 2f : 0) + (turBeh.heat * 10f) + baseHeat + heatBuff;
        if (h != GetHeat())
        {
            
            SetHeat(Mathf.Lerp(h, GetHeat(), 0.99f));
            //GUIHandler.Instance.SetUpperTextHeat(h); // no longer needed as the heatbar is handling it.
        }
        SetHeatbuff(0); // fi they want the PlayerCharacter will remind us there is buff.
    }
    public void SetHeatbuff(float _h)    {        heatBuff = _h;    }




    public void AddGold(int g)     
    {        
        inv.AddItem(1, g);
        GUIHandler.Instance.SetUpperTextGold(inv.GetGold());
    }

    private IEnumerator SetDustDependingOnDistance()
    {
        while (true)
        {

            distanceToHub = Vector3.Distance(transform.position, Vector3.zero);
            dustDensity = distanceToHub / distanceDustDivider + baseDustVolume;
            globalDust.densityInjection.strength = dustDensity;
            yield return new WaitForSeconds(3f);

        }
        //yield return null;

    }

    public float GetBearingToHub()
    {
        bearingToHub = GetBearingToHexTile("The Hub"); // Vector3.Angle(transform.position, Vector3.zero); //   transform.localEulerAngles.y
        return bearingToHub;    
    }
    public float GetDistanceToHub()   {        return distanceToHub;   }
    public float GetBearingToHexTile(string name) {       
        //Vector2Int _tileCoords = HexaWorld.Instance.GetHexTileCoordinatesByName(name);
        //Vector3 _tileCoords3 = new Vector3 (_tileCoords.x, 0, _tileCoords.y);
        if (GameManager.Instance.isHexaWorld)  return Vector3.SignedAngle(Vector3.back, HexaWorld.Instance.GetHexTileWorldCoordinatesByName(name) - (transform.position ), Vector3.up) + 180;    // didn't change anything + GetComponent<Rigidbody>().centerOfMass
        if (!GameManager.Instance.isHexaWorld)
        {
            if (name == "The Hub")
            {
                return Vector3.SignedAngle(Vector3.back,  -(transform.position), Vector3.up) + 180;
            }
        }
        return 0;
    }

    public float GetDustDensity() { return dustDensity;  }


    private void TestScenarioHandler(int test)
    {
        Debug.Log("test scenario " + test);
        if (test == 1 || test == 4) {
            ToggleLights(false);
            transform.position = GameManager.Instance.StartPostions[test].position; // new Vector3(86.5f,0f,2f); // let's move to port
            transform.rotation = GameManager.Instance.StartPostions[test].rotation;
            followTransform.transform.rotation = followTransform.transform.rotation * Quaternion.AngleAxis(140, Vector3.up);
            GearChange(4);


            /*
            PlayerMovement.Instance.SetGold(100);
            PlayerMovement.Instance.inv.SetItemQuantity(12, 6);
            PlayerMovement.Instance.inv.SetItemQuantity(13, 3);
            PlayerMovement.Instance.inv.SetItemQuantity(14, 4);
            PlayerMovement.Instance.inv.SetItemQuantity(15, 5);
            PlayerMovement.Instance.inv.SetItemQuantity(16, 0);
            PlayerMovement.Instance.inv.SetItemQuantity(17, 1);
            */

            inv.AddItem(1, 32);
            inv.AddItem(2, 2);
            inv.AddItem(11, 1);
            

        }
        if (test == 5) // Osbourne Conclusion
        {
            ToggleLights(false);
            transform.position = GameManager.Instance.StartPostions[test].position; // new Vector3(86.5f,0f,2f); // let's move to port
            transform.rotation = GameManager.Instance.StartPostions[test].rotation;
            followTransform.transform.rotation = followTransform.transform.rotation * Quaternion.AngleAxis(140, Vector3.up);
            GearChange(4);


            /*
            PlayerMovement.Instance.SetGold(100);
            PlayerMovement.Instance.inv.SetItemQuantity(12, 6);
            PlayerMovement.Instance.inv.SetItemQuantity(13, 3);
            PlayerMovement.Instance.inv.SetItemQuantity(14, 4);
            PlayerMovement.Instance.inv.SetItemQuantity(15, 5);
            PlayerMovement.Instance.inv.SetItemQuantity(16, 0);
            PlayerMovement.Instance.inv.SetItemQuantity(17, 1);
            */

            inv.AddItem(1, 32);
            inv.AddItem(2, 2);
            inv.AddItem(11, 1);
            inv.AddItem(500, 1); // the black box
        }

        if (test == 2)
        {
            ToggleLights(false);
            transform.position = GameManager.Instance.StartPostions[test].position; // new Vector3(86.5f,0f,2f); // let's move to port
            transform.rotation = GameManager.Instance.StartPostions[test].rotation;
            followTransform.transform.rotation = followTransform.transform.rotation * Quaternion.AngleAxis(140, Vector3.up);
            GearChange(2);


        }

        if (test == 3) //inventory slot test
        {

            inv.AddItem(2, 4);
            inv.AddItem(2, 2);
            inv.AddItem(2, 4);
            inv.AddItem(2, 4);
            inv.AddItem(2, 2);
             
            inv.AddItem(3, 1);

            inv.ListSlots();
        }

        if (test == 10)
        {
            // we are loading the game, so no initial conditions!
        }

    }



    // saving

    public object CaptureState()
    {
        Dictionary<string, object> savedata = new Dictionary<string, object>();
        savedata["position"] = new SerializableVector3(transform.position);
        savedata["rotation"] = new SerializableVector3(transform.eulerAngles);
        savedata["fuel"] = fuel;
        savedata["gear"] = GetGear(); // just in case we allow saving while moving!
        savedata["VisionBlockingTexture"] = FogOfWarManager.INSTANCE.GetVisionBlockingTextureForSaving();
        if (interactible != null)
        {
            if (interactible.GetComponent<Interactible>().dockable && GameManager.Instance.isDocked)
            {
                savedata["interactibleName"] = interactible.transform.name; // surely, it's docked, right?
                Debug.Log("We have saved the game interactible name as " + interactible.transform.name);
            } else
            {

                savedata["interactibleName"] = ""; // no it ain't
            }
        } else
        {
            savedata["interactibleName"] = ""; // no it ain't
        }

        return savedata;
    }

    public void RestoreState(object state)
    {
        Dictionary<string, object> savedata = (Dictionary<string, object>)state;
        transform.position = ((SerializableVector3)savedata["position"]).ToVector();
        transform.eulerAngles = ((SerializableVector3)savedata["rotation"]).ToVector();
        fuel = (float)savedata["fuel"];
        GearChange((int)savedata["gear"]);
        FogOfWarManager.INSTANCE.SetVisionBlockingTexture((byte[])savedata["VisionBlockingTexture"]);
        if ((string)savedata["interactibleName"] == "")
        {
            if (GameManager.Instance.stage != GameManager.Stage.game)
            {
                EscPressedBehaviour();
            }
            interactible = null; // I don't thin I'm discovering America here. But just in case let's undock I guess.

        }
        else
        {
            interactible = GameManager.Instance.GetPortByName((string)savedata["interactibleName"]).transform;
            Debug.Log("We have restored the interactible name as " + interactible.transform.name);
            // gamemanager to cycle through portbeh.transform.name; 
            // set interactinbble to interactibleName from dictionary
            // faking docking is easier than otherwise.
            GameManager.Instance.SetDocking(true);
            DockingSuccessful();
        }
    }
}
