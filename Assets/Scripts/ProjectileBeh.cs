using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBeh : MonoBehaviour
{

    [SerializeField] GameObject explosion;
    public float damagePower;
    public float armourPenetration;
    private Transform originator;
    private string originatorName;
    private string originatorFingerprint;

    private void OnCollisionEnter(Collision collision)
    {
        // if (collision.collider.CompareTag("Player")) return;
        if (collision.collider.CompareTag("Enemy")) // hit an enemy
        {
            // get the enemy health script and deal damage.
            // return;
            // collision.collider.transform.GetComponent<Health>().AddHealth(-damagePower);
            // collision.collider.transform.GetComponent<Health>().SetLastHit(originatorName);
        }

        //GameObject exp = Instantiate(explosion, transform.parent.transform);
        Quaternion q = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
        //GameObject exp = Instantiate(explosion, transform.position, q, null);
        GameObject exp = Instantiate(explosion, collision.contacts[0].point, q, null);

        //Debug.Log("Spawning explosion " + exp.transform.position);

        Destroy(gameObject);


    }

    public void SetOriginator(Transform t)
    {
        originator = t;
        originatorName = t.name;
        originatorFingerprint = t.GetComponent<Contactable>().GetFingerprint();
    }
    public string GetOriginatorName()
    {
        return originatorName;
    }

    public Transform GetOriginator()
    {
        return originator;
    }
    public string GetOriginatorFingerprint()
    {
        return originatorFingerprint;
    }
    public float GetDamagePower()
    {
        return damagePower;
    }

}
