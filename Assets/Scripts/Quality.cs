using GameDevTV.Saving;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/*
 * This is th e Quality framework. It stores information about everything player related (his qualities) in a normalised format, accessible to everything.
 * Will be used by Dialogue Node (conditions for enablement) On dialogue node displayed (conversant) � node has condition
 * Inventory calls it at every item change.
 * On quest adding or removing
 * QuestListPlayer listens for events.
 * 
 *    0 -  999  items
 * 1000 - 1999  quests
 * 2000 - 2499  humans
 * 2500 - 2999  buffs (likely mostly timed)
 * 3000 - 3999  events, achievements, flags, properties.
 * 
 */

// Done: Game Manager reports its items into the dictionary.




public class Quality : MonoBehaviour, ISaveable, IPredicateEvaluator
{

    // public List<quality> quaList = new List<quality> ();    // list of qualities the player has.
    public quality[] quArray = new quality[6000];
    // public Dictionary<int, string> quaDictionary = new Dictionary<int, string> (); // this dictionary should hold list of all qualities to serve as a raeference table in times of need.

    public static event Action<int> OnQualityChanged;
    [HideInInspector] public static Quality Instance { get; private set; }

    [System.Serializable]
    public struct quality
    {
        public int nature;
        public int number; // obsolete in aarray quArray, but used for saving
        public string name;
        public int quantity;
        public float timeStarted; // when was the quality started
        public float timeAlotted; // how many game time second units it's supposed to be good for.
    }

    private void Awake()
    {
        if (Instance == null) { Instance = this; }
        EventQualities();
        Invoke("PrintDictionary", 10f);
    }

    public bool IsQuality(int q)
    {
        // return (quaList.Any(_ql => _ql.number == q)); // this is so elegant, but frightening
        return (quArray[q].name != "");
    }

    public int LookUpQualityNumberFromNameInQuaray(string name) // there has to be "objective" version of lookup in dictionary.
    {
        int i = 0;
        for (i = 0; i < quArray.Length; i++)
        {
            if (quArray[i].name == name) break;
        }
        return i; // putting it here just in case bad stuff makes duplicate qualities, so I can catch it.
    }

    public void IncrementQuality(int q, int amount) // adds amount to 
    {
        Debug.Log("Incrementing " + quArray[q].name + " of " + quArray[q].quantity + " by " + amount);
        quArray[q].quantity += amount;
    }

    public void SetQuality(int q, int amount) // overwrites amount to . will be used by health and other stuff that calculates its stuff better.
    {
        Debug.Log("setting " + quArray[q].name + " of " + quArray[q].quantity + " to " + amount);
        quArray[q].quantity = amount;
    }

    private bool IsQualityOutOfDate(int q)
    {
        if (quArray[q].timeAlotted == 0) return false;
        return ((quArray[q].timeStarted + quArray[q].timeAlotted) < GameManager.Instance.gameTime );
    }

    public int GetQuantity(int q) // returns quantity of particular quality. returns 0 if not found. Should be preceded with "IsQuality" really, but who cares.
    {
        return quArray[q].quantity;
    }



    public bool InitialiseQualityEntry(string n, int i)  // this is the initial setup so all objects can spam this with their entries.
                                                         // did bool for error control. just in case.
    {
        if (quArray[i].name != "") return false;
        quArray[i].name = n;
        return true;
    }



    public void PrintDictionary() // metnd for debug, so we can see what qualities were added to the dictionary
    {
        string _s = "<color=red>quArray contents:</color>\n\r";
        for (int i=0; i < quArray.Length; i++) 
        {
            if (quArray[i].name != "")
            {
                _s += i + "   " + quArray[i].name;

                if (quArray[i].quantity > 0) _s += "  <color=yellow>" + quArray[i].quantity + "</color>";
                _s +=  "\n\r";
            }
        }
        Debug.Log(_s);
    }

    public void ResetTimeLeft(int q) // resets time left to current time
    {
        quArray[q].timeStarted = GameManager.Instance.gameTime;

    }


    public float GetTimeRemaining(int q)    // returns quantity of particular quality. returns 0 if not found. Should be preceded with "IsQuality" really, but who cares.
                                            // deletes an entry with negative time left
                                            // no, it doesn't delete it anymore, because the array is too cheap to do it.
    {
        float _timeRem = quArray[q].timeAlotted - (GameManager.Instance.gameTime - quArray[q].timeStarted);
        if (_timeRem > 0) return _timeRem; 
        
        return 0;
    }

    public float GetTimeRemainingPercent(int q)    // Exactly as above, but returns 0..1
    {
        float _timeRem = (quArray[q].timeAlotted - (GameManager.Instance.gameTime - quArray[q].timeStarted)) / quArray[q].timeAlotted;
        if (_timeRem > 0) return _timeRem; 
        return 0;
    }


    /*
    Methods:
    Bool isQuality � returns true if quantity > 0
    Int qualityCount � returns number of quality.
    Float getTimeLeft � returns time left, doh.In Time units.
    Float getTimeLeftPercent � returns 1-0 range of time left
    Void resetTimeLeft � sets timeleft to the initial timeleft.
    Void UpdateQuality(
    void overwrteQuality - for inventory and such, whare  another script calculates the quantity and feeds me a ready result.

    Saveability!

    Variables:
    Main data list is qualities list of quality
    Quality (type): int nature, int number, string name, time started, time allotted,
    */





// ---------------------- save  ---------------------------
public object CaptureState()
    {
        List<object> state = new List<object>();
        string dlog = "";
        for (int i = 0; i < quArray.Length; i++)
        {
            quality _s = quArray[i];
            _s.number = i;
            state.Add(_s);
            if (_s.name != "") dlog += "Saving "+_s.number+"   "+_s.name+"\n\r";
        }
        Debug.Log(dlog);
        return state;
    }

    public void RestoreState(object state)
    {


        // quArray = (quality[])state;
    
        string muchRestored = "";
        List<object> stateList = state as List<object>;
        if (stateList == null) return;

        // quaList.Clear(); // can't do that with an array, now, can i?
        foreach (object _s in stateList)
        {
            quality _q = (quality)_s;
                quArray[_q.number] = _q;
                //quArray[_q.number].name = _q.name;
                //quArray[_q.number].timeAlotted = _q.timeAlotted;
            //quArray[_q.number].timeStarted = _q.timeStarted;
            muchRestored += "Restored quality " + _q.number + " " + _q.name + "  " + _q.quantity + " \n\r";
        }
        Debug.Log(muchRestored);
    
    }


    // ------------------------------------ Predicate for questing -------------------------------------------


    public bool? Evaluate(string predicate, string[] args)
    {
        if (predicate == "HasQuality")
        {
            // args[0] is quality number
            // args[1] is minimum value to retur true
            // args[2] is max val to ret true.
            int _qnum = quArray[int.Parse(args[0])].quantity;
            if (_qnum >= int.Parse(args[1]) && _qnum <= int.Parse(args[2])) return true;

            return false;
        }

        return null;
    }

    private void EventQualities() // this method fills the array with event qualities, as I don't want another scriptable object mess
                                  // I'm actually happier to see this in code than in the inspector.
                                  // searchability is easier, too.
    {
        quArray[3000].name = "Sharma's journey"; quArray[3000].timeAlotted = 50000; // I want the counter to reset. Sharma travels between ports.
        quArray[3001].name = "Osbourne's wreck discovered";
        quArray[3002].name = "Whedon's Lament discovered";
        quArray[3003].name = "Hull percentage";




    }

}
