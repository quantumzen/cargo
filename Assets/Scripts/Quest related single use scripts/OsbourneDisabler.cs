using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OsbourneDisabler : MonoBehaviour
{

    [SerializeField] GameObject Circle1;
    [SerializeField] GameObject Circle2;
    [SerializeField] GameObject Circle3;
    [SerializeField] GameObject CircleFinal;
    [SerializeField] GameObject TheyreComing;

    public void FinalPoint()
    {
        Circle1.SetActive(false);
        Circle2.SetActive(false);
        if (Circle3 != null) Circle3.SetActive(false); // it may have been destroyed
        CircleFinal.SetActive(true);
        TheyreComing.SetActive(true);
    }
}
