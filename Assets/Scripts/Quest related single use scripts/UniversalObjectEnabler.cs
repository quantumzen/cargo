using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniversalObjectEnabler : MonoBehaviour
{
    [SerializeField] List<ToEnable> Enablifiers = new List<ToEnable>();

    [System.Serializable]
    public class ToEnable
    {

        public bool not = false;
        public GameObject objectToEnable;
        
        
    }

    public void TriggerEnablifiers()
    {
        foreach (ToEnable toEnable in Enablifiers)
        {
            string _s = "";
            if (toEnable.not) _s += "disabling"; else _s += "enabling";
            _s += " object " + toEnable.objectToEnable.transform.name;
            Debug.Log(_s);
            toEnable.objectToEnable.SetActive(!toEnable.not);
        }
    }
}
