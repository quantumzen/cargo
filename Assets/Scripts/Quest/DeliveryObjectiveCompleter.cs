using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliveryObjectiveCompleter : MonoBehaviour
{
    [SerializeField] RPG.Quests.Quest quest;
    [SerializeField] string objComplete;
    [SerializeField] string objActivate;
    [SerializeField] portBeh port;
    [SerializeField] int stillToDeliver;
    [SerializeField] int invNum;
    int mustDeliver;

    void Awake()
    {
        mustDeliver = stillToDeliver;
        portBeh.OnShopTransaction += ShopTransactionHandler;
    }

    void OnDestroy()
    {

        portBeh.OnShopTransaction -= ShopTransactionHandler;
    }

    private void ShopTransactionHandler(int num, int quantity, portBeh portTransacting)
    {
        if  (portTransacting == port && num == invNum && RPG.Quests.QuestListPlayer.Instance.HasQuest(quest) && RPG.Quests.QuestListPlayer.Instance.GetObjectiveStatus(quest, objComplete) == RPG.Quests.QuestTaskStatus.Status.active)
        {
            stillToDeliver += quantity; // a bit reverse to keep intuitive with portBeh buy/sell algo

            RPG.Quests.QuestListPlayer.Instance.ObjectiveCompletionChange(quest, objComplete, Mathf.FloorToInt(((mustDeliver - stillToDeliver) / mustDeliver) * 100), false);

            if (stillToDeliver <= 0) {

                //watch out. the task status gets updated in QuestListPlayer, on objective quantity to max ...or rather 0.
                if (objComplete.Length > 0) RPG.Quests.QuestListPlayer.Instance.ObjectiveStatusChange(quest, objComplete, RPG.Quests.QuestTaskStatus.Status.completed);
                if (objActivate.Length > 0) RPG.Quests.QuestListPlayer.Instance.ObjectiveStatusChange(quest, objActivate, RPG.Quests.QuestTaskStatus.Status.active);

            }


        }

    }
}
