using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventLogger : MonoBehaviour
{
    [SerializeField] Logarium.LogType type;
    [SerializeField] string narrative;
    [SerializeField] int date;
    [SerializeField] int qualityAffected = 0;
    [SerializeField] int qamount = 0;


    public void TriggerEvent()
    {
        if (date == 0) date = GameManager.Instance.GetComponent<Clock>().GetIntDate();
        Logarium.LogLineData lld = new Logarium.LogLineData();
        lld.type = type;
        lld.date = date;
        lld.desc = narrative;
        Logarium.Instance.LogEvent(lld);

        if (qualityAffected > 0)
        {
            Quality.Instance.IncrementQuality(qualityAffected, qamount);
        }
    }
    
}
