using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Quests
{


    public class GiverOfQuest : MonoBehaviour
    {

        [SerializeField] Quest quest;

        public void GiveQuest()
        {
            Debug.Log("Giving the quest " + quest.name);
            QuestListPlayer.Instance.AddQuest(quest);
        }


    }
}