using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPredicateEvaluator
{
    // HasInventoryItem invnum, quantity (must be ints!)
    // HasQuest quest.name
    // CompletedQuest quest.name
    // HasObjectiveActive (Quest q, string objective)
    // HasQuality (int qualityNumber, int minValue, int maxValue)  demonstrated in node 9b1e6ee0-e3ae-466e-9b5c-5313b82f8777
    bool? Evaluate(string predicate, string[] args);
}
