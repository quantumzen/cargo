using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace RPG.Quests
{
    public class ObjectiveActivator : MonoBehaviour
    {
        [SerializeField] Quest quest;
        // [SerializeField] string objective; // I need to supply these in the method, so I can do multiple operations with a single
        // this makes no sense.
        // why don't I call it straight from Beh?

        // [SerializeField] QuestTaskStatus.Status newStatus;


        public void ObjectiveStatusChange(string objective)
        {
            QuestListPlayer.Instance.ObjectiveStatusChange(quest, objective, QuestTaskStatus.Status.active);
        }

    }
}
