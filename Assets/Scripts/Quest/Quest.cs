using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Quests
{
[CreateAssetMenu(fileName = "Quest", menuName = "Quest", order = 0)]
public class Quest : ScriptableObject
    {
        [SerializeField] string title;
        [SerializeField] int number; // this quest serial number to be used by Quality framework
        [SerializeField] string originator;
        [SerializeField] List<QuestTaskStatus> tasks = new List<QuestTaskStatus>(); // string[] tasks;
        [SerializeField] List<Reward> rewards = new List<Reward>();
        //[SerializeField] string[] rewards;

        [System.Serializable]
        public class Reward
        {
            public int invItem;
            [Min (1)]  public int quantity;
            public bool immaterial = false;
            public string immaterialText = ""; // for rewards like "You will not be immediatelly killed in a most gruesome way".

            public void Give()
            {
                if (!immaterial) PlayerMovement.Instance.inv.AddItem(invItem, quantity);
            }
        }

        /*
        [System.Serializable]
        class Objective
        {
            public string reference;
            public string description;
        }
        */

        public string GetTitle()
        {
            return title;
        }
        public int GetQuestNumber()
        {
            return number;
        }

        public string GetOriginator()
        {
            return originator;
        }
        public int GetObjectiveCount()
        {
            return tasks.Count;
        }

        public List<QuestTaskStatus> GetTasks()
        {
            return tasks;
        }

        public int GetRewardsCount()
        {
            return rewards.Count;
        }

        public IEnumerable<QuestTaskStatus> GetAllTasks()
        {
            return tasks;
        }
        public IEnumerable<Reward> GetRewards()
        {
            return rewards;
        }

        public static Quest GetByName(string questName)
        {
            foreach ( Quest q in Resources.LoadAll<Quest>(""))
            {
                // Debug.Log("Checking Quests one by one: " + q.name);
                if (q.name == questName) return q;
            }
            return null;
        }
    }
}
