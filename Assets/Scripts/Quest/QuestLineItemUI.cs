using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPG.Quests;
using TMPro;

public class QuestLineItemUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI title;
    [SerializeField] TextMeshProUGUI progress;
    public QuestListUI questListUIparent;
    string originator;
    QuestStatus  questStatus;



    public void Setup(QuestStatus q)
    {
        questStatus = q;
        title.text = q.GetQuest().GetTitle();
        originator = q.GetQuest().GetOriginator();
        progress.text = q.GetTasksCount(QuestTaskStatus.Status.completed)+"/" + q.GetAllTasksCount();
    }

    public void Abandon()
    {
        // we call the questarium and remove this quest from the list
        // we call redraw? Or questarium does that
        // we destroy.
        //
        // QuestListPlayer.Instance.AbandonQuest(questStatus); // why don't you do it yourself if all you're doing is flipping the status?
        // You know what? I will!
        questStatus.SetStatus(QuestTaskStatus.Status.abandoned);

        // questListUIparent.UpdateQuestList(); shouldn't need it anymore as triggered by an event.
    }

    public QuestStatus GetQuestStatus()
    {
        return questStatus;
    }
}
