using GameDevTV.Saving;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RPG.Quests
{
    
    public class QuestListPlayer : MonoBehaviour, ISaveable, IPredicateEvaluator
    {

        [HideInInspector] public static QuestListPlayer Instance { get; private set; }
        [SerializeField] List<QuestStatus> statuses = new List<QuestStatus>(); // list of quests localised for the player's use. Usually only when assigned, but I may choose to do it for permanent/persistent quests
        [SerializeField] List<TestScenarioQuests> tests = new List<TestScenarioQuests>();
        [SerializeField] QuestListUI questListUI;

        [System.Serializable]
        struct TestScenarioQuests
        {
            public List<Quest> questsForTest;
            public int test;
        }
        private void Awake()
        {
            if (Instance == null) { Instance = this; }
            GameManager.OnTestScenarioChange += TestScenarioHandler;
            GameManager.OnStageChange += WipePlayer;
        }

        private void OnDestroy()
        {
            GameManager.OnTestScenarioChange -= TestScenarioHandler;
            GameManager.OnStageChange -= WipePlayer;
        }

        private void Start()
        {
            Debug.Log("Adding Quests to Dictionary in QuestListPlayer Start");
            foreach (Quest q in Resources.LoadAll<Quest>(""))
                {
                    Quality.Instance.InitialiseQualityEntry(q.name,q.GetQuestNumber()); // deprecated AddToDictionary
                                                                                        // q.name is the name of the file (scriptable object. Not very pretty, but too deeply embedded for me to uproot it.
                                                                                        // q.GetTitle or something like that would be best.
            }
        }

        private void TestScenarioHandler(int test)
        {

            if (test == 1) PopulateQuestsForTest(test); // let's populate test quests.
            if (test == 4) PopulateQuestsForTest(test); // let's populate test quests.
            if (test == 5)
            {
                PopulateQuestsForTest(test); // let's populate test quests.
                ObjectiveStatusChange(tests[1].questsForTest[0], "Return the goods to the Charitable Gentleman.", QuestTaskStatus.Status.active);
            }
        }

        public IEnumerable<QuestStatus> GetStatuses()
        {            return statuses;        }

        private void PopulateQuestsForTest(int test)
        {
            TestScenarioQuests ind = new TestScenarioQuests();
            foreach (TestScenarioQuests ts in tests)
            {
                if (ts.test == test) ind = ts;
            }

            foreach (Quest q in ind.questsForTest)
            {
                Debug.Log("Populating a test quest " + q.GetTitle());
                AddQuest(q);
            }
        }

        public void AbandonQuest(QuestStatus qs)
        {
            if (statuses.Contains(qs))
            {
                // statuses.Remove(qs); // I can't just delete it like it never existed. This is datacide!
                qs.SetStatus(QuestTaskStatus.Status.abandoned);
                Debug.Log("You have chosen to abandon a quest");
            }
        }

        public void AddQuest(Quest q)
        {
            if (HasQuest(q)) return;
            QuestStatus qs = new QuestStatus(q);
            qs.Setup(q);
            statuses.Add(qs);
            qs.SetStatus(QuestTaskStatus.Status.active); // I'm allowing myself to activate here as this is where the quest is added to the player.
            // questListUI.UpdateQuestList(); I shouldnt' need it, as it now is triggered by the event. // good thing I noted it here though. Thanks, Michael from the past!
        }

        public bool HasQuest(Quest q)
        {
            foreach (QuestStatus qs in statuses) if (qs.GetQuest() == q) return true; // compact like ... a neutron star.
            return false;
        }


        // here is my take on objective completion. As objectives can also fail, I need to take that into account.
        // in fact, a reward should be given for completing an objective, not the quest.
        public void ObjectiveStatusChange(Quest q, string objective, QuestTaskStatus.Status st) 
        {
            if (!HasQuest(q)) return;
            QuestStatus _qs = GetQuestStatusFromQuest(q);
            foreach (QuestTaskStatus _qts in _qs.GetAllTasks())
            {
                if (_qts.text == objective)
                {
                    Debug.Log("changing objective " + objective + " to " + st.ToString());
                    _qts.status = st;
                    if (st == QuestTaskStatus.Status.completed)
                    {
                        // first check if this task has its individual rewadss and attempt a payout.
                        // immaterial should have its own custom code action triggers something whatnot.
                        if (_qts.HasRewards())
                        {
                            foreach (Quest.Reward qr in _qts.GetRewards())         qr.Give();
                        }
                        if (AreAllNonOptionalObectivesComplete(_qs) && _qs.GetStatus() != QuestTaskStatus.Status.completed) // we don't want to pay out twice, so I'm checking if completed
                        {
                            // change quest status to complete
                            _qs.SetStatus(QuestTaskStatus.Status.completed);
                            // pay out the reward.
                            foreach (Quest.Reward qr in q.GetRewards())                qr.Give();
                        }
                    }
                    _qs.QuestUpdate();
                }
            }
        }




        // iterating through all questsstatus objs to check if all non-optional are completed.
        private bool AreAllNonOptionalObectivesComplete(QuestStatus _qs)
        {
            foreach (QuestTaskStatus _qts in _qs.GetAllTasks())
            {
                if (_qts.optional == false && _qts.status != QuestTaskStatus.Status.completed)
                {
                    Debug.Log(_qts.text + " ... " + _qts.status.ToString());

                    return false;
                }
            }

            return true;
        }

        // We're retrieving QuestStatus object that's associated with the Quest. We're not chaecking if the player has the quest,
        // as ObjectiveStatusChange (it's biggest fan) does it. 
        private QuestStatus GetQuestStatusFromQuest(Quest q)
        {
            foreach (QuestStatus _qs in statuses)
            {
                if (_qs.GetQuest() == q) // we've found the quest!
                {
                    // Debug.Log("We've found the quest " + _qs.GetQuest().GetTitle());
                    return _qs;
                }
            }
            return null;
        }

        public QuestTaskStatus.Status GetObjectiveStatus(Quest q, string objective)
        {
            if (!HasQuest(q)) return  QuestTaskStatus.Status.inactive;
            foreach (QuestStatus _qs in statuses)
            {
                if (_qs.GetQuest() == q) // we've found the quest!
                {
                    foreach (QuestTaskStatus _qts in _qs.GetAllTasks())
                    {
                        if (_qts.text == objective)
                        {
                            return _qts.status;
                            
                        }
                    }
                }
            }
            return QuestTaskStatus.Status.inactive;
        }


        public void ObjectiveCompletionChange(Quest q, string objective = "", int completion = 0, bool completionIncrement = true)
        {
            if (!HasQuest(q)) return;
            foreach (QuestStatus _qs in statuses)
            {
                if (_qs.GetQuest() == q) // we've found the quest!
                {
                    Debug.Log("We've found the quest " + _qs.GetQuest().GetTitle());
                    foreach (QuestTaskStatus _qts in _qs.GetAllTasks())
                    {
                        if (_qts.text == objective)
                        {
                            int currentCompletion = 0;
                            if (completionIncrement) currentCompletion = _qts.completion;
                            completion += currentCompletion;
                            Debug.Log("changing objective completion " + objective + " to " + completion);
                            _qts.completion = completion;
                            // if (_qts.completion > 100) _qts.status = QuestTaskStatus.Status.completed; // AHTUNG! objective auto-completes having reached 100!
                            // I can't do this here. 
                            // there may be other things that need to be triggered and only the Completer knows.
                            _qs.QuestUpdate();
                        }
                    }
                }
            }
        }

        private void WipePlayer(GameManager.Stage sOld, GameManager.Stage sNew)
        {
            if (sOld != GameManager.Stage.menu && sNew == GameManager.Stage.menu)
            {
                statuses.Clear();
            }
        }

        // ----------------------------------------------------- saaving
        public object CaptureState()
        {
            List<object> state = new List<object>();
            foreach (QuestStatus _s in statuses)
            {
                state.Add(_s.CaptureState());
            }
            return state;
        }
        
        public void RestoreState(object state)
        {
            List<object> stateList = state as List<object>;
            if (stateList == null) return;

            statuses.Clear();
            foreach (object _s in stateList)
            {
                statuses.Add(new QuestStatus(_s)); // TODO: it does not work. I need to understand saveable
            }
            questListUI.UpdateQuestList(); // should call an event, but this will have to do.
        }

        public bool? Evaluate(string predicate, string[] args)
        {
            if (predicate == "HasQuest") return HasQuest(Quest.GetByName(args[0]));
            if (predicate == "CompletedQuest")
            {
                if (HasQuest(Quest.GetByName(args[0])))
                {
                    QuestStatus qs = GetQuestStatusFromQuest(Quest.GetByName(args[0]));
                    if (qs.GetStatus() == QuestTaskStatus.Status.completed) return true;
                }
                return false;
            }
            if (predicate == "HasObjectiveActive")
            {
                if (GetObjectiveStatus(Quest.GetByName(args[0]), args[1]) == QuestTaskStatus.Status.active) return true;
                return false;
            }
            //Debug.Log("Checking if we have the quest " + args[0] + " evaluating to " + Quest.GetByName(args[0]));
            if (predicate == "HasInventoryItem")
            {
                Debug.Log(PlayerMovement.Instance.inv.GetItemQuantity(int.Parse(args[0])) + " quantity needed " + int.Parse(args[1]));
                if (PlayerMovement.Instance.inv.GetItemQuantity(int.Parse(args[0])) >= int.Parse(args[1])) return true;
                return false;
            }

            return null;
        }
    }
}
