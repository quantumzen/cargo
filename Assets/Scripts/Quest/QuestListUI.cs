using RPG.Quests;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestListUI : MonoBehaviour
{
    // [SerializeField] private Quest[] tempQuests;
    [SerializeField] QuestTaskStatus.Status tab = QuestTaskStatus.Status.active;
    [SerializeField] QuestLineItemUI questPrefab;
    List<QuestLineItemUI> qList = new List<QuestLineItemUI>();

    void Start()
    {
        Invoke("UpdateQuestList", 0.1f);
    }
    private void Awake() { QuestStatus.OnQuestUpdated += OnQuestUpdatedListBehaviour; }
    private void OnDestroy() { QuestStatus.OnQuestUpdated -= OnQuestUpdatedListBehaviour; }

    private void OnQuestUpdatedListBehaviour(QuestStatus qs)
    {
        UpdateQuestList();
    }

    public void UpdateQuestList()
    {
        ClearUIList();
        //QuestListPlayer questList = GameManager.Instance.GetComponent<QuestListPlayer>();
        //Debug.Log("Updating QuestList UI");

        foreach (QuestStatus q in QuestListPlayer.Instance.GetStatuses())
        {
            
            //Debug.Log("populationg quest line item " + q.GetStatus().ToString() + "   " + q.GetQuest());
            if (q.GetStatus() == tab) // display only the ones we want to see.
            {
                QuestLineItemUI qli = Instantiate<QuestLineItemUI>(questPrefab, transform);
                qli.Setup(q);
                qList.Add(qli);
                qli.questListUIparent = this;
            }
        
        }
    }

    private void ClearUIList()
    {
        foreach (QuestLineItemUI qs in qList)
        {
            Destroy(qs.gameObject);
        }
        qList.Clear();
    }


}
