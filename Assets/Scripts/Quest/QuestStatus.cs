using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Quest exists as objective reference. Needs to be "instantiated". By that, I mean entered into the list of Player Quests (QuestListPlayer).
 * 
 * QuestStatus is basically a Quest, instantiated. Except we don't instantiate, as it's not monobeh

 * QuestListUI rules the UI
 * for each of the QuestStatus in QuestListPlayer
 * it spawns a QuestLineItemUI(QuestStatus)
 * its Setup() relies heavily on .GetQuest().GetTitle(); type methods from Quest()
 * 
 * The Tooltip is spawned based on the data pulled from QuestToolTipUI using GetQuestStatus();
 * and populates the tooltip QuestToolTipUI with Setup(q);
 * 
 */

namespace RPG.Quests
{

    [System.Serializable]
    public class QuestStatus 
    {
        [SerializeField] List<QuestTaskStatus> tasks = new List<QuestTaskStatus>();
        // quest objective has status, completion, text.
        Quest quest;
        private QuestTaskStatus.Status status;   // who cares it's public, really?!
                                                // actually, I do, if you don't mind. I need to control any change to this so the event can be triggeerd.
        public static event Action<QuestStatus> OnQuestUpdated;
        
        /* // I don't really need these here now, do I.
        string title;
        string originator;
        string[] taskText;
        string[] rewards;
        private object s;
        */

        [System.Serializable]
        class QuestStatusRecord
        {
            public string questName;
            public string status;
            public List<string> objectives;
        }

        // I need this constructor for QuestListPlayer.AddQuest, but you already know that, don't you.
        public QuestStatus(Quest quest)
        {
            this.quest = quest;
        }

        // I need constructor for the save ISaveable interface
        public QuestStatus(object s)
        {
            QuestStatusRecord qsr = s as QuestStatusRecord;

            // Debug.Log("restoring "+qsr.status+" quest " + qsr.questName);
            quest = Quest.GetByName(qsr.questName);
            foreach (string todestring in qsr.objectives)
            {
                tasks.Add(DestringifyObjective(todestring));
            }
            Enum.TryParse(qsr.status, true, out status);
            //QuestUpdate();  // forcing it, as it won't trigger by itself. It must be thinking nobody is subscribed.

        }

        public Quest GetQuest()
        {
            return quest;
        }

        public void QuestUpdate()
        {
            OnQuestUpdated?.Invoke(this);
        }

        // I'd very much rather localise eerythgn about the quest here in the Quest Status.
        // I'll do it with the Setup(Quest) method.

        public void Setup(Quest q)
        {
            //title = q.GetTitle();
            //originator = q.GetOriginator(); // I don't really use these.
            quest = q;
            foreach (QuestTaskStatus qst in q.GetTasks())
            {
                QuestTaskStatus questTaskStatus = new QuestTaskStatus();

                questTaskStatus.status = qst.status;
                questTaskStatus.completion = qst.completion;
                questTaskStatus.text = qst.text;
                questTaskStatus.SetQuest(qst.GetQuest());
                questTaskStatus.optional = qst.optional; // copying all these tot he local instance just in case.
                // and it's easier to copy rewards, too :/
                questTaskStatus.SetRewards(qst.GetRewards());
                questTaskStatus.SetQuest(quest.GetTitle()); // this is important so tasks don't get orphaned on load save.
                tasks.Add(questTaskStatus);
            }

        }

        public void SetStatus(QuestTaskStatus.Status s)
        {
            status = s;
            if (OnQuestUpdated != null) QuestUpdate(); // capturing nulls before stuff is subscribed.
        }

        public QuestTaskStatus.Status GetStatus()
        {
            return status;
        }

        // ------------------------------------------------------------------------------------------------------------------------------
        #region Task Getters

        public IEnumerable<QuestTaskStatus> GetAllTasks()
        {
            return tasks;
        }
        public int GetAllTasksCount()
        {
            return tasks.Count;
        }

        // this method is to return task status. I have a bit of a problem, as I don't know how encapsulate them. Array? List?
        public IEnumerable<QuestTaskStatus> GetTasks(QuestTaskStatus.Status s)
        {
            List<QuestTaskStatus> _qsList = new List<QuestTaskStatus>();
            foreach (QuestTaskStatus qs in tasks)
            {
                if (qs.status == s)
                {
                    _qsList.Add(qs);
                }
            }
            return _qsList;
        }
        public int GetTasksCount(QuestTaskStatus.Status s)
        {
            int _qsList = 0;
            foreach (QuestTaskStatus qs in tasks)
            {
                if (qs.status == s)   _qsList++;
                
            }
            return _qsList;
        }

        #endregion


       


        // ------------------------------------------------------------------------------------------------------------------------------
        #region task Parsing and capturing state

        private string[] explodePipes(string s)
        {
            return s.Split(new string[] { "||" }, StringSplitOptions.None); // remember I'll return empty [0]
        }
        private string joinPipes(string[] s)
        {
            string resultat = "";
            foreach(string _s in s)
            {
                resultat = resultat + "||" + _s; // this way element zero will be empty, I know, I know.
            }
            return resultat;
        }

        private string StringifyObjective(QuestTaskStatus qts)
        {
            string result = qts.status.ToString() + "||" + qts.text + "||" + qts.completion.ToString() + "||" + qts.GetQuest();
            //Debug.Log("Stringified tasK: " + result);
            return result;
        }

        private List<string> GetAllStringifiedObjectives()
        {
            List<string> objestives = new List<string>();
            foreach (QuestTaskStatus qts in tasks)
            {
                objestives.Add(StringifyObjective(qts));
            }
            return objestives;
        }

        private QuestTaskStatus DestringifyObjective(string s)
        {
            QuestTaskStatus qts = new QuestTaskStatus();
            //Debug.Log("Destringifying: " + s);
            string[] split = explodePipes(s);
            Enum.TryParse(split[0], true, out qts.status);
            qts.text = split[1];
            qts.completion = int.Parse(split[2]);
            qts.SetQuest(split[3]);
            return qts;
        }

        public object CaptureState()
        {
            QuestStatusRecord state = new QuestStatusRecord();
            state.questName = quest.name;
            state.status = status.ToString();
            state.objectives = GetAllStringifiedObjectives();
            return state;
        }

        #endregion

    }


}