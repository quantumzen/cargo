using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Quests
{
    // I am status of a task. Each quest contains many tasks.
    // QuestStatus has a list of me.
    // will be used instead QuestStatusRecord from tutorial (lecture 59)

    [System.Serializable]
    public class QuestTaskStatus
    {
        public enum Status { failed, inactive, completed, hidden, active, abandoned, deleted }; // I'll need to write a manual for each of these one day!
        public int completion; // from 0 to 100, because we're sticking to int and it'll be easier torepresent as %. I'm excusing my inconsistency here. I know.
        public Status status;
        public string text;
        private string quest; // this quest name needs to be set so I can associate loaded objectives with their wuersts. It may be redundant. we'll see.
        public bool optional;
        [SerializeField] List<Quest.Reward> rewards = new List<Quest.Reward>();


        public QuestTaskStatus(Status s = Status.active, string taskLabel = "", int taskCompletion = 0, bool isOptional = false) // behold the optional constructor parameters defaulting!
        {
            status = s;
            completion = taskCompletion;
            text = taskLabel;
            optional = isOptional;
        }

        public bool HasRewards()        {            return (rewards.Count > 0);        }
        public IEnumerable<Quest.Reward> GetRewards()        {            return rewards;        }
        public void SetRewards(IEnumerable<Quest.Reward> r)
        {
            foreach (Quest.Reward _r in r)
            {
                Quest.Reward reward = new Quest.Reward();
                reward.invItem = _r.invItem;
                reward.quantity = _r.quantity;
                reward.immaterial = _r.immaterial;
                reward.immaterialText = _r.immaterialText;
                rewards.Add(reward);
            }
        }

        public void SetQuest(string s)       {            quest = s;        }
        public string GetQuest()        {           return quest;        }
    }
}