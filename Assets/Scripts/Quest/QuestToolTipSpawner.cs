using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace RPG.Quests
{
    public class QuestToolTipSpawner : ToolTipSpawner
    {
        public override bool CanCreateTooltip()
        {
            return true;
        }

        public override void UpdateTooltip(GameObject tooltip)
        {
            QuestStatus q = GetComponent<QuestLineItemUI>().GetQuestStatus();
            tooltip.GetComponent<QuestToolTipUI>().Setup(q);
        }



    }
}
