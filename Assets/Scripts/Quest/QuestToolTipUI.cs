using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using RPG.Quests;
using TMPro;
using UnityEngine.UI;

public class QuestToolTipUI : MonoBehaviour
{

    [SerializeField] TextMeshProUGUI title;
    [SerializeField] Transform tasksParent;
    [SerializeField] Transform rewardsParent;
    [SerializeField] QuestTooltipTasksLineUI taskLinePrefab;
    [SerializeField] Sprite taskCompleteSprite;
    [SerializeField] Sprite taskFailedSprite;
    [SerializeField] Sprite taskActiveSprite;
    [SerializeField] Sprite taskInactiveSprite;
    private List<QuestTooltipTasksLineUI> currentTaskLines = new List<QuestTooltipTasksLineUI>();
    private List<QuestTooltipTasksLineUI> currentRewardLines = new List<QuestTooltipTasksLineUI>();
    private List<Quest.Reward> rewards = new List<Quest.Reward>(); // I'm not convinced it's practical!
    private string normalFont;
    string originator;
    Quest quest;


    public void Setup(QuestStatus q)
    {
        // rewards line item - I'll reuse the task one, even though parent's "upper center" alignment causes a bit of vertical misalignment.
        // populate rewards list first main reward, then in the task iterator.
        // spawn rewards lines having composed all that.

        normalFont = "<color=#" + ColorUtility.ToHtmlStringRGBA(GameManager.Instance.narrativeText) + ">";

        foreach (QuestTooltipTasksLineUI qtlui in currentTaskLines)
        {
            Destroy(qtlui.gameObject);
        }
        foreach (QuestTooltipTasksLineUI qtlui in currentRewardLines)
        {
            Destroy(qtlui.gameObject);
        }
        quest = q.GetQuest();
        title.text = q.GetQuest().GetTitle();
        originator = q.GetQuest().GetOriginator();
        foreach (Quest.Reward qr in q.GetQuest().GetRewards())
        {

            SpawnRewardLineItem(qr, q.GetStatus());
        }
        foreach (QuestTaskStatus s in q.GetAllTasks())
        {

            if (s.status != QuestTaskStatus.Status.hidden)
            {
                QuestTooltipTasksLineUI tlui = Instantiate(taskLinePrefab, tasksParent);
                if (s.status == QuestTaskStatus.Status.active)
                {
                    tlui.SetSprite(taskActiveSprite);
                    tlui.SetTask(GetLineItemFormattingPrefix(s.status) + s.text + GetLineItemFormattingSuffix(s.status));
                    if (s.HasRewards())
                    {
                        foreach (Quest.Reward qr in s.GetRewards()) SpawnRewardLineItem(qr, s.status);
                    }
                }
                if (s.status == QuestTaskStatus.Status.failed)
                {
                    tlui.SetSprite(taskFailedSprite);
                    tlui.SetTask(GetLineItemFormattingPrefix(s.status) + s.text + GetLineItemFormattingSuffix(s.status));
                    if (s.HasRewards())
                    {
                        foreach (Quest.Reward qr in s.GetRewards()) SpawnRewardLineItem(qr, s.status);
                    }
                }
                if (s.status == QuestTaskStatus.Status.completed)
                {
                    tlui.SetSprite(taskCompleteSprite);
                    tlui.SetTask(GetLineItemFormattingPrefix(s.status) + s.text + GetLineItemFormattingSuffix(s.status));
                    if (s.HasRewards())
                    {
                        foreach (Quest.Reward qr in s.GetRewards())  SpawnRewardLineItem(qr, s.status);
                    }
                }
                if (s.status == QuestTaskStatus.Status.inactive)
                {
                    tlui.SetSprite(taskInactiveSprite);
                    tlui.SetTask(GetLineItemFormattingPrefix(s.status) + s.text + GetLineItemFormattingSuffix(s.status));
                }
                currentTaskLines.Add(tlui);
            }
        }
    }

    private void SpawnRewardLineItem(Quest.Reward qr, QuestTaskStatus.Status qsts = QuestTaskStatus.Status.active) // active is vanilla
    {
        QuestTooltipTasksLineUI tlui = Instantiate(taskLinePrefab, rewardsParent);
        if (!qr.immaterial)
        {
            tlui.SetSprite(GameManager.Instance.GetRaritySpriteByItemNumber(qr.invItem));
            if (qr.quantity > 1)
            {
                tlui.SetTask(GetLineItemFormattingPrefix(qsts) + qr.quantity + " " + GameManager.Instance.GetItemByNumber(qr.invItem).pluralname + GetLineItemFormattingSuffix(qsts));
            } else
            {
                tlui.SetTask(GetLineItemFormattingPrefix(qsts) + GameManager.Instance.GetItemByNumber(qr.invItem).itemName + GetLineItemFormattingSuffix(qsts));
            }
        }
        else
        {
            tlui.SetTask(GetLineItemFormattingPrefix(qsts) + qr.immaterialText + GetLineItemFormattingSuffix(qsts));
        }
    }



    private string GetLineItemFormattingPrefix(QuestTaskStatus.Status qstst)
    {

        switch (qstst)
        {
            case QuestTaskStatus.Status.inactive:
                return "<color=#" + ColorUtility.ToHtmlStringRGBA(GameManager.Instance.normalText) + ">";
            case QuestTaskStatus.Status.completed:
                return " <color=#" + ColorUtility.ToHtmlStringRGB(GameManager.Instance.GetComponent<RPG.Dialogue.Conversant>().GetRiskColor(1)) + ">";
            case QuestTaskStatus.Status.failed:
                return "<color=#" + ColorUtility.ToHtmlStringRGBA(GameManager.Instance.failedQuestText) + ">";
            default:
                return normalFont;
        }
    }
    private string GetLineItemFormattingSuffix(QuestTaskStatus.Status qstst)
    {
        switch (qstst)
        {

            case QuestTaskStatus.Status.failed:
                return normalFont+"</s>";
            default:
                return normalFont;
        }
    }

}
