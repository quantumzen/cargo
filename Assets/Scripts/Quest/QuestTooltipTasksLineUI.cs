using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class QuestTooltipTasksLineUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI task;
    [SerializeField] Image completionIndicator;
    public void SetTask(string t)
    {
        task.text = t;
    }
    public void SetSprite(Sprite s)
    {
        completionIndicator.sprite = s;
    }
}
