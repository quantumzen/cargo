using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevTV.Saving;
using UnityEngine.InputSystem;

// C:\Users\loref\AppData\LocalLow\QuantumZen\Dust
// saving path
public class SaveWrapper : MonoBehaviour
{

    SavingSystem ss;
    [SerializeField] string saveFileName;
    // Start is called before the first frame update
    void Start()
    {
        ss = GetComponent<SavingSystem>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void QuickLoadPressed(InputAction.CallbackContext val) // Escape
    {
        if (val.started)
        {
            Load("quicksave");
        }
    }
    public void QuickSavePressed(InputAction.CallbackContext val) // Escape
    {
        if (val.started && !GameManager.Instance.IsStalwart())
        {
            Save("quicksave");
        }
    }

    public void Save(string f = "")
    {
        if (f == "") f = saveFileName;
        ss.Save(f);
    }
    public void Load(string f = "")
    {

        if (f == "") f = saveFileName;
        ss.Load(f);
    }
}
