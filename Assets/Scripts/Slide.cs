using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Slide : MonoBehaviour
{

    public void PanelUp()
    {
        transform.DOLocalMoveY(0, 1.5f).SetEase(Ease.InOutCirc).SetDelay(1f);
    }
    public void PanelDown()
    {
        transform.DOLocalMoveY(-2800, 1.5f).SetEase(Ease.InOutCirc);
    }
}
