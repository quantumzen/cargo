using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.InputSystem;
using System;
using UnityEngine.UI;
     


public class TurretBehaviour : MonoBehaviour
{


    [HideInInspector] public static TurretBehaviour Instance { get; private set; }

    private Transform player;
    [SerializeField] private Transform turretCore;
    [SerializeField] private Transform barrel0;
    [SerializeField] private Transform barrel1;
    [SerializeField] private Transform cowl;
    [SerializeField] private Transform turretCoreRetracted;
    [SerializeField] private Transform turretCoreDeployed;
    [SerializeField] private Transform Barrel0Retracted;
    [SerializeField] private Transform Barrel0Deployed;
    [SerializeField] private Transform Barrel1Retracted;
    [SerializeField] private Transform Barrel1Deployed;
    private bool mechanismBusy;
    public bool isDeployed;
    public bool aimTrue;
    public float heat;
    public float reload;
    public float elevation; // x axie 10 deg to -15 up
    public int ammo;
    private float elevationMin = -10;
    private float elevationMax = 5; // that's barrel down, mind you!
    private bool isSwappingAmmo;

    private Vector3 targetRotation;
    [SerializeField] Transform targetRotationPoint; // this is where the mouse is aiming
    private float acceleration = 0; // this is the angular momentum of the turret. It doesn't stop immediately. In fact, the ship should be using a similar mechanism. Ok, it uses rigidbody and it's elegant, I get it.
    private float accellerationConstantCoefficient = 1f;
    private float targetElevation;
    [SerializeField] float aimPrecision = 0.5f;

    [SerializeField] private int ammoLoaded = 3; // 3 4 5 respectively - these correspond with item IDs
    [SerializeField] private int piercerAmmo = 0;
    [SerializeField] private int splasherAmmo = 0;
    [SerializeField] private int scorcherAmmo = 0;

    [SerializeField] GameObject projPiercer;
    [SerializeField] GameObject projSplasher;
    [SerializeField] GameObject projScorcher;

    private GameObject projectile;
    [SerializeField] private Transform muzzle;
    [SerializeField] private ParticleSystem muzzleFlash;
    [SerializeField] private Transform cameraRotationCenter;

    [SerializeField] Sprite[] ammoSwithImage;
    [SerializeField] Transform ammoSwitcherUI;

    [SerializeField] Image aimUI;
    [SerializeField] Image aimReal;
    private float aimAlpha = 1; // so it retracts well
    private bool CurrentAimAlpha = false;
    private AudioSource audioSource;
    [SerializeField] AudioSource muzzleAudioSource;
    [SerializeField] AudioSource muzzleHeatClangAudioSource;
    [SerializeField] AudioClip shotSound;
    [SerializeField] AudioClip heatClangSound;
    [SerializeField] AudioClip turretRotateSound;



    // events --------------------------------------------------
    public static event Action<float> OnElevationChange;


    void Awake()
    {
        player = transform.parent;
        acceleration = 0;
        ammo = 64;
        // isSwappingAmmo = true; // to give the player few seconds without shooting
        // this gets unlocked nlu on turret deployment. As it starts deployed, it's a bit of a problem.
        mechanismBusy = false;
        isDeployed = true;
        if (Instance == null) { Instance = this; }
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {

        GUIHandler.Instance.ProjectileFired( ammo, reload, GetAmmoName()); //initialise display
        projectile = projPiercer; // for start, right?
        ammoLoaded = 3;
        CycleTurret();
    }

    // Update is called once per frame
    void Update()
    {

        if ((GameManager.Instance.stage == GameManager.Stage.game)  && (GameManager.Instance.isMovementEnabled)) TurretOperations(); // removed && (!GameManager.Instance.isDocked) && (!GameManager.Instance.isDocking) because docking should allow reloading and such. 
        CurrentAimAlpha = isDeployed; // for now I see no reason to do it otherwise.
        if (CurrentAimAlpha && aimAlpha < 0.98)
        {
            SetAimAlpha(1);
        } else {
            if (!CurrentAimAlpha && aimAlpha > 0.01)
            {
                SetAimAlpha(0);
            }
        }


        // Debug.Log(divergence);
    }


    private void TurretOperations()
    {
        aimTrue = false;
        if (reload > 0)
        {
            reload -= 2f * Time.deltaTime * (isSwappingAmmo ? 0.05f : 1f);
            // I need to update the GUI somewhere. I forgot where I did that. That's why one shouldn't just run across the field and drop global vars.
            GUIHandler.Instance.UpdateReloadBar(reload);
                // I use                 GUIHandler.Instance.ProjectileFired(ammo, reload, GetAmmoName()); only when fired.
        }
        if (heat > 0) heat -= 0.05f * Time.deltaTime;

        if (!isDeployed)
        {
            targetRotation = new Vector3(turretCoreRetracted.position.x, turretCore.position.y, turretCoreRetracted.position.z) - turretCore.position; // it's in Update() so when the ship turns, the rotation angle updates too

        }
        else
        {
            // all the normal turret operation here.
            // targetRotation = turretCoreDeployed.position - turretCore.position;
            targetRotation = new Vector3(targetRotationPoint.position.x, turretCore.position.y, targetRotationPoint.position.z) - turretCore.position;
            Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;
            // Debug.DrawRay(transform.position, forward, Color.green); // it wasn'tdrawing anyway



        }
        float divergence = Vector3.Angle(targetRotation, turretCore.forward);

        /*
         * if (divergence > 30) // full sped rotation unless close to the aim target
        {
            turretCore.rotation = Quaternion.Lerp(turretCore.rotation, Quaternion.AngleAxis(turretCore.rotation.eulerAngles.y, Vector3.up), accellerationConstantCoefficient * Time.deltaTime);
        } else 
        */

        if (divergence > aimPrecision)  // we're not done with the rotation
        {
            turretCore.rotation = Quaternion.Slerp(turretCore.rotation, Quaternion.LookRotation(targetRotation), accellerationConstantCoefficient * Time.deltaTime);
            if (!audioSource.isPlaying) audioSource.Play();
            audioSource.pitch = 0.7f + Mathf.Min(divergence/50,0.5f);
            audioSource.volume = 0.1f + Mathf.Min(divergence / 30, 0.4f);

        }
        else
        {
            // squelch the rotation sound.
            audioSource.Stop();
            aimTrue = true;
        }
        /*
         * �I do not aim with my hand; he who aims with his hand has forgotten the face of his father.
         * I aim with my eye.
         * 
         * I do not shoot with my hand; he who shoots with his hand has forgotten the face of his father.
         * I shoot with my mind.
         * 
         * I do not kill with my gun; he who kills with his gun has forgotten the face of his father.
         * I kill with my heart.� 
         * */
        if (!isDeployed && divergence < 4f) aimTrue = true; // good enough for retracting the turret



        if (Mathf.Abs(cowl.eulerAngles.x - targetElevation) > 0.2f)
        {
            // cowl.rotation = Quaternion.Slerp(cowl.rotation, Quaternion.LookRotation(targetRotation), accellerationConstantCoefficient * Time.deltaTime/2);
        }
    }

    #region obsolete pieces of twine that may come in handy someday
    public void StowTurret()
    {
        // first, calculate the angle to which we'll rotate. In this case, backwards (in relation tothe palyer)

        // Vector3 stow = turretCoreRetracted.position - turretCore.position;
        //turretCore.DORotate(turretCoreRetracted.position, 2f); // turretCoreRetracted.rotation.eulerAngles, 2f);
        /// turretCore.rotation = Quaternion.LookRotation(stow);
        // turretCore.DOLocalMove(Vector3.back, 2f);
    }
    public void DeployTurret()
    {

        //turretCore.DORotate(turretCoreDeployed.position, 2f);

        //Vector3 stow = turretCoreDeployed.position - turretCore.position;
        //turretCore.DORotate(turretCoreRetracted.position, 2f); // turretCoreRetracted.rotation.eulerAngles, 2f);
        //turretCore.rotation = Quaternion.LookRotation(stow);
        

    }
    #endregion

    public void CycleTurret() // bool retract = null)
    {
        if (!isDeployed && !mechanismBusy && reload <= 0)
        {
            mechanismBusy = true;
            isSwappingAmmo = false;
            // targetRotationPoint = turretCoreDeployed.position;
            //Debug.Log("deploying the turret");
            isDeployed = true;

            StartCoroutine(DeployBarrel());
            // obviously the DOTweens proven unruly and difficult to troubleshoot.
            // I haven't tried DOLocalMove though.
            // barrel0.DOMove(Barrel0Deployed.position, 1f);
            // barrel1.DOMove(Barrel1Deployed.position, 3f);
        }
        if (isDeployed && !mechanismBusy)
        {
            mechanismBusy = true;
            SetElevation(0);
            //Debug.Log("Stowing the turret");
            isDeployed = false; // I know, I should wait for these to complete. For reloading, for example
            if (ammo == 0) SwapAmmo(ammoLoaded); // tough. no other way to do it ...easily.
            
            // barrel0.DOMove(Barrel0Retracted.position, 1f);
            // barrel1.DOMove(Barrel1Retracted.position, 3f);
            StartCoroutine(RetractBarrel());

        }

    }

    private IEnumerator RetractBarrel()
    {
        

        // Debug.Log("Retraction of the barrel from " + barrel0.localPosition.z + " to " + Barrel0Retracted.localPosition.z);
        while (barrel0.localPosition.z > Barrel0Retracted.localPosition.z +0.01f || barrel1.localPosition.z > Barrel1Retracted.localPosition.z + 0.005f)

        {
            barrel0.localPosition = new Vector3(0, barrel0.localPosition.y, Mathf.Lerp(barrel0.localPosition.z, Barrel0Retracted.localPosition.z, 0.03f));
            barrel1.localPosition = new Vector3(0, barrel1.localPosition.y, Mathf.Lerp(barrel1.localPosition.z, Barrel1Retracted.localPosition.z, 0.05f));
            yield return new WaitForSeconds(0.01f);
        }

        while (!aimTrue) yield return new WaitForSeconds(0.1f);

        while (turretCore.localPosition.y > turretCoreRetracted.localPosition.y + 0.01f)

        {
            turretCore.localPosition = new Vector3(turretCore.localPosition.x, Mathf.Lerp(turretCore.localPosition.y, turretCoreRetracted.localPosition.y, 0.06f), turretCore.localPosition.z);
            yield return new WaitForSeconds(0.01f);
        }
        // Debug.Log("Barrel retraction complete");

        mechanismBusy = false;
        yield return null;

    }
    private IEnumerator DeployBarrel()
    {
        while (turretCore.localPosition.y < turretCoreDeployed.localPosition.y - 0.01f)

        {
            turretCore.localPosition = new Vector3(turretCore.localPosition.x, Mathf.Lerp(turretCore.localPosition.y, turretCoreDeployed.localPosition.y, 0.06f), turretCore.localPosition.z);
            yield return new WaitForSeconds(0.01f);
        }

        // Debug.Log("Deployment of the barrel from " + barrel0.localPosition.z + " to " + Barrel0Deployed.localPosition.z);
        while (barrel0.localPosition.z < Barrel0Deployed.localPosition.z - 0.01f || barrel1.localPosition.z < Barrel1Deployed.localPosition.z - 0.005f)

        {
            barrel0.localPosition = new Vector3(0, barrel0.localPosition.y, Mathf.Lerp(barrel0.localPosition.z, Barrel0Deployed.localPosition.z, 0.03f));
            barrel1.localPosition = new Vector3(0, barrel1.localPosition.y, Mathf.Lerp(barrel1.localPosition.z, Barrel1Deployed.localPosition.z, 0.05f));
            yield return new WaitForSeconds(0.01f);
        }
        // Debug.Log("Barrel Deployment complete");
        mechanismBusy = false;
        yield return null;

    }

    public void ToggleAmmoType(InputAction.CallbackContext val)
    {
        if (val.started)
        {

            SwapAmmo(0); // zero means increment 
        }
    }

    public void TryReloadingIfEmpty()
    {
        if (ammo == 0) SwapAmmo(ammoLoaded);
    }

    public void TryReloadingRegardlessIfEmpty(InputAction.CallbackContext val)
    {
        if (val.started)     SwapAmmo(ammoLoaded);
    }


    public void SwapAmmo(int am)
    {
        if (!isDeployed)
        {
            Debug.Log("Swapping ammo " + am);
            if (am == 0)
            {
                am = ammoLoaded + 1;
                if (am > 5) am = 3;
            }

            if (!isDeployed)
            {        // just to make sure the value is stored for when they swap the clip
                Debug.Log("Putting away the ammo " + ammoLoaded + " " + ammo);
                if (ammoLoaded == 3) { piercerAmmo += ammo; }
                if (ammoLoaded == 4) { splasherAmmo += ammo; }
                if (ammoLoaded == 5) { scorcherAmmo += ammo; }
                ammo = 0;
                ammoLoaded = am;
                isSwappingAmmo = true;
                reload = 1f;
                GUIHandler.Instance.ProjectileFired(ammo, reload, GetAmmoName()); // this method will pull all sorts of public vars from

                if (PlayerMovement.Instance.inv.GetItemQuantity(am) > 0)
                {
                    PlayerMovement.Instance.inv.AddItem(am, -1);
                    ammo = 64;
                }
                else
                {
                    Debug.Log("Not enough ammo of this variety. Loading the best clip available. ");
                    if (am == 3) { ammo = piercerAmmo; piercerAmmo = 0; }
                    if (am == 4) { ammo = splasherAmmo; splasherAmmo = 0; }
                    if (am == 5) { ammo = scorcherAmmo; scorcherAmmo = 0; }

                }
                if (ammo > 64)
                {
                    PlayerMovement.Instance.inv.AddItem(am, Mathf.FloorToInt(ammo / 64));
                    ammo = ammo % 64;
                }
                Debug.Log("Loaded " + ammo + " of ammo " + ammoLoaded);
            }
            // TODO test with ammo > clip size if inventory increases.

            if (am == 3) projectile = projPiercer;
            if (am == 4) projectile = projSplasher;
            if (am == 5) projectile = projScorcher;
            ammoSwitcherUI.GetComponent<Image>().sprite = ammoSwithImage[am - 3];
        }
    }

    public void SetElevation(float e)
    {
        if (!isDeployed) return;
        OnElevationChange?.Invoke(e);
        targetElevation = e;
        Quaternion q = new Quaternion (targetElevation, 0f, 0f, 90);
        cowl.DOLocalRotateQuaternion(q, 2f);
        //Debug.Log("New Elevation " + e);
    }

    public void IncreaseElevation(InputAction.CallbackContext val)
    {
        if (val.started)  SetElevation(Mathf.Max(targetElevation - 1, elevationMin));
    }
    public void DecreaseElevation(InputAction.CallbackContext val)
    {
        if (val.started) SetElevation(Mathf.Min(targetElevation + 1,elevationMax));
    }

    public void FireProjectile()
    {
        if (reload <= 0 && PlayerMovement.Instance.GetHeat() < 24 && ammo > 0 && isDeployed && !mechanismBusy)
        {
            reload = 1f;
            heat += 0.1f;
            ammo--;
            muzzleAudioSource.PlayOneShot(shotSound);
            muzzleHeatClangAudioSource.Play();
            muzzleHeatClangAudioSource.volume = (PlayerMovement.Instance.GetHeat() -20) /15;


            GUIHandler.Instance.ProjectileFired(ammo, reload, GetAmmoName()); // this method will pull all sorts of public vars from

            Quaternion q = Quaternion.Euler(cowl.rotation.eulerAngles.x , turretCore.rotation.eulerAngles.y, turretCore.rotation.eulerAngles.z);
            GameObject proj = Instantiate(projectile, muzzle.position,  q, null);
            proj.GetComponent<ProjectileBeh>().damagePower = 0.1f;
            proj.GetComponent<ProjectileBeh>().SetOriginator(transform.parent.transform);
            // Debug.Log("rotation of the projectile iiiis " + q);
            Rigidbody projRB = proj.GetComponent<Rigidbody>();
            projRB.AddRelativeForce(Vector3.forward * 200f);   // AddForce(Vector3.forward*1000f);
            muzzleFlash.Play();
        }
        
    }

    private string GetAmmoName()
    {
        if (ammoLoaded == 4) return "ammo (s)";
        if (ammoLoaded == 5) return "ammo (b)";
        return "ammo (p)";
    }

    public float GetAmmoMass()
    {
        return projectile.GetComponent<Rigidbody>().mass;
    }
    
    private void SetAimAlpha(float target)
    {
        aimAlpha = aimUI.material.color.a;
        aimUI.material.SetColor("_Color", new Color(1, 1, 1, Mathf.Lerp(aimAlpha, target, 5f * Time.deltaTime)));
        aimReal.material.SetColor("_Color", new Color(1, 1, 1, Mathf.Lerp(aimAlpha, target, 5f * Time.deltaTime)));
        //Debug.Log(aimAlpha);
    }

}
