using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vessel : MonoBehaviour
{
    private Humans humans;
    [SerializeField] List<vesselData> vessels = new List<vesselData>();
    [SerializeField] int currentVessel;

    [System.Serializable]
    public struct vesselData
    {
        public int passengerCapacity;
        public int cargoSlots;
        public string vesselType;
    }

    private void Awake() { GameManager.OnTestScenarioChange += TestScenarioHandler; }

    private void OnDestroy() { GameManager.OnTestScenarioChange -= TestScenarioHandler; }
    private void Start()
    {
        humans = GetComponent<Humans>();
    }




    private void TestScenarioHandler(int test)
    {

        AddHuman("You");

        if (test == 1 || test == 2 || test == 4)
        {
            currentVessel = 0;
            AddHuman("Zero");
            AddHuman("Sharma");
        }
        currentVessel = 0; // anyway, just in case I do sthing stupid

    }

    public void AddHuman(string name)
    {
        if (humans.CountHumansAboard() < vessels[currentVessel].passengerCapacity)
        {
            if (!humans.IsHumanAboard(name)) humans.AddHuman(name);
        }
    }
    public void RemoveHuman(string name)
    {
        if (humans.IsHumanAboard(name)) humans.RemoveHuman(name);
        
    }
    public vesselData GetCurrentVesselData()
    {
        return vessels[currentVessel];
    }




}
