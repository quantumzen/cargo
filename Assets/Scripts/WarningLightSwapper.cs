using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class WarningLightSwapper : MonoBehaviour
{

    [SerializeField] Sprite[] spr;

    public void ChangeSpr(int s)
    {
        GetComponent<Image>().sprite = spr[s];
    }
}
