using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameDevTV.Saving;

public class portBeh : MonoBehaviour, ISaveable
{
    public Transform cmspot; // this is where the Cinemachine cam goes when docked.
    [SerializeField] Transform shopButtonsPanel;
    [SerializeField] GameObject shopButtonPrefab;
    [SerializeField] GameObject ShopCanvas; // will be enabled or disabled on dock undock
    [SerializeField] Dockable startInventory;
    [SerializeField] Inventory inv;
    [SerializeField] int InventorySlots;
    private List<InventorySlot> invSlots = new List<InventorySlot>();
    private List<ShopLineBeh> shopButtons = new List<ShopLineBeh>();
    private List<int> notBuyingTheseItems = new List<int>();
    private List<int> ButtonNums = new List<int>();
    [SerializeField] int przycinka = 2;
    [SerializeField] bool isStorage;
    [SerializeField] bool isNotAccepting;
    [SerializeField] int invResetInterval = 0; // how many months between inventory resets.
    private int invResetCount = 0;

    // [SerializeField] Sprite[] rarity;
    public static event Action<int, int, portBeh> OnShopTransaction;


    private void Awake()
    {
        Clock.OnMonthChange += InvResetHandle;
    }
    private void OnDestroy()
    {
        Clock.OnMonthChange -= InvResetHandle;
    }

    private void Start()
    {
        if (inv == null || inv == transform.GetComponent<Inventory>()) inv = transform.GetComponent<Inventory>();
        for (int i = 0; i < InventorySlots; i++)
        {
            invSlots.Add(inv.CreateInventorySlot());
        }


        GameManager.Instance.AddPort(this);
        Invoke("PopulateInventory", 0.5f);
        if (isNotAccepting) NotAcceptingAnything();
        // SpawnShopButtons();
        //invResetCount = invResetInterval;
    }


    public void PrepareButtonTable() // Player movement calls this on "isDocked"
    {
        if (ShopCanvas != null ) ShopCanvas.SetActive(true);
        ButtonNums.Clear();
        ClearShopButtons();
        foreach (Item _itm in GameManager.Instance.items)
        {
           if ((PlayerMovement.Instance.inv.GetItemQuantity(_itm.number) > 0 && !IsNotBuying(_itm.number)) || inv.GetItemQuantity(_itm.number) > 0) ButtonNums.Add(_itm.number);
        }
        SpawnShopButtons();
    }

    public void DisableCanvas()
    {
        if (ShopCanvas != null ) ShopCanvas.SetActive(false);
    }


// TODO  add price modifiers?

    private void SpawnShopButtons() {
      int buttonCount = 0; //legacy and laziness
      for (int i = 0; i < ButtonNums.Count; i++)
      {
            GameObject g = Instantiate(shopButtonPrefab, shopButtonsPanel);
            g.transform.localPosition = new Vector3(400+32, -64 * buttonCount - 80, 0); // (g.transform.localPosition.x, g.transform.localPosition.y + 64 * i, g.transform.localPosition.z);
            shopButtons.Add(g.transform.GetComponent<ShopLineBeh>());
            shopButtons[i].rarity.GetComponent<Image>().sprite = GameManager.Instance.GetRaritySpriteByItemNumber(ButtonNums[i]);
            shopButtons[i].itemName.text = GameManager.Instance.GetItemByNumber(ButtonNums[i]).itemName;
            shopButtons[i].num = ButtonNums[i];
            if (!isStorage)
            {
                shopButtons[i].sellPrice.text = GameManager.Instance.GetItemByNumber(ButtonNums[i]).price.ToString();
                shopButtons[i].buyPrice.text = (GameManager.Instance.GetItemByNumber(ButtonNums[i]).price + przycinka).ToString();
            }
            shopButtons[i].available.text = inv.GetItemQuantity(ButtonNums[i]).ToString();
            shopButtons[i].owned.text = PlayerMovement.Instance.inv.GetItemQuantity(ButtonNums[i]).ToString(); // I can't directly reference here.
          // in fact, there is something wrong with PlayerMovement.Instance.inv.GetItemQuantity. It's not returning what I want.
            shopButtons[i].pb = this;

          //Debug.Log("Player Owns " + PlayerMovement.Instance.PrintInvCount(inv.items[i].item.number).ToString() + " of " + inv.items[i].item.name.ToString()); //inv.GetItemQuantity(inv.items[i].item.number).
                                                                                                                                                               //disable sell button if not buying item
          if (IsNotBuying(shopButtons[i].num)) shopButtons[i].sell.interactable = false;
          buttonCount++;
          //Debug.Log("button count: " + buttonCount);
      }
    }

    private void InvResetHandle(int month)
    {
        //invResetInterval
        if (invResetInterval > 0)
        {
            if (invResetInterval == invResetCount)
            {
                // reset inventory as the count is up.
                inv.PurgeInvslots();
                PopulateInventory();
                invResetCount = 0;
            } else
            {
                Debug.Log("Will reset inventory of " + transform.name + " in " + (invResetInterval - invResetCount) + " months.");
                invResetCount++;
            }
            
        }
        
    }

    #region Deprecated
    private void SpawnShopButtons_Deprecated()
    {
        Debug.Log("Spawning "+ inv.items.Count + " Shop Buttons of the port ");
        shopButtons.Clear();


        // this code is super clunky. It rearranges buttons each time item is sold out and is pulled from Player's inventory. What we should be doing is to iterate through items array as they are organised, by numbers.

        int buttonCount = 0;
        for (int i = 0; i < inv.items.Count; i++)
        {
            GameObject g = Instantiate(shopButtonPrefab, shopButtonsPanel);
            g.transform.localPosition = new Vector3(400+32, -64 * buttonCount - 80, 0); // (g.transform.localPosition.x, g.transform.localPosition.y + 64 * i, g.transform.localPosition.z);
            shopButtons.Add(g.transform.GetComponent<ShopLineBeh>());
            shopButtons[i].itemName.text = inv.items[i].item.itemName;
            shopButtons[i].num = inv.items[i].item.number;
            shopButtons[i].sellPrice.text = inv.items[i].item.price.ToString();
            shopButtons[i].available.text = inv.items[i].quantity.ToString();
            shopButtons[i].owned.text = PlayerMovement.Instance.inv.GetItemQuantity(inv.items[i].item.number).ToString(); // I can't directly reference here.
            // in fact, there is something wrong with PlayerMovement.Instance.inv.GetItemQuantity. It's not returning what I want.
            shopButtons[i].pb = this;
            shopButtons[i].num = inv.items[i].item.number;

            //Debug.Log("Player Owns " + PlayerMovement.Instance.PrintInvCount(inv.items[i].item.number).ToString() + " of " + inv.items[i].item.name.ToString()); //inv.GetItemQuantity(inv.items[i].item.number).
                                                                                                                                                                 //disable sell button if not buying item
            if (IsNotBuying(shopButtons[i].num)) shopButtons[i].sell.interactable = false;
            buttonCount++;
            //Debug.Log("button count: " + buttonCount);
        }

        for (int i = 0; i < PlayerMovement.Instance.inv.items.Count; i++)
        {
            if (!IsButtonListed(PlayerMovement.Instance.inv.items[i].item.number) && !IsNotBuying(PlayerMovement.Instance.inv.items[i].item.number))
            {
                GameObject g = Instantiate(shopButtonPrefab, shopButtonsPanel);
                g.transform.localPosition = new Vector3(400 + 32, -64 * buttonCount - 80, 0);
                shopButtons.Add(g.transform.GetComponent<ShopLineBeh>());
                shopButtons[buttonCount].itemName.text = PlayerMovement.Instance.inv.items[i].item.itemName;
                shopButtons[buttonCount].num = PlayerMovement.Instance.inv.items[i].item.number;
                shopButtons[buttonCount].sellPrice.text = PlayerMovement.Instance.inv.items[i].item.price.ToString();
                shopButtons[buttonCount].available.text = inv.GetItemQuantity(PlayerMovement.Instance.inv.items[i].item.number).ToString();
                shopButtons[buttonCount].owned.text = PlayerMovement.Instance.inv.GetItemQuantity(PlayerMovement.Instance.inv.items[i].item.number).ToString(); // I can't directly reference here.
                                                                                                                                        // in fact, there is something wrong with PlayerMovement.Instance.inv.GetItemQuantity. It's not returning what I want.
                shopButtons[buttonCount].pb = this;
                shopButtons[buttonCount].num = PlayerMovement.Instance.inv.items[i].item.number;

                //Debug.Log("Player Owns " + PlayerMovement.Instance.PrintInvCount(PlayerMovement.Instance.inv.items[i].item.number).ToString() + " of " + inv.items[i].item.name.ToString()); //inv.GetItemQuantity(inv.items[i].item.number).
                                                                                                                                                                     //disable sell button if not buying item
                if (IsNotBuying(shopButtons[buttonCount].num)) shopButtons[buttonCount].sell.interactable = false;
                buttonCount++;

                Debug.Log("button count: " + buttonCount + " i: " + i);
            }

        }




    }

    #endregion


    public void RefreshInventoryDisplay() // action trigger from a dialogue wants to use it.
    {
        foreach(ShopLineBeh sh in shopButtons)
        {
            // Destroy(sh.transform.gameObject);
            // sh.itemName.text = GameManager.Instance.GetItemByNumber(sh.num).name; //we don't need that
            if (!isStorage)
            {
                sh.sellPrice.text = GameManager.Instance.GetItemByNumber(sh.num).price.ToString();
                sh.buyPrice.text = (GameManager.Instance.GetItemByNumber(sh.num).price + przycinka).ToString();
            }
            sh.available.text = inv.GetItemQuantity(sh.num).ToString();
            sh.owned.text = PlayerMovement.Instance.inv.GetItemQuantity(sh.num).ToString(); // I can't directly reference here.
            if (IsNotBuying(sh.num)) sh.sell.interactable = false;

        }
    }

    private void ClearShopButtons()
    {
        foreach (ShopLineBeh sh in shopButtons)
        {
            Destroy(sh.gameObject);
        }
        shopButtons.Clear();
    }

    private bool IsNotBuying(int num)
    {
        foreach (int n in notBuyingTheseItems)
        {

            if (num == n)
            {
                //Debug.Log("Not Buying " + num);
                return true;
            }
        }
        return false;
    }

    private bool IsButtonListed(int num)
    {
        foreach (ShopLineBeh sh in shopButtons)
        {

            if (num == sh.num)
            {
                //Debug.Log("already there! " + num);
                return true;
            }
        }

        return false;
    }



    private void PopulateInventory()
    {
        Dockable d = startInventory;
        foreach (Dockable.invlist il in d.inv)
        {

            inv.AddItem(il.id, il.quantity);
        }

        foreach (int non in d.notBuying)
        {
            notBuyingTheseItems.Add(non);
        }

        inv.ListSlots();
    }

    private void NotAcceptingAnything()
    {
        for (int i = 1; i < 1000; i++)
        {
            notBuyingTheseItems.Add(i);
        }
    }


    // --------------------------- Buying and selling

    public void OnBuyClicked(int num)
    {
        // check if the shop has more than one item
        // check if player has >= money than price (or if it's a storage and requires no payment)
        // check if the player has cargo space
        // deduct the money. add the item.
        Item item = GameManager.Instance.GetItemByNumber(num);
        if (inv.GetItemQuantity(num) > 0 && ((PlayerMovement.Instance.inv.GetGold() ) > (item.price + przycinka) || isStorage) && PlayerMovement.Instance.inv.GetSlotSpaceForItem(num) > 0 ) 
        {
            if (!isStorage) PlayerMovement.Instance.AddGold(-item.price - przycinka);
            // inv.AddItem(1, inv.GetItemPrice(num)); // shop having gold is stupid.
            inv.AddItem(num,-1);
            PlayerMovement.Instance.inv.AddItem(num, 1);
            ShopTransaction(num, 1);
            RefreshInventoryDisplay();
        }

        // refresh to buttons!
    }


    public void OnSellClicked(int num)
    {
        if (PlayerMovement.Instance.inv.GetItemQuantity(num) > 0) // && inv.GetGold() > inv.GetItemPrice(num)
        {
            if (!isStorage) PlayerMovement.Instance.AddGold(GameManager.Instance.GetItemByNumber(num).price);
            // inv.AddItem(1, inv.GetItemPrice(num)); // shop having gold is stupid.
            inv.AddItem(num, 1);
            PlayerMovement.Instance.inv.AddItem(num, -1);
            ShopTransaction(num, -1);
            RefreshInventoryDisplay();
        }
    }


    public void ShopTransaction(int num, int quantity)
    {
        OnShopTransaction?.Invoke(num, quantity, this);
    }

    public object CaptureState()
    {
        return invResetCount;
    }

    public void RestoreState(object state)
    {
        invResetCount = (int)state;
    }
}
