Shader "Nature/Terrain/Lore3"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex("Albedo (RGB)", 2D) = "white" {}
        _SecTex("Secondary (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows

        #pragma target 3.0

        sampler2D _Texture;
        sampler2D _SecTex;

        
        struct Input
        {
            float2 uv_Texture;
            float3 worldNormal;
            float3 worldPos;
        };

        half _Glossiness;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {

            // fixed4 color = tex2D(_Texture, uv_Texture);
            fixed4 cX = tex2D(_Texture, IN.worldPos.yz);
            fixed4 cY = tex2D(_Texture, IN.worldPos.xz);
            fixed4 cZ = tex2D(_Texture, IN.worldPos.xy);

            float3 blend = abs(IN.worldNormal);
            blend /= blend.x + blend.y + blend.z + 0.001f;

            fixed4 color = blend.x * cX + blend.y * cY + blend.z * cZ;


            // Albedo comes from a texture tinted by color
            // fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = color.rgb;

            o.Smoothness = _Glossiness;
            //o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
